$(function(){
    prepareCBox();
});
function prepareCBox(){
    var l = $('.cbox-pad');
    for(var i=0;i< l.length;i++){
        var box = $(l[i]);
        var boxs = $($(box.children('.cbox-line')).children('.cboxs'));
        var next = $(box.children('.cbox-next'));
        var prev = $(box.children('.cbox-prev'));
        //console.log(boxs.width() + ' > ' + box.width());
        if(boxs.width() > box.width()){
            next.css('display','block');
            prev.css('display','none');
        }
    }

    $('.cbox-next').click(function(){
        this.blur();
        var box = $(this).parent();
        var boxs = $($(box.children('.cbox-line')).children('.cboxs'));
        var next = $(box.children('.cbox-next'));
        var prev = $(box.children('.cbox-prev'));
        cboxNextPrev(box,boxs,next,prev,true);
    });
    $('.cbox-prev').click(function(){
        var box = $(this).parent();
        var boxs = $($(box.children('.cbox-line')).children('.cboxs'));
        var next = $(box.children('.cbox-next'));
        var prev = $(box.children('.cbox-prev'));
        this.blur();
        cboxNextPrev(box,boxs,next,prev,false);
    });
}

function cboxNextPrev(box,boxs,next,prev,isnext){
    var boxViewCount = parseInt(box.width()/230);
    if(box.width()%230 > 0) boxViewCount = boxViewCount+1;

    var marginL = parseInt(boxs.css('margin-left'));
    var nowPos = marginL - (marginL%230);
    var boxWcount = parseInt(box.width()/230);
    if(isnext){
        var nextPos = marginL - (marginL%230) - boxWcount*230;
//        console.log(nextPos+' > '+(boxs.width() + nextPos)+' < '+box.width());
        if(boxs.width() + nextPos < box.width()){
            nextPos = box.width() - boxs.width();
        }
        if( boxs.width() + marginL > box.width() ){
            boxs.stop().animate({
                    marginLeft:nextPos
                },
                400,
                function(){
                    cboxNextPrevHideShow(box,boxs,next,prev);
                }
            );
        }
    }
    else{
        var nextPos = marginL - (marginL%230) + boxWcount*230;
        if(nextPos > 0){
            nextPos = 0;
        }
        if( marginL < 0 ){
            boxs.stop().animate({
                    marginLeft:nextPos
                },
                400,
                function(){
                    cboxNextPrevHideShow(box,boxs,next,prev);
                }
            );
        }
    }
    //console.log('isnext='+isnext+' boxViewCount='+boxViewCount+' marginL='+marginL+' nowPos='+nowPos+' nextPos='+nextPos);
}
function cboxNextPrevHideShow(box,boxs,next,prev){
    var marginL = parseInt(boxs.css('margin-left'));
    if(marginL >= 0){
        prev.css('display','none');
        next.css('display','block');
    }
    else{
        if( boxs.width() + marginL <= box.width() ){
            prev.css('display','block');
            next.css('display','none');
        }
        else{
            prev.css('display','block');
            next.css('display','block');
        }
    }
}