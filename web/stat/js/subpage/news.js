var tsetsee_news = (function(){
    var _inited = false;
    var _firstInit = true;
    var _config = {};
    var $root;

    var _data;

    var _init = function(option) {
        _config = {
            page: 1,
            pagesize: 16,
            dataUrl: 'http://news.gogo.mn/gogomusic/getnews',
            thumb_width: 342
        };
        $.extend(_config, option);
        $root = $("#"+_config.root);
        _loadEvents();
        _loadData();
        _inited = true;
    }, _loadEvents = function(){
        $root.on('tsetsee_news:start_load_data', function(){
            if(_firstInit)
                $("#loader").modal('show');
        }).on('tsetsee_news:finish_load_data', function(){
            _build();
            if(_firstInit) {
                $("#loader").modal('hide');
                _firstInit = false;
            }
        }).on('tsetsee_news:fail_load_data', function(){
            if(_firstInit) {
                $("#loader").modal('hide');
                _firstInit = false;
            }
        });

    },_loadData = function(){

        $root.trigger('tsetsee_news:start_load_data');

        $.ajax({
            url: _config.dataUrl,
            type: 'POST',
            dataType: 'json',
            data: {
                page: _config.page,
                pagesize: _config.pagesize,
                thumb_width: _config.thumb_width
            },
            success: function(response) {
                _data = response;
                $root.trigger('tsetsee_news:finish_load_data');
            }
        }).fail(function(){
            $root.trigger('tsetsee_news:fail_load_data');
        });
    }, _build = function(){
        $.each(_data.newslist, function(i, news){
            $root.find('.clearfix').before(tmpl("simple-grid-tmpl", news));
        });
    }, _loadNext = function() {
        if(Math.ceil(_data.total / _config.pagesize) < _config.page + 1) {
            return;
        }
        _config.page++;
        _loadData();
    };

    return {
        init: _init,
        loadNext: _loadNext,
        isInited: function(){ return _inited }
    };
})();

function fromPhpDateToJsDate(phpdate) {
    var t = phpdate.split(" ");
    var k = t[1].split(".");
    var s = t[0].split("-").map(function(el){return parseInt(el);});
    var l = k[0].split(":").map(function(el){return parseInt(el);});
    return new Date(s[0],s[1]-1,s[2],l[0],l[1],l[2]);
}

function changeToDateString(phpdate){
    var date = fromPhpDateToJsDate(phpdate);
    var locale = 'mn';
    var now = new Date();
    var unuudur = now.clone();
    unuudur.clearTime();

    var uchigdur = unuudur.clone();
    uchigdur.addDays(-1);

    var urjigdar = uchigdur.clone();
    urjigdar.addDays(-1);

    var GMT = 'GMT' + date.getUTCOffset().substring(0, 3);

    if (urjigdar > date) {
        return date.toString('yyyy-MM-dd');
    }
    if (uchigdur > date) {
        if (locale == 'en') {
            return date.toString('yyyy-MM-dd');
        }
        else {
            return 'уржигдар';
        }
    }
    if (unuudur > date) {
        return 'өчигдөр';
    }

    if (date > unuudur) {
        var intervalTsag = date - now;
        var tsagzuruu = Math.floor(intervalTsag / (60 * 60 * 1000));

        var min = date.clone();

        if (tsagzuruu > 0) {
            min.addHours(tsagzuruu);
        }

        var intervalMin = min - now;
        var minzuruu = Math.floor(intervalMin / (1000 * 60)) % 60;

        if (tsagzuruu > 0) {
            if (locale == 'en') {
                var ret = '';
                if (tsagzuruu == 1) {
                    ret += tsagzuruu + ' hour ';
                }
                else {
                    ret += tsagzuruu + ' hours ';
                }
                if (minzuruu > 0) {
                    if (minzuruu == 1) {
                        ret += minzuruu + ' min ';
                    }
                    else {
                        ret += minzuruu + ' min ';
                    }
                }
                ret += 'ago';

                return ret;
            }
        }
        else {
            if (minzuruu > 0) {
                if (locale == 'en') {
                    if (minzuruu == 1) {
                        return minzuruu + ' minute ago';
                    }
                    else {
                        return minzuruu + ' minutes ago';
                    }
                }
                else {
                    return minzuruu + ' минут';
                }
            }
            else {
                if (locale == 'en') {
                    return 'right now';
                }
                else {
                    return 'дөнгөж сая';
                }
            }
        }
        return date.toString('yyyy-MM-dd HH:mm') + ' ' + GMT;
    }

    return 'ww';
}