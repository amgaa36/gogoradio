var tsetsee_video = (function(){
    var _firstInit = false;
    var _config = {};
    var $_el;
    var _data = {};

    var _init = function(option) {
        _data = {};
        _config = {
            page: 1,
            pagesize: 10,
            dataUrl: '',
            thumb_width: 342,
            orderBy: 'new',
            templateId: 'simple-grid-video-tmpl'
        }
        $.extend(_config, option);
        $_el = $("#video-grid-container");
        _loadEvents();
        //_loadData();
    }, _updateConfig = function(option){
        $.extend(_config, option);
    }, _loadEvents = function(){
        $_el.on('tsetsee_video:start_load_data', function(){
        }).on('tsetsee_video:finish_load_data', function(){
            _build();
        }).on('tsetsee_video:fail_load_data', function(){
        });

    },_loadData = function(){

        $_el.trigger('tsetsee_video:start_load_data');

        $.ajax({
            url: _config.dataUrl,
            type: 'POST',
            dataType: 'json',
            data: _config,
            success: function(response) {
                _data = response;
                $_el.trigger('tsetsee_video:finish_load_data');
            }
        }).fail(function(){
            $_el.trigger('tsetsee_video:fail_load_data');
        });
    }, _build = function(){
        $.each(_data.videos, function(i, video){
            $("#video-grid-container > .clearfix").before(tmpl(_config.templateId, video));
        });
    }, _loadNext = function() {
            if((typeof _data.video_config !== 'undefined') && Math.ceil(_data.video_config.total / _config.pagesize) < _config.page + 1) {
                return;
            }
        _config.page++;
        _loadData();
    };

    return {
        init: _init,
        loadData: _loadData,
        loadNext: _loadNext,
        updateConfig: _updateConfig
    };
})();
