/**
 * Created by Amgaa on 3/2/15.
 */

$('.thumb-option-later a').click(function addToLast(e) {
    e.preventDefault();
    var htmlObject = $(this) ;
    if ($('.sortable li').length == 0) {
        var current = generateThumb(htmlObject);
        if ($("#sortable li").length > 0) {
            $('#playing').after(current);
            playNext();
        } else {
            $(".sortable").append(current);
            current.attr('id', 'playing').addClass('played-track');
            playthumb(current);
        }
        $(".sortable").sortable("refresh");
    } else {
        var lastItem = generateThumb(htmlObject);

        $(".sortable").append(lastItem);
        $(".sortable").sortable("refresh");

    }


});

$('.grid-thumb-audio').click(function addToCurrent(e) {
    console.log('addtocurrent');
    e.preventDefault();
    var htmlObject = $(this) ;
    var current = generateThumb(htmlObject);
    if ($("#sortable li").length > 0) {
        $('#playing').after(current);
        playNext();
    } else {
        $(".sortable").append(current);
        current.attr('id', 'playing').addClass('played-track');
        playthumb(current);
    }
    $(".sortable").sortable("refresh");

});