/**
 * Created by Amgaa on 2/3/15.
 */
(function($) {
    // loop toggle

    MediaElementPlayer.prototype.buildlyrics = function(player, controls, layers, media) {
        var
        // create the loop button
            lyrics =
                $('<div class="mejs-button gogo-plugin mejs-lyrics-button  mejs-lyrics-off">' +
                    '<button type="button" title="Дууны үг" href="javascript:void(0);" ></button>' +
                    '</div>')
                    // append it to the toolbar
                    .appendTo(controls)
                    // add a click toggle event
                    .click(function() {

                        if(!lyrics.hasClass("mejs-gogo-radio-inactive-button")) {
                            var audioid;
                            var backgroundurl;
                            var audioTitle;
                            if(radioconfig.isplaying){
                                var currentAudio = radioconfig.playlist[radioconfig.current_audio].audio;
                                console.log(currentAudio);
                                audioid = currentAudio.id;
                                var baseUrl = 'http://gstat.mn/musicnew';
                                if (currentAudio.img){
                                    backgroundurl =  baseUrl + currentAudio.img;
                                }else if(currentAudio.album_img){
                                    backgroundurl = baseUrl + currentAudio.album_img;
                                }else if(currentAudio.artist_img){
                                    backgroundurl = baseUrl + currentAudio.artist_img;
                                }
                                console.log(backgroundurl);
                                audioTitle = currentAudio.name+' - '+currentAudio.artist_name;
                            }else{
                                if($('#playing').length > 0){
                                    audioid = $('#playing').attr('contentid');
                                    backgroundurl = 'url('+$('#playing').find('.player-track-item-image').attr('src')+')';
                                    audioTitle = $('#playing').find('.player-track-title').html();
                                }else{
                                    toastr['info']('Дуу тоглож байх үед боломжтой.');
                                }

                            }

                            if(audioid){
                                $.ajax({
                                    url: "/get/lyrics",
                                    data: { audioId: audioid},
                                    dataType: 'json',
                                    success: function(msg){
                                        if(msg.length > 0){
                                            var k = msg[0];
                                            $('#lyrics-title').html(audioTitle);
                                            if(k['lyrics']){
//                                            $("#lyrics-container").html(msg[0]['lyrics']);
//                                            $("#player-lyrics-container").show();
                                                $("#lyrics-container-background").css({
                                                    'background-image': backgroundurl
                                                });
                                                $("#lyrics-container-background").css("-webkit-filter", "blur(13px)");
                                                $("#lyrics-text").html(k['lyrics']);
                                                $('#lyrics-modal-lg').modal('show');
                                            }else{
                                                toastr['info']('Уучлаарай. Дууны үг олдсонгүй.');
                                            }
                                        }
                                    }
                                })
                            }
                        }

                    }

                );
    }




    MediaElementPlayer.prototype.buildnexttrack = function(player, controls, layers, media) {
        var
        // create the next button
            nexttrack =
                $('<div class="mejs-button mejs-nexttrack-button">' +
                    '<button type="button" title="Дараагийн дуу"></button>' +
                    '</div>')
                    // append it to the toolbar
                    .appendTo(controls)
                    // add a click toggle event
                    .click(function() {
                        if(!nexttrack.hasClass("mejs-gogo-radio-inactive-button")){
                            playNext();
                        }
                    });
    }

    MediaElementPlayer.prototype.buildTitle = function(player, controls, layers, media) {
        var
            nexttrack =
                $('<div id="player-current-title"><marquee width="100%"></marquee></div>')
                    // append it to the toolbar
                    .appendTo(controls);
    }

})(jQuery);
