/**
 * CommentFactory
 *
 * @author tsetsee_yugi
 * @date 2015-MAR-24 15:22
 * @version 1.0
 */

var CommentFactory = function () {
    this.config = {
        logged: false,
        userImageUrl: '',
        isPremium: false,
        loginName: '',
        putUrl: '',
        getUrl: ''
    };
    this.data = {};
    this.lock = false;
}
CommentFactory.prototype.toDate = function (str) {
    var a = str.split(" ");
    var b = a[0].split("-");
    var c = a[1].split(":");

    return new Date(b[0], b[1]-1, b[2], c[0], c[1], c[2]);
}
CommentFactory.prototype.fromDateToString = function (date) {
    return date.getFullYear() + "." + ("00" + (date.getMonth() + 1)).slice(-2) + "." + ("00" + date.getDate()).slice(-2);
}
CommentFactory.prototype.init = function (options) {
    var me = this;
    $.extend(me.config, options);

    me.$el = $("#" + me.config.id);
    me.$list = me.$el.find('.comment-list');
    me.$comment_input = me.$el.find(".comment-input");
    me.$sendbox = me.$comment_input.find(".sendbox");
    me.$count = me.$el.find('.commentbox-count');

    $.ajax({
        url: me.config.getUrl,
        async: false,
        type: 'POST',
        data: { path: me.$el.data('path') },
        success: function(response) {
            if(response.status == 'success')
                me.data = response.comments;
        }
    });
    me.$count.html(me.data.length);
    me.$el.trigger('comment:change_count', [me.data.length]);
    $.each(me.data, function(i, comment){
        me.$list.append(tmpl('comment-tmpl', comment));
    });

    this.$comment_input.find(".sendbtn").click(function () {
        me.put();
    });
}
CommentFactory.prototype.put = function () {
    var me = this;
    if (me.lock)
        return;
    var body = me.$sendbox.val();

    if (!body || $.trim(body) == '') return;

    me.lock = true;
    $.ajax({
        url: me.config.putUrl,
        type: "POST",
        data: {
            path: me.$el.data('path'),
            comment_body: body
        },
        dataType: 'json',
        success: function (response) {
            if (response.status == 'success') {
                me.$list.prepend(tmpl('comment-tmpl',{
                    userImageUrl: me.config.userImageUrl,
                    isPremium: me.config.isPremium,
                    loginName: me.config.loginName,
                    main: {
                        body: me.$sendbox.val(),
                        posted_date: response.date
                    }
                }));
                me.$sendbox.val('');
                var count = parseInt(me.$count.html())+1;
                me.$count.html(count);
                me.$el.trigger('comment:change_count', [count]);
            }
            me.lock = false;
        }
    }).fail(function () {
        me.lock = false;
    });
}
