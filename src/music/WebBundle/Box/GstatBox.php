<?php
/**
 * Created by PhpStorm.
 * User: ankhaa
 * Date: 3/31/15
 * Time: 4:59 PM
 */

namespace music\WebBundle\Box;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GstatBox {
    public function setGstatUrl(Controller $controlelr,$container){
        $arr = array();

//        $controlelr->getRequest()->getSession()->remove('statdomain');
        //geoip
        if(!$controlelr->getRequest()->getSession()->has('statdomain')){
//            $ip = $controlelr->getRequest()->getClientIp();
            $ip = $_SERVER["REMOTE_ADDR"];

            $geoip = $controlelr->get('maxmind.geoip')->lookup($ip);
            if($geoip){
                $countryCode = strtolower($geoip->getCountryCode());
//                $countryCode = 'ro';
                $pos = strpos("mn",$countryCode);
                if($pos === false){
                    $pos = strpos("al|ad|az|at|am|be|ba|bg|by|hr|cy|cz|dk|ee|fo|fi|ax|fr|ge|de|gi|gr|va|hu|is|ie|it|kz|lv|li|lt|lu|mt|mc|md|me|nl|no|pl|pt|ro|ru|sm|sk|si|es|sj|se|ch|tr|ua|mk|gb|gg|je|im",$countryCode);
//                    echo $pos;
                    if($pos === false){
                        $pos = strpos("us|ca",$countryCode);
                        if($pos === false){
                            $pos = strpos("cn|hk|sg|in|kr|jp|au|id",$countryCode);
                            if($pos === false){
                                //unknown gstat.mn
                                $controlelr->getRequest()->getSession()->set('statdomain',$container->getParameter('statdomain'));
                            }
                            else{
                                $controlelr->getRequest()->getSession()->set('statdomain',$container->getParameter('statdomain_asia'));
                            }
                        }
                        else{
                            $controlelr->getRequest()->getSession()->set('statdomain',$container->getParameter('statdomain_us'));
                        }
                    }
                    else{
                        $controlelr->getRequest()->getSession()->set('statdomain',$container->getParameter('statdomain_eu'));
                    }
                }
                else{
                    //mongol gstat.mn
                    $controlelr->getRequest()->getSession()->set('statdomain',$container->getParameter('statdomain'));
                }
            }
            else{
                //unknown gstat.mn
                $controlelr->getRequest()->getSession()->set('statdomain',$container->getParameter('statdomain'));
            }
        }
        $arr['statdomain'] = $controlelr->getRequest()->getSession()->get('statdomain');
//        $ip = $controlelr->getRequest()->getClientIp();
        $ip = $_SERVER["REMOTE_ADDR"];
        $geoip = $controlelr->get('maxmind.geoip')->lookup($ip);
        $arr['ip'] = $ip;
        if($geoip){
            $countryCode = strtolower($geoip->getCountryCode());
            $arr['ipregion'] = $countryCode;
        }
        else{
            $arr['ipregion'] = 'ddd';
        }

        return $arr;
    }
} 