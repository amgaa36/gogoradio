<?php

namespace music\WebBundle\Box;


class AudioBox {



    public function audioDetail($controller, $id, $uid=0) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager('default');

        $qb = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('a')
            ->select('a.id, a.name,COALESCE(a.img, a.album_img, a.artist_img) as img,
            a.publish_date, a.artist_name as artist')
            ->where('a.publish_date <= :now OR a.publish_date is null')
            ->andWhere('a.id = :id')
            ->setParameter('now', new \DateTime())
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();

        $song = null;
        if($qb && count($qb) > 0)
            $song = $qb[0];

        if(!$song)
            throw $controller->createNotFoundException('song not found.');

        $connection = $em->getConnection();

        $statement = $connection->prepare('select v.videoUrl as videoUrl from msc_audio_video av
        left join msc_video v on v.id = av.video_id
        where av.audio_id = :aid and v.publish_date < :now');
        $statement->execute(array('aid'=>$id, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s') ));
        $videos = $statement->fetchAll();

        $statement = $connection->prepare('select genre_id from msc_genre_audio where audio_id = :audioId limit 1');
        $statement->execute(array(':audioId'=>$id));
        $genre = $statement->fetch();

        if($genre['genre_id']) {
            $statement = $connection->prepare('select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.artist_name_html as artist, au.is_lyrics, au.is_video, au.artist_name as artists, au.file_url as url, "1" as content_type
                    from msc_genre_audio gen left join msc_audio au on gen.audio_id = au.id
                    where au.publish_date <= :now and gen.genre_id = :gid
                    order by au.publish_date DESC, content_type ASC, au.artist_name ASC
                    limit 20');
            $statement->execute(array(':gid' => $genre['genre_id'], ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $related = $statement->fetchAll();
        }
        else {
            $statement = $connection->prepare('select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.artist_name_html as artist, au.is_lyrics, au.is_video, au.artist_name as artists, au.file_url as url, "1" as content_type
                    from msc_audio au
                    where au.publish_date <= :now
                    order by RAND()
                    limit 20');
            $statement->execute(array( ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $related = $statement->fetchAll();
        }

        shuffle($related);
        $related = array_splice($related, 0, 20);

        if($uid != 0) {
            $statement = $connection->prepare('select id from msc_user_audio where user_id = :uid and audio_id = :aid');
            $statement->execute(array(':uid' => $uid, ':aid' => $id));
            $islike = $statement->fetchAll();

            if(count($islike) > 0){
                $song['islike'] = true;
            }

        }

        $arr['content'] = $song;
        $arr['videos'] = $videos;
        $arr['related'] = $related;

        return $arr;
    }


    public function getSong($controller, $id){
        $em = $controller->getDoctrine()->getManager('default');

        $result = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('a')
            ->select('a.name, a.artist_name,COALESCE (a.img,a.album_img,a.artist_img) as img, a.publish_date')
            ->where('a.id= :audid')
            ->setParameter('audid',$id)
            ->getQuery()
            ->getArrayResult();
        return $result[0];
    }


    public function getLyric($controller,$audid){
        $em = $controller->getDoctrine()->getManager('default');

        $result = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('a')
            ->select('a.lyrics')
            ->where('a.id= :audid')
            ->setParameter('audid',$audid)
            ->getQuery()
            ->getArrayResult();
        return $result;
    }


} 