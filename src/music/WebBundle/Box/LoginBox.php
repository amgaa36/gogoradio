<?php

namespace music\WebBundle\Box;

class LoginBox {
    public function isLogin($controller) {
        $arr = array();

        $user = $controller->get('security.context')->getToken()->getUser();

//        echo var_dump($user);
//        exit(0);


        $islogged = false;

        if(is_object($user)) {
            $islogged = true;
            $arr['loginuser'] = $user;
        }

        $arr['islogged'] = $islogged;

        return $arr;
    }
}



