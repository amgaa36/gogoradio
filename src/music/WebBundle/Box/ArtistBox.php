<?php

namespace music\WebBundle\Box;

class ArtistBox {

    public function getArtists($controller, $uid=0) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $connection = $em->getConnection();

        if($uid == 0) {
            $statement = $connection->prepare('select * from (select a.id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, null as islike
            from msc_artist a where a.published_date < :now and a.published_date is not null order by rand() limit 50) sub order by rand() limit 15');
            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }
        else {
            $statement = $connection->prepare('select * from (select a.id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type,
              (select user_id from msc_user_artist where user_id = :uid and artist_id = a.id) as islike from msc_artist a
              left join msc_user_artist ua on ua.artist_id = a.id
              where a.published_date < :now and a.published_date is not null
              order by rand() limit 50) sub order by rand() limit 15');
            $statement->execute(array(':uid' => $uid,':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }

        $related = $statement->fetchAll();

        $arr['artists'] = $related;

        return $arr;
    }



    public function artistDetail($controller,$id, $uid=0){

        $arr = array();
        $em = $controller->getDoctrine()->getManager('default');
        $connection = $em->getConnection();
        if($uid == 0){
            $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('a')
                ->select("a.id, a.name as name, a.img as img, a.like_count as like_count, a.web_url as web_url, a.fb_url as fb_url, a.tw_url as tw_url,a.description as description, a.created_date as created_date, a.cover as artist_cover, 4 as content_type, 'null' as islike")
                ->where('a.id = :aid')
                ->andWhere('a.published_date < :now')
                ->setParameter('aid', $id)
                ->setParameter('now', new \DateTime())
                ->getQuery()
                ->getArrayResult();
        }else{
            $statement = $connection->prepare("select a.id as id, a.artist_name as name,  a.artist_img as img, a.like_count as like_count, a.web_url as web_url, a.fb_url as fb_url, a.tw_url as tw_url,a.artist_cover as artist_cover,  a.description as description, a.created_date as created_date, 4 as content_type, userlike.liked as followed
            from msc_artist a
            left join (select artist_id as arid, '1' as liked from msc_user_artist where user_id = :uid) as userlike on userlike.arid = a.id
            where a.id = :id and a.published_date < :now");
            $statement->execute(array(':uid' => $uid, ':id'=>$id, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $qb = $statement->fetchAll();
        }



        if($qb && count($qb)>0){
            $arr['artist'] = $qb[0];

            //ih sonsson duunuud

            if($uid == 0)
                $ihsonsson = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('aa')
                    ->addSelect("aud.id as id, aud.name, aud.listen_count,aud.artist_name_html as artist, aud.hitone as hitone, aud.duration, aud.is_lyrics, aud.is_video, aud.artist_name as artists, aud.file_url,COALESCE(aud.img, aud.album_img, aud.artist_img) as img,1 as content_type, 'null' as islike")
                    ->leftJoin('aa.audio','aud')
                    ->leftJoin('aa.artist','art')
                    ->where('art.id=:aid')
                    ->andWhere('aud.publish_date < :now')
                    ->setParameter('aid',$id)
                    ->setParameter('now',new \DateTime('now'))
                    ->orderBy('aud.listen_count','DESC')
                    ->setMaxResults(5)
                    ->getQuery()
                    ->getArrayResult();


            else{
                $statement = $connection->prepare("select aud.id as id, aud.audio_name as name, aud.artist_name_html as artist, aud.hitone as hitone, aud.artist_name as artists, aud.listen_count, aud.duration, aud.is_lyrics, aud.is_video, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img, 1 as content_type, userlike.liked as islike
                from msc_audio_artist aa, msc_audio aud
                left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on aud.id = userlike.auid
                where aud.publish_date <= :now and aa.artist_id = :artid and aa.audio_id = aud.id order by aud.listen_count desc limit 5");
                $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'),':artid'=>$id,':uid'=>$uid));
                $ihsonsson = $statement->fetchAll();
            }

            $arr['most_listens'] = $ihsonsson;

            // single duunuud
            if($uid == 0)
                $audioQB = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('aa')
                    ->addSelect("aud.id as id, aud.name, aud.listen_count,aud.artist_name_html as artist, aud.hitone as hitone, aud.duration, aud.is_lyrics, aud.is_video, aud.artist_name as artists, aud.file_url,COALESCE(aud.img, aud.album_img, aud.artist_img) as img,1 as content_type, 'null' as islike")
                    ->leftJoin('aa.audio','aud')
                    ->leftJoin('aa.artist','art')
                    ->where('art.id=:aid')
                    ->andWhere('aud.album_name is null')
                    ->andWhere('aud.publish_date < :now')
                    ->setParameter('aid',$id)
                    ->setParameter('now',new \DateTime('now'))
                    ->orderBy('aud.publish_date','DESC')
                    ->getQuery()
                    ->getArrayResult();


            else{
                $statement = $connection->prepare("select aud.id as id, aud.audio_name as name, aud.artist_name_html as artist, aud.hitone as hitone, aud.artist_name as artists, aud.listen_count, aud.duration, aud.is_lyrics, aud.is_video, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img, 1 as content_type, userlike.liked as islike
                from msc_audio_artist aa, msc_audio aud
                left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on aud.id = userlike.auid
                where aud.publish_date <= :now and aa.artist_id = :artid and aa.audio_id = aud.id and aud.album_name is null order by aud.publish_date DESC");
                $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'),':artid'=>$id,':uid'=>$uid));
                $audioQB = $statement->fetchAll();
            }
            if($audioQB && count($audioQB)>0){
                $arr['audios'] = $audioQB;
            }



            if($uid == 0){
                $albums = $em->getRepository('musicCmsBundle:AlbumArtist')->createQueryBuilder('aa')
                    ->addSelect("a.id, a.name as name, COALESCE(a.img, a.artist_img) as img, a.like_count as like_count, a.publish_date, a.artist_name as artist, 2 as content_type, a.track_number as track_number, a.duration as duration,'null' as islike")
                    ->leftJoin('aa.album', 'a')
                    ->where('aa.artist = :id')
                    ->setParameter('id', $id)
                    ->andWhere('a.publish_date <= :now')
                    ->setParameter('now', new \DateTime())
                    ->orderBy('a.publish_date', 'DESC')
                    ->getQuery()
                    ->getArrayResult();
                foreach($albums as &$album){
                    $albumid = $album['id'];
                    $audios = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('aa')
                        ->addSelect("aud.id as id, aud.name, aud.listen_count, aud.artist_name as artists, aud.hitone as hitone, aud.duration, aud.is_lyrics, aud.is_video, aud.file_url,COALESCE(aud.img, aud.album_img, aud.artist_img) as img, 'null' as islike")
                        ->leftJoin('aa.audio','aud')
                        ->leftJoin('aa.album','alb')
                        ->where('alb.id=:aid')
                        ->setParameter('aid',$albumid)
                        ->orderBy('aa.order', 'ASC')
                        ->getQuery()
                        ->getArrayResult();

                    $album['audios'] = $audios;
                }

            }else{
                $statement = $connection->prepare("select a.id as id, a.album_name as name, a.artist_name as artist,a.publish_date, a.like_count,COALESCE(a.album_img, a.artist_img) as img, 2 as content_type, userlike.liked as islike
            from msc_album_artist aa, msc_album a
            left join (select album_id as albumid, '1' as liked from msc_user_album where user_id = :uid) as userlike on a.id = userlike.albumid
            where a.publish_date <= :now and aa.artist_id = :arid and aa.album_id = a.id order by a.publish_date desc ");
                $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'),':arid'=>$id,':uid'=>$uid));
                $albums = $statement->fetchAll();
                foreach($albums as &$album){
                    $albumId = $album['id'];
                    $statement = $connection->prepare("select aud.id as id, aud.audio_name as name, aud.hitone as hitone, aud.artist_name as artists, aud.listen_count, aud.duration, aud.is_lyrics, aud.is_video, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img, 1 as content_type, userlike.liked as islike
                    from msc_album_audio aa, msc_audio aud
                    left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on aud.id = userlike.auid
                    where aud.publish_date <= :now and aa.album_id = :albid and aa.audio_id = aud.id order by aa.audio_order asc");
                    $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'),':albid'=>$albumId,':uid'=>$uid));
                    $album['audios'] = $statement->fetchAll();
                }
            }


            $arr['albums'] = $albums;

        }




        return $arr;

    }

    public function getAllArtists($controller, $page,$type) {
        $arr = array();
        $pageSize = 50;
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $offset = ($page-1)*$pageSize;
        $now = new \DateTime('now');

        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $uid = $controller->getUser()->getId();
            $statement = $connection->prepare("select a.id as id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, userlike.liked as islike
            from msc_artist a
            left join (select max(id) as id, artist_id from msc_audio_artist group by artist_id) aa on aa.artist_id = a.id
            left join (select artist_id as arid, '1' as liked from msc_user_artist where user_id = ?) as userlike on userlike.arid = a.id
            where a.is_mongolian = ? and a.published_date < ? and a.published_date is not null order by aa.id desc limit ? , ?");

            $statement->bindValue(1, $uid, 'integer');
            $statement->bindValue(2, $type, 'integer');
            $statement->bindValue(3, $now, 'datetime');
            $statement->bindValue(4, $offset, 'integer');
            $statement->bindValue(5, $pageSize, 'integer');
            $statement->execute();
        }else{
            $statement = $connection->prepare("select a.id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, null as islike
            from msc_artist a
            left join (select max(id) as id, artist_id from msc_audio_artist group by artist_id) aa on aa.artist_id = a.id
            where a.is_mongolian = ? and a.published_date < ? and a.published_date is not null order by aa.id desc limit ? , ? ");
            $statement->bindValue(1, $type, 'integer');
            $statement->bindValue(2, $now, 'datetime');
            $statement->bindValue(3, $offset, 'integer');
            $statement->bindValue(4, $pageSize, 'integer');
            $statement->execute();
        }


        $related = $statement->fetchAll();

        $arr['artists'] = $related;

        return $arr;
    }

    public function getAbroadArtists($controller, $uid=0) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $connection = $em->getConnection();

        if($uid == 0) {
            $statement = $connection->prepare('select * from (select a.id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, null as islike
            from msc_artist a where a.is_mongolian = 0 and a.published_date < :now and a.published_date is not null order by rand() limit 50) sub order by rand() limit 10');
            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }
        else {
            $statement = $connection->prepare("select * from (select a.id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, userlike.liked as islike
              from msc_artist a
              left join (select artist_id as arid, '1' as liked from msc_user_artist where user_id = :uid) as userlike on userlike.arid = a.id
              where a.is_mongolian = 0

              where a.published_date < :now and a.published_date is not null
              order by rand() limit 50) sub order by rand() limit 10");
            $statement->execute(array(':uid' => $uid, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }

        $related = $statement->fetchAll();

        $arr['artists'] = $related;

        return $arr;
    }

    public function getArtist($controller, $id) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $artist = $em->getRepository('musicCmsBundle:Artist')->find($id);

        if(!$artist)
            throw $controller->createNotFoundException('artist not found.');

        $arr['$artist'] = $artist;

        return $arr;
    }

    public function getArtistTopFive($controller, $id) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('aa')
            ->addSelect('aud')
            ->leftJoin('aa.audio', 'aud')
            ->addSelect('art')
            ->leftJoin('aa.artist', 'art')
            ->where('art.id = :id')
            ->setParameter('id', $id)
            ->andWhere('aud.publish_date <= :now')
            ->setParameter('now', new \DateTime());

        $topFive = $qb
            ->orderBy('aud.listen_count', 'DESC')
            ->addOrderBy('aud.publish_date', 'ASC')
            ->setMaxResults(5)
            ->getQuery()
            ->getArrayResult();

        $arr['artist_top_five'] = $topFive;

        return $arr;
    }

    public function getArtistAlbums($controller, $id) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AlbumArtist')->createQueryBuilder('aa')
            ->addSelect('alb')
            ->leftJoin('aa.album', 'alb')
            ->addSelect('art')
            ->leftJoin('aa.artist', 'art')
            ->where('art.id = :id')
            ->setParameter('id', $id)
            ->andWhere('alb.publish_date <= :now')
            ->setParameter('now', new \DateTime());

        $albums = $qb
            ->orderBy('alb.publish_date', 'DESC')
            ->getQuery()
            ->getArrayResult();

        $arr['artist_albums'] = $albums;

        return $arr;
    }
} 