<?php

namespace music\WebBundle\Box;

use music\CmsBundle\Entity\Genre;
use music\CmsBundle\Entity\AudioBanner;
use Symfony\Component\DependencyInjection\Container;

class RadioBox
{
    public function getGenres(Container $container) {
        $arr = array();
        $em = $container->get('doctrine')->getManager();
        $qb = $em->getRepository('musicCmsBundle:Genre')->createQueryBuilder('e');
        $genres = $qb
            ->addSelect('rand() as HIDDEN rand')
            ->andWhere($qb->expr()->isNotNull('e.publish_date'))
            ->andWhere($qb->expr()->lte('e.publish_date', ':now'))
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('rand','DESC')
            ->getQuery()
            ->getArrayResult()
        ;
        $arr['genre_list'] = $genres;

        return $arr;
    }
    public function getAllGenres(Container $container) {
        $arr = array();
        $em = $container->get('doctrine')->getManager();
        $qb = $em->getRepository('musicCmsBundle:Genre')->createQueryBuilder('e');
        $genres = $qb
            ->select('e.id')
            ->addSelect('e.name')
            ->andWhere($qb->expr()->isNotNull('e.publish_date'))
            ->andWhere($qb->expr()->lte('e.publish_date', ':now'))
            ->setParameter('now', new \DateTime('now'))
            ->getQuery()
            ->getArrayResult()
        ;
        $arr['genre_list'] = $genres;

        return $arr;
    }


}