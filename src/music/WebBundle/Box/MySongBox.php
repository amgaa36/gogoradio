<?php

namespace music\WebBundle\Box;


class MySongBox {

    public function getUserInfo($controller, $userId){

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $id = $controller->getUser()->getId();
            $statement = $connection->prepare("select u.id as id, u.loginName as name,u.lastName as lastName, u.userImageUrl as img,u.userEmail as email, u.userBirthday as birthday, u.fb_url as fb_url, u.tw_url as tw_url, 4 as content_type, userlike.liked as followed, u.loginType as loginType, u.description as description, u.isPremium as isPremium, u.expireDate as expireDate from msc_user u
            left join (select leader_id, '1' as liked from msc_user_follow where follower_id = :uid ) as userlike on u.id = userlike.leader_id
            where u.id = :userid");
            $statement->execute(array(':userid' => $userId, ':uid'=>$id));
            $result = $statement->fetch();
        }else{
            $result = $em->getRepository('musicCmsBundle:User')->createQueryBuilder('u')
                ->select("u.id as id, u.loginName as name,u.lastName as lastName, u.userImageUrl as img, u.userEmail as email, u.userBirthday as birthday, u.fb_url as fb_url, u.tw_url as tw_url, 4 as content_type,'null' as followed, u.loginType as loginType, u.description as description, u.isPremium as isPremium, u.expireDate as expireDate")
                ->where('u.id = :uid')
                ->setParameter('uid', $userId)
                ->getQuery()
                ->getSingleResult();
        }


        $statement = $connection->prepare("select count(*) as cnt from msc_user_follow uf
            where uf.leader_id = :userid");
        $statement->execute(array(':userid' => $userId));
        $followers = $statement->fetch();

        $statement = $connection->prepare("select count(*) as cnt from msc_user_follow uf
            where uf.follower_id = :userid");
        $statement->execute(array(':userid' => $userId));
        $following = $statement->fetch();

        $user = $result;

        $user['follower_number'] = $followers['cnt'];
        $user['followed_number'] = $following['cnt'];
        $arr['user'] = $user;
        return $arr;
    }

    public function getMyFavoriteSongs($controller, $userId) {
        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("select ua.audio_id as id, aud.audio_name as name, aud.artist_name_html as artist, aud.artist_name as artists, aud.listen_count, aud.hitone as hitone, aud.is_lyrics, aud.is_video, aud.listen_count, aud.duration, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img, 1 as content_type, userlike.liked as islike from msc_user_audio ua
        left join msc_audio as aud on ua.audio_id = aud.id
        left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :userid) as userlike on aud.id = userlike.auid
        where ua.user_id = :userid and aud.publish_date < :now
        order by ua.id desc");

        $statement->execute(array(':userid' => $userId, ':now'=>date_format(new \DateTime(),'Y-m-d H:i:s')));


        $results = $statement->fetchAll();
        $arr['audios'] = $results;
        return $arr;
    }

    public function getFavPlaylists($controller, $userId, $id=0) {
        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        if($id == 0){
            $statement = $connection->prepare("select up.playlist_id as id, p.user_id as user_id, p.user_name, p.id, p.playlist_name as name, p.playlist_img as img, '3' as content_type, null as islike from msc_user_playlist up
            left join msc_playlist as p on up.playlist_id = p.id
            where up.user_id = :userid and p.publish_date <= :now
            order by up.id desc
            ");
            $statement->execute(array(':userid' => $userId,':now'=>date_format(new \DateTime(),'Y-m-d H:i:s')));
        }else{
            $statement = $connection->prepare("select up.playlist_id as id, p.user_id as user_id, p.user_name, p.id, p.playlist_name as name, p.playlist_img as img, '3' as content_type, 1 as islike from msc_user_playlist up
            left join msc_playlist as p on up.playlist_id = p.id
            left join (select playlist_id as plid, '1' as liked from msc_user_playlist where user_id = :uid) as userlike on userlike.plid = p.id
            where up.user_id = :userid and p.publish_date <= :now
            order by up.id desc
            ");
            $statement->execute(array(':userid' => $userId,':now'=>date_format(new \DateTime(),'Y-m-d H:i:s'), ':uid'=>$id));
        }

        $results = $statement->fetchAll();
        $arr['playlist'] = $results;
        return $arr;
    }

    public function getMyPlaylists($controller, $userId, $id=0){
        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        if($id == 0){
            $statement = $em->getConnection()->prepare("select pl.id as id, pl.user_id, pl.playlist_name as name, pl.playlist_img as img, pl.user_name, '3' as content_type, null as islike
                from msc_playlist pl
                where pl.publish_date <= :now and pl.user_id = :userid order by pl.publish_date DESC");
            $statement->execute(array(':now'=>date_format(new \DateTime(), 'Y-m-d H:i:s'), ':userid'=>$userId));
        }else{
            $statement = $em->getConnection()->prepare("select pl.id as id, pl.user_id, pl.playlist_name as name, pl.playlist_img as img, pl.user_name, '3' as content_type, userlike.liked as islike
                from msc_playlist pl
                left join (select playlist_id as plid, '1' as liked from msc_user_playlist where user_id = :uid) as userlike on userlike.plid = pl.id
                where pl.publish_date <= :now and pl.user_id = :userid order by pl.publish_date DESC");
            $statement->execute(array(':now'=>date_format(new \DateTime(), 'Y-m-d H:i:s'), ':userid'=>$userId, ':uid'=>$id));
        }

        $arr['playlist'] = $statement->fetchAll();
        return $arr;
    }

    public function getMyAlbums($controller, $userId, $id=0) {

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        if($id == 0){
            $statement = $connection->prepare("select ua.album_id as id, al.album_name as name, al.album_img as img, al.album_img as img1, al.id as url, al.publish_date, al.artist_name_html as artist, '2' as content_type,null as islike from msc_user_album ua
            left join msc_album as al on ua.album_id = al.id
            where ua.user_id = :userid and al.publish_date < :now
            order by ua.id desc");

            $statement->execute(array(':now'=>date_format(new \DateTime(), 'Y-m-d H:i:s'),':userid' => $userId));
        }else{
            $statement = $connection->prepare("select ua.album_id as id, al.album_name as name, al.album_img as img, al.album_img as img1, al.id as url, al.publish_date, al.artist_name_html as artist, '2' as content_type,userlike.liked as islike from msc_user_album ua
            left join msc_album as al on ua.album_id = al.id
            left join (select album_id as alid, '1' as liked from msc_user_album where user_id = :uid) as userlike on userlike.alid = al.id
            where ua.user_id = :userid and al.publish_date < :now
            order by ua.id desc");

            $statement->execute(array(':now'=>date_format(new \DateTime(), 'Y-m-d H:i:s'),':userid' => $userId,':uid'=>$id));
        }

        $results = $statement->fetchAll();

        $arr['albums'] = $results;

        return $arr;
    }

    public function getMyArtists($controller, $userId, $id=0) {

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $artists = $em->getRepository('musicCmsBundle:UserArtist')->createQueryBuilder('ua')
            ->addSelect("a.id, a.name as name, a.img as img, a.like_count as like_count, 4 as content_type,1 as islike")
            ->leftJoin('ua.artist', 'a')
            ->where('ua.user = :id')
            ->andWhere('a.published_date < :now')
            ->setParameter('id', $userId)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getArrayResult();

        $arr['artists'] = $artists;

        return $arr;
    }

    public function getMyVideos($controller, $userId, $id=0) {

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $qb = $em->getRepository('musicCmsBundle:UserVideo')->createQueryBuilder('e');

        $entities = $qb
            ->leftJoin('e.video', 'video')
            ->addSelect('video')
            ->where($qb->expr()->eq('e.user', ':userId'))
            ->setParameter('userId', $userId)
            ->andWhere($qb->expr()->isNotNull('video.publish_date'))
            ->andWhere($qb->expr()->lte('video.publish_date', ':now'))
            ->setParameter('now', new \DateTime('now'))
            ->getQuery()
            ->getArrayResult()
        ;
        $arr['videos'] = $entities;

        return $arr;
    }

    public function getMyCurrentAudios($controller, $userId){
        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $controller->getUser()->getId();
            if($myid == $userId || $userId == null){
                $arr['isme'] = true;
                $userId = $myid;
                $arr["left_menu"] = 4;
            }

            $statement = $connection->prepare("select ua.audio_id  as id, max(ua.listen_date) as listen_date, aud.audio_name as name, aud.artist_name_html as artist, aud.hitone as hitone, aud.listen_count, aud.is_lyrics, aud.is_video, aud.artist_name as artists, aud.listen_count, aud.duration, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img, 1 as content_type, userlike.liked as islike from msc_user_log ua
            left join msc_audio as aud on ua.audio_id = aud.id
            left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on aud.id = userlike.auid
            where ua.user_id = :userid and aud.publish_date < :now group by ua.audio_id
            order by ua.listen_date desc limit 30
            ");
            $statement->execute(array(':userid' => $userId, ':uid'=>$myid, ':now'=>date_format(new \DateTime(),'Y-m-d H:i:s')));
        }else{
            $statement = $connection->prepare("select ua.audio_id  as id, max(ua.listen_date) as listen_date, aud.audio_name as name, aud.artist_name_html as artist, aud.hitone as hitone, aud.listen_count, aud.is_lyrics, aud.is_video, aud.artist_name as artists, aud.listen_count, aud.duration, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img, 1 as content_type, null as islike from msc_user_log ua
            left join msc_audio as aud on ua.audio_id = aud.id
            where ua.user_id = :userid and aud.publish_date < :now group by ua.audio_id
            order by ua.listen_date desc limit 30
            ");
            $statement->execute(array(':userid' => $userId, ':now'=>date_format(new \DateTime(),'Y-m-d H:i:s')));
        }
        $results = $statement->fetchAll();
        $arr['audios'] = $results;
        return $arr;
    }

}





