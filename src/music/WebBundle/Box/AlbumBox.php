<?php

namespace music\WebBundle\Box;

class AlbumBox{

    public function albumDetail($controller,$id, $uid=0){

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        if($uid == 0){
            $qb = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('a')
                ->select("a.id, a.name as name, COALESCE(a.img, a.artist_img) as img, a.like_count as like_count, a.publish_date, a.artist_name_html as artist,2 as content_type, a.track_number as track_number, a.duration as duration,'null' as islike")
                ->where('a.id = :aid')
                ->andWhere('a.publish_date < :now')
                ->setParameter('aid', $id)
                ->setParameter('now',new \DateTime())
                ->getQuery()
                ->getArrayResult();
        }else{
            $statement = $connection->prepare("select a.id as id, a.album_name as name, COALESCE (a.album_img, a.artist_img) as img, a.like_count as like_count, a.publish_date as publish_date,
            a.artist_name_html as artist, '2' as content_type, a.track_number as track_number, a.duration as duration, userlike.liked as islike
            from msc_album a
            left join (select album_id as alid, '1' as liked from msc_user_album where user_id = :uid) as userlike on userlike.alid = a.id
            where a.publish_date <= :now and a.id = :aid");
            $statement->execute(array(':uid' => $uid, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'),':aid'=>$id));
            $qb = $statement->fetchAll();
        }



        if($qb && count($qb)>0){
            $duration = gmdate('H:i:s',$qb[0]['duration']);
            $qb[0]['duration'] = $duration;
            $arr['content'] = $qb[0];

            if($uid == 0)
                $audioQB = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('aa')
                    ->addSelect("aud.id as id, aud.name, aud.listen_count,aud.artist_name_html as artist, aud.artist_name as artists, aud.duration, aud.is_lyrics, aud.is_video, aud.file_url,COALESCE(aud.img, aud.album_img, aud.hitone as hitone, aud.artist_img) as img, 'null' as islike")
                    ->leftJoin('aa.audio','aud')
                    ->leftJoin('aa.album','alb')
                    ->where('alb.id=:aid')
                    ->andWhere('aud.publish_date < :now')
                    ->setParameter('aid',$id)
                    ->setParameter('now', new \DateTime())
                    ->orderBy('aa.order', 'ASC')
                    ->getQuery()
                    ->getArrayResult();


            else{
                $statement = $connection->prepare("select aud.id as id, aud.audio_name as name, aud.artist_name_html as artist, aud.artist_name as artists, aud.listen_count, aud.duration, aud.hitone as hitone, aud.is_lyrics, aud.is_video, aud.file_url,COALESCE(aud.audio_img, aud.album_img, aud.artist_img) as img,userlike.liked as islike
            from msc_album_audio aa, msc_audio aud
            left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on aud.id = userlike.auid
            where aud.publish_date <= :now and aa.album_id = :albid and aa.audio_id = aud.id order by aa.audio_order asc ");
                $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'),':albid'=>$id,':uid'=>$uid));
                $audioQB = $statement->fetchAll();
            }
            if($audioQB && count($audioQB)>0){
                $arr['audios'] = $audioQB;
            }
        }



//        if($qb[0]['user_id']){
//            if($uid == 0)
//                $related = $em->getRepository('musicCmsBundle:Playlist')->createQueryBuilder('pl')
//                    ->select('pl.id as id, pl.name as name, pl.img as img, pl.user_name as artists, 3 as content_type')
//                    ->where('pl.user=:userid')
//                    ->andWhere('pl.id !=:plid')
//                    ->setParameter('userid',$qb[0]['user_id'])
//                    ->setParameter('plid',$id)
//                    ->getQuery()
//                    ->getArrayResult();
//            else{
//                $statement = $connection->prepare("select pl.id as id, pl.playlist_name as name, pl.playlist_img as img, pl.user_name as artists, '3' as content_type, userlike.liked as islike
//                from msc_playlist pl
//                left join (select playlist_id as plid, '1' as liked from msc_user_playlist where user_id = :uid) as userlike on userlike.plid = pl.id
//                where pl.publish_date <= :now and pl.user_id=:userid");
//                $statement->execute(array(':userid'=>$qb[0]['user_id'],':uid' => $uid, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
//                $related = $statement->fetchAll();
//            }
//            if($related && count($related)){
//                $arr['related'] = $related;
//            }
//        }



        return $arr;
    }
}

