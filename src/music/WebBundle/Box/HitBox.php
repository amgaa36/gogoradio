<?php

namespace music\WebBundle\Box;

class HitBox {

    public function getTasteChart($controller){

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $currentTable = 'msc_chart_'.date("Y");
        $string = 'select startDate from '.$currentTable.' order by id desc limit 1';
        $statement = $connection->prepare($string);
        $statement->execute();
        $result = $statement->fetch();
        if(!$result){
            $currentTable = 'msc_chart_'.(date('Y')-1);
            $string = 'select startDate from '.$currentTable.' order by id desc limit 1';
            $statement = $connection->prepare($string);
            $statement->execute();
            $result = $statement->fetch();
        }
        $startDate =  new \DateTime($result['startDate']);

        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $userId = $controller->getUser()->getId();
            $queryString = "select hit.sortId as rank,(hit.sortId-hit.prevWeekPos) as up, au.id as id, au.audio_name as name, au.hitone as hitone, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.artist_name_html as artist, au.is_lyrics, au.is_video, au.artist_name as artists, '1' as content_type, userlike.liked as islike
                    from ".$currentTable." hit, msc_audio au
                    left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on au.id = userlike.auid
                     where au.publish_date <= :now and hit.audioId = au.id and hit.startDate = :startDate order by rank asc limit 15";
            $statement = $connection->prepare($queryString);
            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'), ':startDate'=>date_format($startDate,'Y-m-d'), ':uid'=>$userId));
            $arr['charts'] = $statement->fetchAll();
        }else{
            $queryString = "select hit.sortId as rank,(hit.sortId-hit.prevWeekPos) as up, au.id as id, au.audio_name as name, au.hitone as hitone, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.artist_name_html as artist, au.is_lyrics, au.is_video, au.artist_name as artists, '1' as content_type, null as islike
                    from ".$currentTable." hit, msc_audio au
                     where au.publish_date <= :now and hit.audioId = au.id and hit.startDate = :startDate order by rank asc limit 15";
            $statement = $connection->prepare($queryString);
            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'), ':startDate'=>date_format($startDate,'Y-m-d')));
            $arr['charts'] = $statement->fetchAll();
        }

        return $arr;

    }

    public function getLastChart($controller){

        $arr = array();
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $currentTable = 'msc_chart_'.date("Y");
        $arr['max_year'] = date("Y");
        $string = 'select startDate from '.$currentTable.' order by id desc limit 1';
        $statement = $connection->prepare($string);
        $statement->execute();
        $result = $statement->fetch();
        if(!$result){
            $currentTable = 'msc_chart_'.(date('Y')-1);
            $arr["max_year"] = date("Y")-1;
            $string = 'select startDate from '.$currentTable.' order by id desc limit 1';
            $statement = $connection->prepare($string);
            $statement->execute();
            $result = $statement->fetch();
        }
        $currentDate =  new \DateTime($result['startDate']);

        $arr += $this->getChartWithDate($controller,$currentDate);

        return $arr;
    }


    public function getChartWithDate($controller,$startDate){
        $arr = array();
        $arr['current_date'] = $startDate;
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $currentTable = 'msc_chart_'.date_format($startDate,'Y');
        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $userId = $controller->getUser()->getId();
            $queryString = "select hit.sortId as rank,(hit.sortId-hit.prevWeekPos) as up, au.id as id, au.audio_name as name, au.hitone as hitone, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.is_lyrics, au.is_video, au.publish_date, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, userlike.liked as islike
                    from ".$currentTable." hit, msc_audio au
                    left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on au.id = userlike.auid
                     where au.publish_date <= :now and hit.audioId = au.id and hit.startDate = :startDate order by rank asc";
            $statement = $connection->prepare($queryString);
            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'), ':startDate'=>date_format($startDate,'Y-m-d'), ':uid'=>$userId));
            $arr['audios'] = $statement->fetchAll();
        }else{
            $queryString = "select hit.sortId as rank,(hit.sortId-hit.prevWeekPos) as up, au.id as id, au.audio_name as name, au.hitone as hitone, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.is_lyrics, au.is_video, au.publish_date, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, null as islike
                    from ".$currentTable." hit, msc_audio au
                     where au.publish_date <= :now and hit.audioId = au.id and hit.startDate = :startDate order by rank asc";
            $statement = $connection->prepare($queryString);
            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'), ':startDate'=>date_format($startDate,'Y-m-d')));
            $arr['audios'] = $statement->fetchAll();
        }

        $arr['days'] = $this->daysForYear($controller, date_format($startDate,'Y'));

        return $arr;

    }

    public function daysForYear($controller,$year){
        $em = $controller->getDoctrine()->getManager();
        $queryString = 'select startDate as startDate from msc_chart_'.$year.' group by startDate order by startDate asc';
        $statement = $em->getConnection()->prepare($queryString);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

} 