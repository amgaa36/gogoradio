<?php

namespace music\WebBundle\Box;

class NewBox {
    public function getNewAudioAndAlbum($controller, $page)
    {

        $arr = array();

        $pageSize = 50;
        $offset = ($page-1)*$pageSize;
        $now = new \DateTime('now');

        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();

        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $controller->getUser();
            $myid = $user->getId();
            $statement = $connection->prepare(
                "select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.listen_count, au.is_lyrics, au.is_video, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, au.hitone as hitone,
                 userlike.liked as islike
                    from msc_audio au
                    left join (select audio_id as audid, '1' as liked from msc_user_audio where user_id = ?) as userlike on userlike.audid = au.id
                    where au.publish_date <= ? and au.before_album is not true
                    union all
                select al.id as id, al.album_name as name, COALESCE (al.album_img,al.artist_img) as img, al.id as url, al.publish_date, al.track_number as listen_count, al.is_mongolian as is_lyrics,al.is_mongolian as is_video, al.artist_name_html as artist, al.artist_name as artists, '2' as content_type,null as hitone,
                 userlike.liked as islike
                    from msc_album al
                    left join (select album_id as albid, '1' as liked from msc_user_album where user_id = ?) as userlike on userlike.albid = al.id
                    where al.publish_date <= ?
                    order by publish_date DESC, content_type ASC,artist ASC
                    limit ?,?");
            $statement->bindValue(1, $myid, 'integer');
            $statement->bindValue(2, $now, 'datetime');
            $statement->bindValue(3, $myid, 'integer');
            $statement->bindValue(4, $now, 'datetime');
            $statement->bindValue(5, $offset, 'integer');
            $statement->bindValue(6, $pageSize, 'integer');
            $statement->execute();

        }else{
            $statement = $connection->prepare(
                "select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.listen_count, au.is_lyrics, au.is_video, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, null as islike, au.hitone as hitone
                    from msc_audio au
                    where au.publish_date <= ? and au.before_album is not true
                    union all
                select al.id as id, al.album_name as name, COALESCE (al.album_img,al.artist_img) as img, al.id as url, al.publish_date, al.track_number as listen_count, al.is_mongolian as is_lyrics,al.is_mongolian as is_video, al.artist_name_html as artist, al.artist_name as artists, '2' as content_type,null as islike, null as hitone
                    from msc_album al
                    where al.publish_date <= ?
                    order by publish_date DESC, content_type ASC,artist ASC
                    limit ?,?");

            $statement->bindValue(1, $now, 'datetime');
            $statement->bindValue(2, $now, 'datetime');
            $statement->bindValue(3, $offset, 'integer');
            $statement->bindValue(4, $pageSize, 'integer');
            $statement->execute();
        }

        $results = $statement->fetchAll();
        $arr['new'] = $results;

        return $arr;
    }


    public function getNewForHome($controller){

        $arr = array();

        $now = new \DateTime('now');

        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();

        if (true === $controller->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $controller->getUser();
            $myid = $user->getId();
            $statement = $connection->prepare(
                "select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.listen_count, au.is_lyrics, au.is_video, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type,
                 userlike.liked as islike, au.hitone as hitone
                    from msc_audio au
                    left join (select audio_id as audid, '1' as liked from msc_user_audio where user_id = ?) as userlike on userlike.audid = au.id
                    where au.publish_date <= ? and au.before_album is not true
                    union all
                select al.id as id, al.album_name as name, COALESCE (al.album_img,al.artist_img) as img, al.id as url, al.publish_date, al.track_number as listen_count, al.is_mongolian as is_lyrics,al.is_mongolian as is_video, al.artist_name_html as artist, al.artist_name as artists, '2' as content_type,
                 userlike.liked as islike, null as hitone
                    from msc_album al
                    left join (select album_id as albid, '1' as liked from msc_user_album where user_id = ?) as userlike on userlike.albid = al.id
                    where al.publish_date <= ?
                    order by publish_date DESC, content_type ASC,artist ASC
                    limit 20");
            $statement->bindValue(1, $myid, 'integer');
            $statement->bindValue(2, $now, 'datetime');
            $statement->bindValue(3, $myid, 'integer');
            $statement->bindValue(4, $now, 'datetime');
            $statement->execute();

        }else{
            $statement = $connection->prepare(
                "select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.listen_count, au.is_lyrics, au.is_video, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, null as islike, au.hitone as hitone
                    from msc_audio au
                    where au.publish_date <= ? and au.before_album is not true
                    union all
                select al.id as id, al.album_name as name, COALESCE (al.album_img,al.artist_img) as img, al.id as url, al.publish_date, al.track_number as listen_count, al.is_mongolian as is_lyrics,al.is_mongolian as is_video, al.artist_name_html as artist, al.artist_name as artists, '2' as content_type,null as islike, null as hitone
                    from msc_album al
                    where al.publish_date <= ?
                    order by publish_date DESC, content_type ASC,artist ASC
                    limit 20");

            $statement->bindValue(1, $now, 'datetime');
            $statement->bindValue(2, $now, 'datetime');
            $statement->execute();
        }

        $results = $statement->fetchAll();
        $arr['new'] = $results;

        return $arr;

    }


    public function getNewAudio($controller, $uid=0) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $connection = $em->getConnection();

        if($uid == 0) {



            $statement = $connection->prepare(
                "select au.id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.is_lyrics, au.is_video, au.artist_name as artists, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, null as islike, au.hitone as hitone
                    from msc_audio au
                    where au.publish_date <= :now
                    order by au.publish_date DESC, content_type ASC, au.artist_name ASC
                    limit 15");

            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }
        else {
            $statement = $connection->prepare(
                "select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as url, au.publish_date, au.is_lyrics, au.is_video, au.artist_name as artists, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, au.hitone as hitone,
                 userlike.liked as islike
                    from msc_audio au
                    left join (select audio_id as audid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on userlike.audid = au.id
                    where au.publish_date <= :now
                    order by au.publish_date DESC, content_type ASC, au.artist_name ASC
                    limit 15");

            $statement->execute(array(':uid' => $uid, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }

        $results = $statement->fetchAll();

        $arr['songs'] = $results;
        return $arr;
    }

    public function getNewAlbum($controller, $uid=0) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $connection = $em->getConnection();

        if($uid == 0) {
            $statement = $connection->prepare(
                "select al.id as id, al.album_name as name, al.album_img as img, al.album_img as img1, al.id as url, al.publish_date, al.artist_name as artists, al.artist_name_html as artist, '2' as content_type,null as islike
                    from msc_album al
                    where al.publish_date <= :now
                    order by publish_date DESC, content_type ASC,artist ASC
                    limit 20");

            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }
        else {
            $statement = $connection->prepare(
                "select al.id as id, al.album_name as name, al.album_img as img, al.album_img as img1, al.id as url, al.publish_date, al.artist_name as artists, al.artist_name_html as artist, '2' as content_type,
                 userlike.liked as islike
                    from msc_album al
                    left join (select album_id as albid, '1' as liked from msc_user_album where user_id = :uid) as userlike on userlike.albid = al.id
                    where al.publish_date <= :now
                    order by al.publish_date DESC, content_type ASC, al.artist_name ASC
                    limit 20");

            $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        }

        $results = $statement->fetchAll();

        $arr['albums'] = $results;

        return $arr;
    }
}



