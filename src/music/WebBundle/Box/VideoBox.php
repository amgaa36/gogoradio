<?php
/**
 * Created by PhpStorm.
 * User: Amgaa
 * Date: 3/9/15
 * Time: 3:09 PM
 *
 * @author tsetsee
 * @date 2015/03/18 16:27 PM
 */
namespace music\WebBundle\Box;

use Symfony\Component\DependencyInjection\Container;

class VideoBox
{
    public function getVideos(Container $container, $option = array())
    {
        $config = array_merge(array(
            'orderBy'    => 'new',
            'orderType' => 'DESC',
            'page'     => 1,
            'pagesize' => 10,
            'em' => 'default',
        ), $option);

        $config['page'] = intval($config['page']);
        $config['pagesize'] = intval($config['pagesize']);

        $em = $container->get('doctrine')->getManager($config['em']);

        $qb = $em->getRepository('musicCmsBundle:Video')->createQueryBuilder('e');
        $qb->where($qb->expr()->isNotNull('e.publish_date'))
            ->andWhere($qb->expr()->lte('e.publish_date', ':now'))
            ->setParameter('now',new \DateTime('now'))
            ;

        if($config['orderBy'] == 'new') {
            $qb->orderBy('e.publish_date', $config['orderType']);
        } else {
            $qb->orderBy('e.view', $config['orderType']);
        }
        $qb_total = clone $qb;
        $total = $qb_total->select($qb_total->expr()->count('e.id'))->getQuery()->getSingleScalarResult();
        $config['total'] = intval($total);
        $videos = $qb
            ->setFirstResult(($config['page'] - 1) * $config['pagesize'])
            ->setMaxResults($config['pagesize'])
            ->getQuery()
            ->getArrayResult();

        foreach ($videos as &$video) {
            $video['href'] = $container->get('router')->generate('web_menu_video_info', array(
                'id' => $video['id'],
            ));
        }

        return array(
            'videos'       => $videos,
            'video_config' => $config,
        );
    }


    public function getHomeVideos(Container $container){
        $em = $container->get('doctrine')->getManager();
        $qb = $em->getRepository('musicCmsBundle:Video')->createQueryBuilder('e');
        $qb->where($qb->expr()->isNotNull('e.publish_date'))
            ->andWhere($qb->expr()->lte('e.publish_date', ':now'))
            ->setParameter('now',new \DateTime('now'))
        ;
        $qb->orderBy('e.publish_date','DESC');
        $videos = $qb
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult();

        foreach ($videos as &$video) {
            $video['href'] = $container->get('router')->generate('web_menu_video_info', array(
                'id' => $video['id'],
            ));
        }

        return array(
            'videos'       => $videos,
        );
    }

    public function getVideoInfo(Container $container, $id) {
        
    }
}
