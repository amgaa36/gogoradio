<?php

namespace music\WebBundle\Box;


class NewsBox {
    public function getNews($controller, $count) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $news = $em->getRepository('musicCmsBundle:News')->createQueryBuilder('n')
            ->select('n.publish_date, n.title, n.img, n.url, 6 as content_type')
            ->where('n.publish_date <= :now')
            ->andWhere('n.expire_date > :now or n.expire_date is null')
            ->setParameter('now', new \DateTime())
            ->orderBy('n.publish_date', 'DESC')
            ->setMaxResults($count)
            ->getQuery()
            ->getArrayResult();

        $arr['news'] = $news;

        return $arr;
    }


    public function getHomeNews($controller) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $news = $em->getRepository('musicCmsBundle:News')->createQueryBuilder('n')
            ->select('n.publish_date, n.title, n.img, n.url, 6 as content_type')
            ->where('n.publish_date <= :now')
            ->andWhere('n.expire_date > :now or n.expire_date is null')
            ->setParameter('now', new \DateTime())
            ->orderBy('n.publish_date', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult();

        $arr['news'] = $news;

        return $arr;
    }

} 