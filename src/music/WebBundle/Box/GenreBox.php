<?php

namespace music\WebBundle\Box;


class GenreBox {
    public function getGenres($controller) {
        $arr = array();

        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:Genre')->createQueryBuilder('g');

        $countQueryBuilder = clone $qb;
        $count = $countQueryBuilder->select('count(g.id)')->getQuery()->getSingleScalarResult();

        $genres = $qb->select('g.id, g.name, 5 as content_type')
            ->where('g.publish_date <= :now')
            ->setParameter('now', new \DateTime())
            ->setFirstResult(rand(0, $count - 1))
            ->setMaxResults(20)
            ->getQuery()
            ->getArrayResult();

        $arr['genres'] = $genres;

        return $arr;
    }
} 