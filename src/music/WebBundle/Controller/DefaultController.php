<?php

namespace music\WebBundle\Controller;

use Assetic\Filter\JSMinFilter;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUser;
use music\CmsBundle\Entity\User;
use music\LoginBundle\Controller\LoginUserProvider;
use music\WebBundle\Box\ArtistBox;
use music\WebBundle\Box\GenreBox;
use music\WebBundle\Box\HitBox;
use music\WebBundle\Box\GstatBox;
use music\WebBundle\Box\RadioBox;
use music\WebBundle\Box\VideoBox;
use music\WebBundle\Box\LoginBox;
use music\WebBundle\Box\NewBox;
use music\WebBundle\Box\NewsBox;
use music\WebBundle\Box\PlaylistBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Filesystem\Filesystem;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $arr = array();

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {

            $newBox = new NewBox();
            $arr += $newBox->getNewForHome($this);


            $playlistBox = new PlaylistBox();
            $arr += $playlistBox->getPlaylist($this);

            $artistBox = new ArtistBox();
            $arr += $artistBox->getArtists($this);

            $newsBox = new NewsBox();
            $arr += $newsBox->getNews($this, 20);

            $genreBox = new GenreBox();
            $arr += $genreBox->getGenres($this);
        } else {


            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }

//
            $newBox = new NewBox();
            $arr += $newBox->getNewForHome($this);

            $playlistBox = new PlaylistBox();
            $arr += $playlistBox->getPlaylist($this, $user->getId());

            $artistBox = new ArtistBox();
            $arr += $artistBox->getArtists($this, $user->getId());

            $newsBox = new NewsBox();
            $arr += $newsBox->getNews($this, 20);


        }

        $hitBox = new HitBox();
        $arr += $hitBox->getTasteChart($this);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);



        return $this->render('musicWebBundle:Default:default.html.twig', $arr);
    }

    public function aboutAction(){
        return $this->render('musicWebBundle:Default:about.html.twig');
    }


    public function tosAction(){
        return $this->render('musicWebBundle:Default:term-of-service.html.twig');
    }



    public function homeAction(Request $request){

        $useragents = $request->headers->get('User-Agent');
        if (strpos($useragents, 'iPhone') !== false || strpos($useragents, 'iphone') !== false || strpos($useragents, 'ipod') !== false || strpos($useragents, 'iPod') !== false) {
            return $this->render('musicWebBundle:Default:iphone-screen.html.twig');
        }else if(strpos($useragents, 'Android') !== false || strpos($useragents, 'android') !== false){
            return $this->render('musicWebBundle:Default:android-screen.html.twig');
        }

        $guide = false;
//        if($user->getLoginType() != "gogo"){
            if($request->getSession()->get('is_new')) {
                $guide = true;
                $request->getSession()->remove('is_new');
            }
//        }

        $arr = array();
        $radioBox = new RadioBox();
        $arr = $arr + $radioBox->getGenres($this->container);


        $videoBox = new VideoBox();
        $arr = array_merge($arr, $videoBox->getHomeVideos($this->container));

        $newsBox = new NewsBox();
        $arr = array_merge($arr, $newsBox->getHomeNews($this));

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);
        $arr['guide'] = $guide;
        return $this->render('musicWebBundle:Default:default.html.twig',$arr);


    }

    public function playerAction()
    {
        return $this->render('musicWebBundle:Default:player.html.twig');
    }


}
