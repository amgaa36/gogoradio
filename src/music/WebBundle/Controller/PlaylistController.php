<?php
/**
 * Created by PhpStorm.
 * User: Amgaa
 * Date: 1/26/15
 * Time: 9:19 AM
 */

namespace music\WebBundle\Controller;

use music\CmsBundle\Entity\Playlist;
use music\CmsBundle\Entity\PlaylistAudio;
use music\WebBundle\Box\LoginBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\PlaylistBox;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use music\WebBundle\Box\GstatBox;

use music\WebBundle\Box\NewBox;
use Symfony\Component\Security\Core\User\UserInterface;

class PlaylistController extends Controller{

    public function indexAction(Request $request)
    {
        $arr = array("menu"=>2,"ttab"=>1);
        $playlistBox = new PlaylistBox();
        $arr += $playlistBox->getNewPlaylist($this);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu:play-list.html.twig', $arr);
    }


    public function topAction(){

        $arr = array("menu"=>2,"ttab"=>2);
        $playlistBox = new PlaylistBox();
        $arr += $playlistBox->getTopPlaylist($this);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu:play-list.html.twig', $arr);
    }

    public function getPageAction(Request $request){
        $arr = array();
        $pageNum = $request->get('pageNum');
        $type = $request->get('type');
        $playlistBox = new PlaylistBox();

        $arr += $playlistBox->getPlaylistPage($this,$pageNum,$type);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:inc:thumb-page.html.twig',$arr);
    }


    public function detailAction(Request $request, $id)
    {
        $arr = array("menu"=>2);


        $playlistBox = new PlaylistBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
            $arr += $playlistBox->playlistDetail($this, $id, $this->getUser()->getId());
        }else{
            $arr += $playlistBox->playlistDetail($this, $id);
        }


        //facebook bot
//        if(preg_match('/(facebookexternalhit)/i', $request->headers->get('user-agent'))
//            || $request->get('fbtest')=='yes'){
//            $twig = 'musicWebBundle:fb-detail:fb-song.html.twig';
//
//            return $this->render($twig, $arr);
//        }
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:detail:detail-template.html.twig', $arr);
    }





    public function getMyPlaylistAction(Request $request) {
        $arr = array();
        $twig = 'musicWebBundle:inc:playlist-list.html.twig';

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }

        $user = $this->getUser();
        $audioId = $request->get ( "audioId" );

        $arr["audio_id"] = $audioId;

        $playlistBox = new PlaylistBox();
        $arr += $playlistBox->getMyPlaylist($this, $user->getId());


        return $this->render($twig, $arr);
    }

    public function createAction(Request $request)
    {

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }

        $user = $this->getUser();
        $name = $request->get ( "name" );
        $isPublic = $request->get( "isPublic" );
        $description = $request->get ( "description" );

        if ($name == null || strlen ( trim($name)) == 0)
            throw $this->createNotFoundException("name is null or length is zero");

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('musicCmsBundle:Playlist')->findBy(array('name'=>$name));
        if(count($entities) > 0){
            return new JsonResponse(array('status'=>'error','desc'=>'Өмнө нь үүсгэсэн байна.'));
        }

        $playlist = new Playlist();
        $playlist->user = $user;
        $playlist->name = $name;
        $playlist->user_name = $user->getLoginName();
        if ($name != null && strlen ( trim($name)) > 0)
            $playlist->description = $description;
        $playlist->created_date = new \DateTime();
        $playlist->publish_date = new \DateTime();
        if($isPublic)
            $playlist->is_public = true;
        else
            $playlist->is_public = false;
        $playlist->type = 1;
        $playlist->duration = 0;
        $playlist->track_number = 0;
        $playlist->likeCount = 0;

        $playlist->createDate();

        $em->persist($playlist);
        $em->flush();

       
        return new JsonResponse(array("pid"=>$playlist->getId(), "status"=>'success'));
    }


    public function addAudioAction(Request $request)
    {

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }

        $playlistId = $request->get ( "playlistId" );
        $audioId = $request->get ( "audioId" );

        $em = $this->getDoctrine()->getManager();

        $playlist = $em->getRepository ( 'musicCmsBundle:Playlist' )->find ( $playlistId );

        if(!$playlist) {
            throw $this->createNotFoundException("not found playlist id=$playlistId");
        }

        $audio = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $audioId );

        if(!$audio) {
            throw $this->createNotFoundException("not found audio id=$audioId");
        }

        $connection = $em->getConnection();

        $statement = $connection->prepare("select id from msc_playlist_audio
            where playlist_id = :playlistid and audio_id = :audioid");

        $statement->execute(array('playlistid' => $playlist->getId(), ':audioid' => $audio->getId()));
        $results = $statement->fetchAll();

        if($results && count($results) > 0) {

        }
        else {
            $playlist->track_number = $playlist->track_number + 1;
            $playlist->duration += $audio->duration;
            if(!$playlist->img)
                if($audio->img)
                    $playlist->img = $audio->img;
                elseif($audio->album_img)
                    $playlist->img = $audio->album_img;
                elseif($audio->artist_img)
                    $playlist->img = $audio->artist_img;

            $em->persist($playlist);
            $em->flush();

            $playlistAudio = new PlaylistAudio();
            $playlistAudio->playlist = $playlist;
            $playlistAudio->audio = $audio;
            $playlistAudio->audio_orders = $playlist->track_number;

            $em->persist($playlistAudio);
            $em->flush();
        }

        return new JsonResponse(array('status'=>'success'));
    }

    public function addMultipleAudioAction(Request $request)
    {

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }


        $playlistId = $_REQUEST['playlistId'];
        $audios = $request->get ( "audioIds" );

        $em = $this->getDoctrine()->getManager();

        $playlist = $em->getRepository ( 'musicCmsBundle:Playlist' )->find ( $playlistId );

        if(!$playlist) {
            throw $this->createNotFoundException("not found playlist id=$playlistId");
        }


        foreach($audios as $audioId){
            $audio = $em->getRepository('musicCmsBundle:Audio')->find($audioId);

            if (!$audio) {
                throw $this->createNotFoundException("not found audio id=$audioId");
            }
            if(!$playlist->img)
                if($audio->img)
                    $playlist->img = $audio->img;
                elseif($audio->album_img)
                    $playlist->img = $audio->album_img;
                elseif($audio->artist_img)
                    $playlist->img = $audio->artist_img;
            $playlist->duration += $audio->duration;
            $playlistAudio = new PlaylistAudio();
            $playlistAudio->playlist = $playlist;
            $playlistAudio->audio = $audio;
            $playlistAudio->audio_orders = $playlist->track_number;
            $playlist->track_number++;

            $em->persist($playlistAudio);
        }

        $em->persist($playlist);
        $em->flush();
        return new JsonResponse(array('status'=>'success'));
    }

    public function getAllAudioAction(Request $request)
    {
        $playlistId = $request->get ( "playlistId" );

        $em = $this->getDoctrine()->getManager();

        $connection = $em->getConnection();

        $statement = $connection->prepare("select a.id, a.audio_name as name, a.is_lyrics, a.is_video,a.artist_name as artist_name, COALESCE (a.audio_img,a.album_img,a.artist_img) as img, a.file_url from msc_playlist_audio as pa left join msc_audio as a on a.id = pa.audio_id where pa.playlist_id = :pid order by pa.audio_orders asc");

        $statement->execute(array('pid' => $playlistId));
        $results = $statement->fetchAll();

        return new JsonResponse($results);
    }


    public function deleteAction(Request $request){

        $playlistId = $request->get ( "playlistId" );
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return new JsonResponse(array('success'=>false,'detail'=>'Хэрэглэгч нэвтрээгүй байна.'));
        }
        $loggedUserId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $playlist = $em->getRepository ( 'musicCmsBundle:Playlist' )->find( $playlistId );
        if($loggedUserId == $playlist->user->getId()){
            $entities = $em->getRepository('musicCmsBundle:PlaylistAudio')->findBy(array('playlist'=>$playlist));
            if(count($entities) > 0){
                foreach($entities as $entity){
                    $em->remove($entity);
                    $em->flush();
                }
            }

            $em->remove($playlist);
            $em->flush();
            return new JsonResponse(array('success'=>true,'detail'=>'returned'));
        }
        return new JsonResponse(array('success'=>false,'detail'=>'user is not owner'));
    }

    public function removeAudioAction(Request $request){
        $playlistId = $request->get ( "playlistId" );
        $audioId = $request->get("audioId");
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return new JsonResponse(array('success'=>false,'detail'=>'Хэрэглэгч нэвтрээгүй байна.'));
        }
        $loggedUserId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $playlist = $em->getRepository ( 'musicCmsBundle:Playlist' )->find( $playlistId );
        if($loggedUserId == $playlist->user->getId()){
            $audio = $em->getRepository ( 'musicCmsBundle:Audio' )->find( $audioId );
            $entities = $em->getRepository('musicCmsBundle:PlaylistAudio')->findBy(array('playlist'=>$playlist,'audio'=>$audio));

            if(count($entities) > 0){
                foreach($entities as $entity){
                    $em->remove($entity);
                    $em->flush();
                }
                $playlist->setTrackNumber($playlist->getTrackNumber()-1);
                $playlist->setDuration($playlist->getDuration() - $audio->getDuration());
                $em->persist($playlist);
                $em->flush();


                return new JsonResponse(array('success'=>true,'duration'=>$playlist->getDuration(), 'trackNumber'=>$playlist->getTrackNumber()));
            }

        }
        return new JsonResponse(array('success'=>false,'detail'=>'user is not owner'));
    }
}