<?php

namespace music\WebBundle\Controller;

use Assetic\Filter\JSMinFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\PlaylistBox;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use music\WebBundle\Box\GstatBox;

class SearchController extends Controller{

    public function indexAction(){
        $arr = array("left_menu"=>1);

        return $this->render('musicWebBundle:Default:search.html.twig',$arr);
    }



    public function searchAction(Request $request){

        $arr = array('left_menu'=>1);

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $queryencoded = $request->get('q');
        $query = urldecode($queryencoded);

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $userId = $this->getUser()->getId();

            $arStatement = $connection->prepare("select a.id as id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, userlike.liked as islike from msc_artist a
            left join (select artist_id as arid, '1' as liked from msc_user_artist where user_id = :uid) as userlike on userlike.arid = a.id
            where a.artist_name like :arname and a.published_date < :now and a.published_date is not null limit 30");

            $auStatement = $connection->prepare("select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as file_url, au.is_lyrics, au.is_video, au.publish_date, au.artist_name_html as artist, au.hitone as hitone, au.artist_name as artists, au.listen_count, au.duration, '1' as content_type, userlike.liked as islike from msc_audio au
            left join (select audio_id as auid, '1' as liked from msc_user_audio where user_id = :uid) as userlike on au.id = userlike.auid
            where (au.audio_name like :arname or au.audio_name_eng like :arname) and au.publish_date < :now limit 30");

            $alStatement = $connection->prepare("select al.id as id, al.album_name as name, al.album_img as img, al.artist_name_html as artist, '2' as content_type, userlike.liked as islike from msc_album al
            left join (select album_id as alid, '1' as liked from msc_user_album where user_id = :uid) as userlike on userlike.alid = al.id
            where al.album_name like :arname and al.publish_date < :now limit 30");

            $plStatement = $connection->prepare("select pl.id as id, pl.playlist_name as name, pl.playlist_img as img, pl.user_name as user_name, pl.user_id as user_id, '3' as content_type, userlike.liked as islike from msc_playlist pl
            left join (select playlist_id as plid, '1' as liked from msc_user_playlist where user_id = :uid) as userlike on userlike.plid = pl.id
            where pl.playlist_name like :arname limit 30");

            $arStatement->execute(array('arname' => '%'.$query.'%', 'uid'=>$userId, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $auStatement->execute(array('arname' => '%'.$query.'%', 'uid'=>$userId, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $alStatement->execute(array('arname' => '%'.$query.'%', 'uid'=>$userId, ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $plStatement->execute(array('arname' => '%'.$query.'%', 'uid'=>$userId));
        }
        else{
            $arStatement = $connection->prepare("select a.id as id, a.artist_name as name, a.artist_img as img, a.like_count, 4 as content_type, null as islike from msc_artist a where a.artist_name like :arname and a.published_date < :now and a.published_date is not null limit 30");
            $auStatement = $connection->prepare("select au.id as id, au.audio_name as name, COALESCE (au.audio_img,au.album_img,au.artist_img) as img,au.duration, au.hitone as hitone, au.listen_count, au.file_url as file_url, au.is_lyrics, au.is_video, au.publish_date, au.artist_name_html as artist, au.artist_name_html as artist, au.artist_name as artists, '1' as content_type, null as islike from msc_audio au where (au.audio_name like :arname or au.audio_name_eng like :arname) and au.publish_date < :now limit 30");
            $alStatement = $connection->prepare("select al.id as id, al.album_name as name, al.album_img as img, al.artist_name_html as artist, '2' as content_type,null as islike from msc_album al where al.album_name like :arname and al.publish_date < :now limit 30");
            $plStatement = $connection->prepare("select pl.id as id, pl.playlist_name as name, pl.playlist_img as img, pl.user_name as user_name, pl.user_id as user_id, '3' as content_type, null as islike from msc_playlist pl where pl.playlist_name like :arname limit 30");
            $arStatement->execute(array('arname' => '%'.$query.'%', ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $auStatement->execute(array('arname' => '%'.$query.'%', ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $alStatement->execute(array('arname' => '%'.$query.'%', ':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
            $plStatement->execute(array('arname' => '%'.$query.'%'));
        }

        $qb = $em->getRepository('musicCmsBundle:Video')->createQueryBuilder('video');
        $videos = $qb
            ->where('video.name LIKE :q')
            ->andWhere($qb->expr()->isNotNull('video.publish_date'))
            ->andWhere($qb->expr()->lte('video.publish_date', ':now'))
            ->setParameter('now', new \DateTime('now'))
            ->setParameter('q','%'.$query.'%')
            ->getQuery()
            ->getArrayResult()
        ;

        $arr['videos'] = $videos;

        $artists = $arStatement->fetchAll();
        $arr['artists'] = $artists;

        $audios = $auStatement->fetchAll();
        $arr['audios']= $audios;

        $albums = $alStatement->fetchAll();
        $arr['albums']= $albums;

        $playlists = $plStatement->fetchAll();
        $arr['playlist']= $playlists;

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);
        $arr['query'] = $query;

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }


        return $this->render('musicWebBundle:sub-menu:search-result.html.twig',$arr);

    }
}