<?php

namespace music\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\HitBox;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use music\WebBundle\Box\GstatBox;

class HitController extends Controller
{
    public function indexAction()
    {
        $arr = array("menu"=>3,"left_menu"=>0);
        $hitBox = new HitBox();
        $arr += $hitBox->getLastChart($this);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }

        return $this->render('musicWebBundle:sub-menu:hit.html.twig', $arr);
    }

    public function getDaysForYearAction(Request $request){
        $year = $request->get('year');
        $hitBox = new HitBox();
        $result = $hitBox->daysForYear($this,$year);
        return new JsonResponse($result);
    }


    public function showChartAction(Request $request){
        $arr = array("menu"=>3);
        $startDate = new \DateTime($request->get('startDate'));
        $hitBox = new HitBox();
        $arr += $hitBox->getChartWithDate($this,$startDate);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }

        return $this->render('musicWebBundle:sub-menu:hit.html.twig', $arr);
    }
}
