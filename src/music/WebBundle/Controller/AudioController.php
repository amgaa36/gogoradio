<?php

namespace music\WebBundle\Controller;

use music\WebBundle\Box\AudioBox;
use music\WebBundle\Box\GstatBox;
use music\WebBundle\Box\LoginBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\ArtistBox;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class AudioController extends Controller
{
    public function detailAction(Request $request, $id)
    {
        $arr = array();


        $audioBox = new AudioBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $arr += $audioBox->audioDetail($this, $id, $this->getUser()->getId());
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }
        else
            $arr += $audioBox->audioDetail($this, $id);

        //facebook bot
        if(preg_match('/(facebookexternalhit)/i', $request->headers->get('user-agent'))
            || $request->get('fbtest')=='yes'){
            $twig = 'musicWebBundle:fb-detail:fb-song.html.twig';

            return $this->render($twig, $arr);
        }


        //twitter bot
        if(preg_match('/(Twitterbot)/i', $request->headers->get('user-agent'))){
            $twig = 'musicWebBundle:tw-detail:tw-song.html.twig';

            return $this->render($twig, $arr);
        }

        return $this->redirect($this->generateUrl('web_homepage'));
    }


    public function shareAction(Request $request, $id){
        $arr = array();
        $audioBox = new AudioBox();
        $arr['song'] = $audioBox->getSong($this, $id);


        if(preg_match('/(facebookexternalhit)/i', $request->headers->get('user-agent'))
            || $request->get('fbtest')=='yes'){
            $twig = 'musicWebBundle:fb-detail:fb-song.html.twig';

            return $this->render($twig, $arr);
        }

        //twitter bot
        if(preg_match('/(Twitterbot)/i', $request->headers->get('user-agent'))){
            $twig = 'musicWebBundle:tw-detail:tw-song.html.twig';

            return $this->render($twig, $arr);
        }

        return $this->redirect($this->generateUrl('web_homepage'));
    }


    public function getLyricsAction(Request $request){
        $audioBox = new AudioBox();
        $lyrics = $audioBox->getLyric($this, $request->get('audioId'));
        return new JsonResponse($lyrics);
    }

    public function addListenCountAction(Request $request){
        $audioId = $request->get('audioId');
        $em = $this->getDoctrine()->getManager();
        $audio = $em->getRepository('musicCmsBundle:Audio')->find($audioId);
        $audio->listen_count++;
        $em->persist($audio);
        $em->flush();

        $radios = $em->getRepository('musicCmsBundle:GenreAudio')->findBy(array('audio'=>$audio));
        foreach($radios as $radio){
            $radio->genre->listen_count++;
            $em->persist($radio);
        }
        $em->flush();


        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $userid = $this->getUser()->getId();
        }else{
            $userid = null;
        }
        $connection = $em->getConnection();
        $statement = $connection->prepare("INSERT INTO msc_user_log (user_id, audio_id, listen_date) VALUES (:userid, :audioid, :now)");
        $statement->execute(array('userid' => $userid,'audioid'=>$audioId, 'now'=>date_format(new \DateTime(), 'Y-m-d H:i:s')));
        return new JsonResponse();
    }

}
