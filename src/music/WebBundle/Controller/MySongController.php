<?php

namespace music\WebBundle\Controller;

use music\CmsBundle\Entity\UserAlbum;
use music\CmsBundle\Entity\UserArtist;
use music\CmsBundle\Entity\UserAudio;
use music\CmsBundle\Entity\UserDislike;
use music\CmsBundle\Entity\UserPlaylist;
use music\WebBundle\Box\LoginBox;
use music\WebBundle\Box\MySongBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use music\WebBundle\Box\GstatBox;

class MySongController extends Controller{

    public function indexAction(Request $request) {
        $arr = array("left_menu"=>4);
        $arr["menu"] = 1;

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
            $mySongBox = new MySongBox();
            $arr += $mySongBox->getMyFavoriteSongs($this, $user->getId());
            $arr += $mySongBox->getUserInfo($this, $user->getId());
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }else{
            throw $this->createNotFoundException("no login user");
        }
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:Default:my-song.html.twig',$arr);
    }

    public function likeAction(Request $request) {

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }
        $user = $this->getUser();

        $contentId = $request->get ( "contentId" );

        $em = $this->getDoctrine()->getManager();

        $content = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $contentId );
        if(!$content) {
            throw $this->createNotFoundException("not found content id=$contentId");
        }


        $qb = $em->getRepository('musicCmsBundle:UserAudio')->createQueryBuilder('ua');
        $ua = $qb->select('ua.id')
            ->where('ua.user_id = :uid')
            ->setParameter('uid', $user->getId())
            ->andWhere('ua.audio_id = :cid')
            ->setParameter('cid', $contentId)
            ->setMaxResults(1)
            ->getQuery()
            ->getArrayResult();


        if($ua && count($ua) > 0) {
            return new JsonResponse(array('status'=>true));
        }

        $userContent = new UserAudio();
        $userContent->audio = $content;

        if($userContent) {
            $userContent->user = $em->getReference('musicCmsBundle:User', $user->getId());
            $em->persist($userContent);
            $em->flush();
        }

        $content->setLikeCount($content->getLikeCount()+1);
        $em->persist($content);
        $em->flush();

        return new JsonResponse(array('status'=>true,'like_count'=>$content->getLikeCount()));
    }


    public function unlikeAction(Request $request) {

        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }
        $user = $this->getUser();

        $contentId = $request->get ( "contentId" );

        $em = $this->getDoctrine()->getManager();

        $content = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $contentId );

        if(!$content) {
            return new JsonResponse(array('status'=>false, 'desc'=>'oldsongui'));
        }

        $entities = $em->getRepository('musicCmsBundle:UserAudio')->findBy(array(
                'user_id' => $user->getId(), 'audio_id' => $contentId));


        if($entities && count($entities)>0) {
            foreach($entities as $entity) {
                $content->setLikeCount($content->getLikeCount()-1);
                $em->remove($entity);
                $em->flush();
            }


        }
        $em->persist($content);
        $em->flush();

        return new JsonResponse(array('status'=>true, 'like_count'=>$content->getLikeCount()));
    }


    public function dislikeAction(Request $request){
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }
        $contentId = $request->get('contentId');
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $contentId );
        if(!$content) {
            throw $this->createNotFoundException("not found content id=$contentId");
        }
        $user = $this->getUser();
        $qb = $em->getRepository('musicCmsBundle:UserDislike')->createQueryBuilder('ua');
        $ua = $qb->select('ua.id')
            ->where('ua.user = :u')
            ->setParameter('u', $user)
            ->andWhere('ua.audio = :c')
            ->setParameter('c', $content)
            ->setMaxResults(1)
            ->getQuery()
            ->getArrayResult();
        if($ua && count($ua) > 0) {
            return new JsonResponse(array('status'=>true));
        }

        $userContent = new UserDislike();
        $userContent->audio = $content;
        if($userContent) {
            $userContent->user = $em->getReference('musicCmsBundle:User', $user->getId());
            $em->persist($userContent);
            $em->flush();
        }

        $content->setDislikeCount($content->getDislikeCount()+1);
        $em->persist($content);
        $em->flush();

        return new JsonResponse(array('status'=>true,'dislike_count'=>$content->getDislikeCount()));


    }

    public function unDislikeAction(Request $request){
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException("no login user");
        }

        $contentId = $request->get('contentId');
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $contentId );
        if(!$content) {
            throw $this->createNotFoundException("not found content id=$contentId");
        }
        $user = $this->getUser();
        $qb = $em->getRepository('musicCmsBundle:UserDislike');
        $ua = $qb->findOneBy(array('user'=>$user,'audio'=>$content));
        if($ua && count($ua) > 0) {
            $em->remove($ua);
            $em->flush();
        }
        return new JsonResponse(array('status'=>true));

    }
}