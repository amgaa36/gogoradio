<?php

namespace music\WebBundle\Controller;

use music\CmsBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    public function getAction(Request $request)
    {
        $arr = array(
            'status' => 'success',
            'logged' => true,
        );
        try {
            $path = $request->request->get('path');
            if (!$path) {
                $arr = array(
                    'status' => 'fail',
                    'message' => 'path empty',
                );
                throw new \Exception();
            }

            $em = $this->getDoctrine()->getManager();
            $qb = $em->getRepository('musicCmsBundle:Comment')->createQueryBuilder('e');
            $comments = $qb
                ->select('e as main')
                ->leftJoin('e.user', 'u')
                ->addSelect('u.loginName')
                ->addSelect('u.isPremium')
                ->addSelect('u.userImageUrl')
                ->where($qb->expr()->eq('e.path', ':path'))
                ->setParameter('path', $path)
                ->andWhere($qb->expr()->eq('e.is_show', ':show'))
                ->setParameter('show', true)
                ->setMaxResults(100)
                ->orderBy('e.posted_date', 'DESC')
                ->getQuery()
                ->getArrayResult()
            ;
            $arr['comments'] = $comments;
        }catch(\Exception $e) {
            $arr = array_merge($arr, array(
                    'status' => 'fail',
                    //'message' => $e->getMessage(),
                ));
        }
        return new JsonResponse($arr);
    }

    public function putAction(Request $request)
    {
        $arr = array(
            'status' => 'success',
            'logged' => true,
        );
        try {
            $body = $request->request->get('comment_body');
            if (!is_string($body)) {
                $arr = array(
                    'status' => 'fail',
                    'message' => 'bad body',
                );
                throw new \Exception();
            }
            if (trim($body) === '') {
                $arr = array(
                    'status' => 'fail',
                    'message' => 'empty body',
                );
                throw new \Exception();
            }
            $path = $request->request->get('path');
            if (!$path) {
                $arr = array(
                    'status' => 'fail',
                    'message' => 'path empty',
                );
                throw new \Exception();
            }
            $user = $this->getUser();
            if (!$user) {
                $arr = array(
                    'status' => 'fail',
                    'message' => 'not logged',
                    'logged' => false,
                );
                throw new \Exception();
            }
            $em = $this->getDoctrine()->getManager();
            $comment = new Comment();
            $comment->setUser($em->getReference('musicCmsBundle:User',$user->getId()));
            $comment->setBody($body);
            $comment->setIpAddress($request->getClientIp());
            $comment->setPostedBy($user->getLoginName());
            $comment->setPostedDate(new \DateTime('now'));
            $comment->setPath($path);
            $em->persist($comment);
            $em->flush();

            $arr['date'] = $comment->getPostedDate();
        }catch(\Exception $e) {
            $arr = array_merge($arr, array(
                'status' => 'fail',
                'message' => $e->getMessage(),
            ));
        }
        return new JsonResponse($arr);
    }
}
