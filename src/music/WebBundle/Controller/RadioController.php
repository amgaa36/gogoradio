<?php

namespace music\WebBundle\Controller;

use music\CmsBundle\Entity\AudioBanner;
use music\CmsBundle\Entity\UserBanner;
use music\CmsBundle\Entity\UserDislike;
use music\CmsBundle\Entity\UserSkip;
use music\WebBundle\Box\RadioBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use music\WebBundle\Box\GstatBox;

class RadioController extends Controller{

    public function indexAction(){
        $arr = array();

        $radioBox = new RadioBox();
        $arr = $arr + $radioBox->getGenres($this->container);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:Default:radio.html.twig',$arr);
    }

    public function listenAction(Request $request) {
        $genreid = $request->get('channel');
        $skip = $request->get('skip');
        $audio = $request->get('audio');
        $arr = array();

        $em = $this->getDoctrine()->getManager();
        $systemuser = $this->getUser();
        if(!$systemuser){
            return new JsonResponse(array("status"=>false,'desc'=>'not logged'));
        }
        $userid = $systemuser->getId();
        $user = $em->getReference('musicCmsBundle:User', $userid);
        $genre = $em->getReference('musicCmsBundle:Genre',$genreid);

        if(!$user){
            throw $this->createNotFoundException("not found user id=$userid");
        }
        if(!$genre){
            throw $this->createNotFoundException("not found genre id=$genreid");
        }

        if($audio){
            $oldaudio = $em->getReference('musicCmsBundle:Audio',$audio);
        }else{
            $oldaudio = null;
        }


        if($skip == true){


            $skipped = $request->get('skipped');
            $skipped_genre = $em->getReference('musicCmsBundle:Genre',$skipped);


            $oneTimeAgosk = (new \DateTime())->modify('-1 hours');
            $qbsk = $em->getRepository('musicCmsBundle:UserSkip')->createQueryBuilder('us');

            $pastSkipssk = $qbsk->where('us.skipDate > :olddate')
                ->andWhere('us.user = :user')
                ->andWhere('us.genre = :genre')
                ->setParameter('olddate',$oneTimeAgosk)
                ->setParameter('user',$user)
                ->setParameter('genre',$skipped_genre)
                ->getQuery()
                ->getResult();

//            if(count($pastSkipssk) > 5){
//                return new JsonResponse(array('status'=>false,'skip'=>'olmaz'));
//            }else{
                $thisSkip = new UserSkip();
                $thisSkip->setUser($user);
                $thisSkip->setGenre($skipped_genre);
                $thisSkip->setSkipDate(new \DateTime('now'));
                $thisSkip->setAudio($oldaudio);
                $em->persist($thisSkip);
                $em->flush();
//            }
        }

        $oneTimeAgo = (new \DateTime())->modify('-1 hours');
        $qb = $em->getRepository('musicCmsBundle:UserSkip')->createQueryBuilder('us');
        $qbdelete = $em->createQueryBuilder();
        $pastSkips = $qb->where('us.skipDate > :olddate')
            ->andWhere('us.user = :user')
            ->andWhere('us.genre = :genre')
            ->setParameter('olddate',$oneTimeAgo)
            ->setParameter('user',$user)
            ->setParameter('genre',$genre)
            ->getQuery()
            ->getResult();



        $count = 6-count($pastSkips) >= 0 ? 6-count($pastSkips) : 0;

        $qbdelete->delete('musicCmsBundle:UserSkip','usd')
            ->where($qbdelete->expr()->lte('usd.skipDate',':olddate'))
            ->andWhere('usd.user = :user')
            ->andWhere('usd.genre = :genre')
            ->setParameter(':olddate',$oneTimeAgo)
            ->setParameter('user',$user)
            ->setParameter('genre',$genre)
            ->getQuery()
            ->getResult();

            $arr = $arr + $this->getAudiosByGenre($genre,$pastSkips,$oldaudio);




//        if($user->getStreakCount() > 5){
//            $banner = $this->getBanner($user,$em);
//            if (count($banner) > 0){
//                return new JsonResponse($banner);
//            }
//        }


        $arr = $arr + array('radio_channel' => $genre,'skip_count'=>$count);

        $streaks = $user->getStreakCount();
        $user->setStreakCount($streaks+1);
        $em->persist($user);
        $em->flush();
        return new JsonResponse($arr);


    }



    private function getBanner($user,$em){
        $user->setStreakCount(0);
        $em->persist($user);

        $now = new \DateTime('now');

        $arr = array();

        $qb = $em->getRepository('musicCmsBundle:AudioBanner')->createQueryBuilder('ab');
        $audiobanner = $qb->select('ab')
            ->addSelect('RAND() as HIDDEN rand')
            ->where('ab.startDate < :now')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('ab.startDate'),
                $qb->expr()->lte('ab.startDate',':now')
            ))
            ->setParameter(':now',new \DateTime('now'))
            ->orderBy('rand')
            ->getQuery()
            ->getResult();
        if(count($audiobanner) > 0){
            $arr['status'] = true;
            $arr['t'] = 'b';
            $arr['banner'] = $audiobanner[0];

            $banner = new UserBanner();
            $banner->setUser($user);
            $banner->setListenDate($now);
            $banner->setBanner($audiobanner[0]);
            $em->persist($banner);
            $em->flush();

            return $arr;
        }
    }


    public function getAudiosByGenre($genre, $pastSkips,$oldaudio = null) {
        $arr = array();
        $em = $this->getDoctrine()->getManager();


        if(!$genre->getPublishDate() || $genre->getPublishDate() > new \DateTime('now')) {
            $audios = array("st"=>'yanzal');
        } else {
            $userid = $this->getUser()->getId();
            $user = $em->getReference('musicCmsBundle:User', $userid);

            $subqb = $em->getRepository('musicCmsBundle:UserDislike')->createQueryBuilder('udsk');
            $dislikes = $subqb
                ->select('udsk')
                ->where($subqb->expr()->eq('udsk.user',':user'))
                ->setParameter('user',$user)
                ->getQuery()
                ->getResult();



            $qb = $em->getRepository('musicCmsBundle:GenreAudio')->createQueryBuilder('ga');
            $qb->leftJoin('ga.audio','a')
                ->select('a.id, a.name, coalesce(a.img,a.album_img,a.artist_img) as img, a.album_name, a.artist_name,a.is_lyrics,a.is_video,a.file_url,coalesce(a.hitone,a.hitone_dahilt,a.hitone_badag) as hitone,
                 coalesce(a.unimusic, a.unimusic_dahilt, a.unimusic_badag) as unimusic')
                ->addSelect('RAND() as HIDDEN rand')
                ->where($qb->expr()->eq('ga.genre',':genre'))
                ->setParameter('genre',$genre)
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->lte('a.publish_date', ':now'),
                    $qb->expr()->isNull('a.publish_date')
                ))
                ->setParameter('now', new \DateTime('now'));

            if($oldaudio){

                $qb->andWhere($qb->expr()->neq('a.artist_name',':artist_name'))
                    ->setParameter('artist_name',$oldaudio->getArtistName());
            }


            if(count($pastSkips) > 0){
                $audioIds = array();
                foreach($pastSkips as $skip){
                    if($skip->getAudio()->getId() != null){
                        array_push($audioIds,$skip->getAudio()->getId());
                    }
                }

                $qb->andWhere($qb->expr()->notIn('a.id', ':nots'))
                    ->setParameter('nots',$audioIds);
            }

            if(count($dislikes) > 0){
                $dislikedAudios = array();
                foreach($dislikes as $dislike){

                    array_push($dislikedAudios,$dislike->getAudio());

                }
                $qb->andWhere($qb->expr()->notIn('a', ':nots'))
                    ->setParameter('nots',$dislikedAudios);
            }

            $audios = $qb->orderBy('rand')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

        }
        if(count($audios) > 0){
            $temp = array();
            $audio = $audios[0];
            $connection = $em->getConnection();

            $statement1 = $connection->prepare('select audio_id from msc_user_audio  where msc_user_audio.audio_id = :aid');
            $statement1->execute(array(':aid' =>$userid));
            $qbc = $statement1->fetchAll();
            if(count($qbc) > 0){
                $temp['islike'] = true;
            }else{
                $temp['islike'] = false;
            }

            $statement2 = $connection->prepare('select audio_id from msc_user_dislike  where msc_user_dislike.audio_id = :aid');
            $statement2->execute(array(':aid' =>$userid));
            $qbc = $statement2->fetchAll();
            if(count($qbc) > 0){
                $temp['isdislike'] = true;
            }else{
                $temp['isdislike'] = false;
            }

            $temp = $temp + $audio;
            $arr['radio_channel_audio'] = $temp;
        }else{
            $arr['status'] = false;
            $arr['desc'] = 'Алдаа гарлаа. Өөр радио сонгоно уу.';
        }

        return $arr;
    }


    public function retrieveListAction(){
        $arr = array();

        $radioBox = new RadioBox();
        $arr = $arr + $radioBox->getAllGenres($this->container);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);
        return new JsonResponse($arr);
    }

    public function detailAction(){
        return $this->render('musicWebBundle:Default:radio-detail.html.twig');
    }
}