<?php

namespace music\WebBundle\Controller;

use music\CmsBundle\Entity\UserArtist;
use music\CmsBundle\Entity\UserFollow;
use music\CmsBundle\Entity\PaymentLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\ArtistBox;
use music\WebBundle\Box\MySongBox;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use music\WebBundle\Box\GstatBox;

class UserController extends Controller
{
    private $price;
    private $userid;

    public function detailAction($id=null)
    {
        $arr = array();
        $arr['menu'] = 1;
        $mysongbox = new MySongBox();

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();


            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }


            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }
        }


        $arr += $mysongbox->getMyFavoriteSongs($this,$id, $myid);
        $arr += $mysongbox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);


        return $this->render('musicWebBundle:detail:user-detail.html.twig', $arr);
    }



    public function audiosAction($id=null)
    {
        $arr = array();
        $arr['menu'] = 1;
        $mySongBox = new MySongBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();
            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }


            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }

        }

        $arr += $mySongBox->getMyFavoriteSongs($this,$id , $myid);
        $arr += $mySongBox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu-user:user-audio-list.html.twig', $arr);
    }

    public function playlistsAction($id=null)
    {
        $arr = array("ttab"=>1);
        $arr['menu'] = 2;
        $mySongBox = new MySongBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();
            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }

            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }

        }
        $arr += $mySongBox->getMyPlaylists($this,$id,$myid);
        $arr += $mySongBox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu-user:user-playlist-list.html.twig', $arr);
    }

    public function favPlaylistsAction($id=null)
    {
        $arr = array("ttab"=>2);
        $arr['menu'] = 2;
        $mySongBox = new MySongBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();
            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }

            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }

        }
        $arr += $mySongBox->getFavPlaylists($this,$id,$myid);
        $arr += $mySongBox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu-user:user-playlist-list.html.twig', $arr);
    }

    public function albumsAction($id=null)
    {
        $arr = array();
        $arr['menu'] = 3;
        $mySongBox = new MySongBox();
        $myid = 0;
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();
            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }

            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }

        }
        $arr += $mySongBox->getMyAlbums($this,$id,$myid);
        $arr += $mySongBox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu-user:user-album-list.html.twig', $arr);
    }

    public function currentAction($id=null)
    {
        $arr = array();
        $arr['menu'] = 4;
        $mySongBox = new MySongBox();
        $arr += $mySongBox->getMyCurrentAudios($this,$id);
        $arr += $mySongBox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);


        $user = $this->getUser();
        $now = new \DateTime('now');
        if($user->getExpireDate() > $now){
            $arr += array('isPremium'=>1);
        }


        return $this->render('musicWebBundle:sub-menu-user:user-current-list.html.twig', $arr);
    }

    public function videosAction($id=null)
    {
        $arr = array();
        $arr['menu'] = 5;
        $mySongBox = new MySongBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();
            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }
        }

        $arr += $mySongBox->getUserInfo($this,$id);
        $arr += $mySongBox->getMyVideos($this, $id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu-user:user-video-list.html.twig', $arr);
    }

    public function artistsAction($id=null)
    {
        $arr = array();
        $arr['menu'] = 6;

        $mySongBox = new MySongBox();
        $myid = 0;
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $myid = $this->getUser()->getId();
            if($myid == $id || $id == null){
                $arr['isme'] = true;
                $id = $myid;
                $arr["left_menu"] = 4;
            }
        }
        $arr += $mySongBox->getMyArtists($this,$id,$myid);
        $arr += $mySongBox->getUserInfo($this,$id);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu-user:user-artist-list.html.twig', $arr);
    }

    public function followAction(Request $request){
        $arr = array();
        $arr['success'] = false;
        $userId = $request->get('followId');
        $userType = $request->get('followType');
        $unfollow = $request->get('unfollow');
        $em = $this->getDoctrine()->getManager();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            if($userType == 'artist'){
                $entities = $em->getRepository ('musicCmsBundle:UserArtist')->findBy(array('user_id'=>$me->getId(), 'artist_id'=>$userId));
                if($unfollow=='true'){
                    if($entities) {
                        foreach($entities as $entity){
                            $entity->getArtist()->setLikeCount($entity->getArtist()->getLikeCount()-1);
                            $em->remove($entity);
                        }
                        $em->flush();
                    }

                }else{
                    if(!$entities || count($entities) == 0){
                        $qb = $em->getRepository ( 'musicCmsBundle:Artist' )->find( $userId );
                        if(!$qb) {
                            throw $this->createNotFoundException("not found content id=$userId");
                        }
                        $qb->setLikeCount($qb->getLikeCount()+1);
                        $entity = new UserArtist();
                        $entity->setUser($em->getReference('musicCmsBundle:User', $me->getId()));
                        $entity->setArtist($qb);
                        $entity->setFollowDate(new \DateTime('now'));
                        $em->persist($entity);
                        $em->flush();
                    }
                }

                $qb = $em->getRepository ( 'musicCmsBundle:Artist' )->find( $userId );
                $arr['follower_number'] = $qb->getLikeCount();

            }else{
                $entities = $em->getRepository ('musicCmsBundle:UserFollow')->findBy(array('follower_id'=>$me->getId(), 'leader_d'=>$userId));
                if($unfollow=='true'){
                    if($entities) {
                        foreach($entities as $entity) {
                            $em->remove($entity);
                        }
                        $em->flush();
                    }

                }else{
                    if(!$entities || count($entities) == 0){
                        $qb = $em->getRepository ( 'musicCmsBundle:User' )->find( $userId );
                        $entity = new UserFollow();
                        $entity->setLeaderD($qb);
                        $entity->setFollowerId($me);
                        $em->persist($entity);
                    }
                }

                $em->flush();

                $connection = $em->getConnection();
                $statement = $connection->prepare("select count(*) as cnt from msc_user_follow uf where uf.leader_id = :userid");
                $statement->execute(array(':userid' => $userId));
                $followers = $statement->fetch();
                $arr['follower_number'] = $followers['cnt'];
            }
            $arr['success'] = true;
            return new JsonResponse($arr);
        }
        return new JsonResponse($arr);
    }


    public function subscriptionAction(){
        $arr = array();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $arr['isPremium'] = $me->getIsPremium();
            $arr['price'] = $this->getCurrentPrice();
        }
        return $this->render('musicWebBundle:sub-menu-user:user-subscription.html.twig',$arr);
    }


    public function myInfoAction(){
        $arr = array();
        $mysongbox = new MySongBox();

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $myid = $me->getId();
            $myinfo = $mysongbox->getUserInfo($this,$myid);
            $now = new \DateTime('now');
            if($now < $me->getExpireDate() && $me->getIsPremium() == false){
                $myinfo['user']['isPremium'] = 2;
            }
            $arr += $myinfo;
        }

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);
        return $this->render('musicWebBundle:sub-menu-user:user-myinfo.html.twig', $arr);
    }


    public function myNotifAction(){
        $arr = array();

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $myid = $me->getId();

            $em = $this->getDoctrine()->getManager('default');
            $connection = $em->getConnection();
            $statement = $connection->prepare('select a.audio_name, a.id, a.artist_name_html as artist, usa.artist_id  from msc_audio a, msc_audio_artist as aua, msc_user_artist as usa
 where usa.user_id = :myid and a.publish_date < :now and a.publish_date > usa.followDate and usa.artist_id = aua.artist_id and aua.audio_id = a.id limit 50');
            $statement->execute(array(':myid'=>$myid, ':now' => date_format(new \DateTime('now'), 'Y-m-d H:i:s') ));
            $audios = $statement->fetchAll();
            $me->setNotifDate(new \DateTime('now'));
            $me->setNotifCount(0);
            $em->persist($me);
            $em->flush();

            $arr['notifs']= $audios;
        }

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);
//        return new JsonResponse($arr);
        return $this->render('musicWebBundle:sub-menu-user:user-notification.html.twig', $arr);
    }


    public function getUserNotifCount(){
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $myid = $me->getId();
            $loginDate = $myid->getLoginDate();

            $em = $this->getDoctrine()->getManager('default');
            $connection = $em->getConnection();
            $statement = $connection->prepare('select count(a) as notifcount from msc_audio a, msc_audio_artist as aua, msc_user_artist as usa
 where msc_user_artist.user_id = :myid and a.publish_date > :lastLogin and a.publish_date > usa.followDate and usa.artist_id = aua.artist_id and aua.audio_id = a.id');
            $statement->execute(array(':myid'=>$myid, ':lastLogin' => $loginDate ));
            $count = $statement->fetchAll();
            return $count;
        }
        return 0;
    }

    public function updateUserInfoAction(Request $request){
//        $ovog = $request->get('ovog');
        $ner = $request->get('ner');
        $birth = $request->get('tudur');
        $mail = $request->get('mail');


        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $me->setLoginName($ner);
//            $me->setLastName($ovog);
            $me->setUserBirthday(new \DateTime($birth));
            $me->setUserEmail($mail);
            $em = $this->getDoctrine()->getManager();
            $em->persist($me);
            $em->flush();
            return new JsonResponse(array('success'=>true));
        }
        return new JsonResponse(array('success'=>false));
    }


    public function paymentAction(Request $request){

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $price = $this->getCurrentPrice();
            $dateString = date("Y-m-d");

            $log = new PaymentLog();
            $log->setUser($me);
            $em = $this->getDoctrine()->getManager();
            $em->persist($log);
            $em->flush();
            $productId = $log->getId();
            $hashString = $productId.'|gogo-music-web|'.$me->getId().'|'.$price.'|'.$dateString.'|TNn4A114WfwXNWoFiGuyXA9ab';
            $redirect = 'http://music.gogo.mn/payment/callback?prodId='.$productId;

            $hashedString = hash('sha256',$hashString,false);
            $requestString = urlencode($hashedString);
            $urlRedirect = 'http://payment.gogo.mn/order?p=2&productid='.$productId.'&usertype=gogo-music-web&userid='.$me->getId().'&price='.$price.'&redirect='.$redirect.'&c='.$requestString;
            return $this->redirect($urlRedirect);
        }
    }


    public function paymentCallbackAction(Request $request){
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $me = $this->getUser();
            $price = $this->getCurrentPrice();
            $productId = $request->get('prodId');
            $dateString = date("Y-m-d");


            $hashString = $productId.'|gogo-music-web|'.$me->getId().'|'.$price.'|'.$dateString.'|TNn4A114WfwXNWoFiGuyXA9ab';
            $hashedString = hash('sha256',$hashString,false);
            $requestString = urlencode($hashedString);
            $urlRedirect = 'https://payment.gogo.mn/check?p=2&productid='.$productId.'&usertype=gogo-music-web&userid='.$me->getId().'&price='.$price.'&c='.$requestString;


            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $urlRedirect,
                CURLOPT_FOLLOWLOCATION => true
            ));
            $response = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($response,true);

            if($result['status'] == 'success'){
                $em = $this->getDoctrine()->getManager();
                $log = $em->getRepository('musicCmsBundle:PaymentLog')->find($productId);
                if($log->getTransId() != 1){
                    $mydate = $me->getExpireDate();
                    $now = new \DateTime('now');

                    if($mydate < $now ){
                        $start = clone $now;
                        $end = $now->modify('+30 days');

                    }else{
                        $start = clone $mydate;
                        $end = $mydate->modify('+30 days');
                    }

                    $me->setExpireDate(clone $end);
                    $me->setIsPremium(true);
                    $log->setStartDate($start);
                    $log->setPaidValue($price);
                    $log->setTransactionDate(new \DateTime('now'));
                    $log->setEndDate($end);
                    $log->setTransId(1);

                    $em->persist($me);
                    $em->flush();
                }


                return $this->redirect($this->generateUrl('web_homepage'));

            }

            $em = $this->getDoctrine()->getManager();
            $log = $em->getRepository('musicCmsBundle:PaymentLog')->find($productId);
            $em->remove($log);
            $em->flush();

            return $this->redirect($this->generateUrl('web_homepage'));
        }
    }

    private function getCurrentPrice(){
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare('select priceTug from msc_price_schedule where (endDate > :now or endDate is NULL ) and beginDate < :now order by beginDate desc limit 1');
        $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s')));
        $price = $statement->fetch();
        return $price['priceTug'];
    }

}
