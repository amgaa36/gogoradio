<?php

namespace music\WebBundle\Controller;

use music\WebBundle\Box\VideoBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use music\CmsBundle\Entity\UserVideo;
use music\WebBundle\Box\GstatBox;


class VideoController extends Controller
{
    public function indexAction(Request $request)
    {
        $arr = array("menu" => 2);

        $videoBox = new VideoBox();

        $order = $request->query->get('order');

        switch ($order) {
            case 'view':
                break;
            case 'new':
            default:
                $order = 'new';
        }

        $arr['video_order'] = $order;

        $arr = array_merge($arr, $videoBox->getVideos($this->container, array(
            'orderBy' => $order,
        )));

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:Default:video.html.twig', $arr);
    }

    public function getVideosAction(Request $request)
    {
        $page = $request->request->get('page');
        $pagesize = $request->request->get('pagesize');
        $orderBy = $request->request->get('orderBy');

        $videoBox = new VideoBox();
        $videos = $videoBox->getVideos($this->container, array(
            'page'     => $page,
            'pagesize' => $pagesize,
            'orderBy'  => $orderBy,
        ));

        return new JsonResponse($videos);
    }

    public function infoAction(Request $request, $id)
    {


        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$video || !$video->publish_date || $video->publish_date > new \DateTime('now')) {
            throw $this->createNotFoundException();
        }

        if(preg_match('/(facebookexternalhit)/i', $request->headers->get('user-agent'))
           || $request->get('fbtest')=='yes'){

            return $this->render('musicWebBundle:fb-detail:fb-video-info.html.twig', array('video'=>$video));
        }

        $qb = $em->getRepository('musicCmsBundle:Video')->createQueryBuilder('e');

        $qb->update()
            ->set('e.view', $qb->expr()->sum('e.view','1'))
            ->where($qb->expr()->eq('e.id', ':id'))
            ->setParameter('id', $video->getId())
            ->getQuery()
            ->execute();
        $video->setView($video->getView()+1);

        //throw new \Exception(var_dump($user));
        $arr['video_liked'] = false;
        $user = $this->getUser();
        if ($user) {
            $qb= $em->getRepository('musicCmsBundle:UserVideo')->createQueryBuilder('e');
            $uservideo = $qb
                ->where($qb->expr()->eq('e.user',':user'))
                ->setParameter('user', $user)
                ->andWhere($qb->expr()->eq('e.video',':video'))
                ->setParameter('video', $video)
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();
            if (count($uservideo) > 0) {
                $arr['video_liked'] = $uservideo[0]['liked'];
            }
        }
        $arr['video'] = $video;

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:Default:video-info.html.twig', $arr);
    }

    public function toggleLikeAction(Request $request, $id)
    {
        $arr = array(
            'logged' => true,
        );
        $user = $this->getUser();
        if (!$user) {
            $arr['logged'] = false;
        }
        if ($arr['logged']) {
            $em = $this->getDoctrine()->getManager();

            $video = $em->getRepository('musicCmsBundle:Video')->find($id);

            if (!$video) {
                throw $this->createNotFoundException();
            }

            $uservideo = $em->getRepository('musicCmsBundle:UserVideo')->findBy(array(
                'user'  => $user,
                'video' => $video,
            ));

            if (count($uservideo) > 0) {
                $uservideo = $uservideo[0];
                $uservideo->liked = !$uservideo->liked;
            }
            else {
                $uservideo = new UserVideo();
                $uservideo->setUser($user);
                $uservideo->setVideo($video);
                $uservideo->liked = true;
            }

            $em->persist($uservideo);
            $em->persist($video);
            $em->flush();

            $totalLike = $em->getRepository('musicCmsBundle:UserVideo')->createQueryBuilder('e')
                ->select('COUNT(e.id)')
                ->where('e.video=:video')
                ->setParameter('video', $video)
                ->andWhere('e.liked=1')
                ->getQuery()
                ->getSingleScalarResult();
            $video->like_count = $totalLike;
            $em->persist($video);
            $em->flush();
            $arr = array_merge($arr, array(
                    'totallike' => $totalLike,
                    'haha' => $video->like_count,
                    'userlike'  => $uservideo->liked,
                ));
        }
        return new JsonResponse($arr);
    }



    public function getUrlAction(Request $request){
        $audioId = $request->get('audioId');
        $em = $this->getDoctrine()->getManager();
        $videos = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e')
            ->select('e.video_id')
            ->where('e.audio_id = :aid')
            ->setParameter('aid', $audioId)
            ->getQuery()
            ->getArrayResult();
        $videoId = $videos[0];
        $video = $this->container->get('router')->generate('web_menu_video_info', array(
            'id' => $videoId['video_id'],
        ));
        $arr = array('videoUrl'=>$video,'success'=>true);
        return new JsonResponse($arr);
    }
}
