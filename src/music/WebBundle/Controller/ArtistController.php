<?php

namespace music\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use music\WebBundle\Box\ArtistBox;
use music\WebBundle\Box\GstatBox;

class ArtistController extends Controller
{
    public function indexAction($page)
    {
        $arr = array("menu"=>4,"ttab"=>1);
        $artistBox = new ArtistBox();
        $type = 1;
        $arr += $artistBox->getAllArtists($this,$page,$type);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu:artist.html.twig', $arr);


    }

    public function getPageAction(Request $request){
        $arr = array();
        $pageNum = $request->get('pageNum');
        $type = $request->get('type');
        $artistBox = new ArtistBox();
        if($type == 1){
            $arr += $artistBox->getAllArtists($this,$pageNum,$type);
        }else{

        }

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:inc:thumb-page.html.twig',$arr);
    }

    public function abroadAction()
    {
        $arr = array("menu"=>4,"ttab"=>2);
        $artistBox = new ArtistBox();
        $arr += $artistBox->getAllArtists($this,1,0);

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:sub-menu:artist.html.twig', $arr);
    }

    public function detailAction($id)
    {
        $arr = array("menu"=>4);

        $artistBox = new ArtistBox();

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
            $arr += $artistBox->artistDetail($this, $id, $this->getUser()->getId());
        } else
            $arr += $artistBox->artistDetail($this, $id);



        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:detail:artist-detail.html.twig', $arr);
    }
}
