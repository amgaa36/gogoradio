<?php

namespace music\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\NewsBox;
use music\WebBundle\Box\GstatBox;

class NewsController extends Controller
{
    public function indexAction()
    {
        $arr = array("menu"=>5);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        return $this->render('musicWebBundle:Default:news.html.twig', $arr);
    }
}
