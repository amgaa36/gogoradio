<?php

namespace music\WebBundle\Controller;

use music\WebBundle\Box\LoginBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use music\WebBundle\Box\NewBox;
use music\WebBundle\Box\GstatBox;

class NewController extends Controller
{
    public function indexAction($page)
    {
        $arr = array("menu"=>1,"left_menu"=>0);
        $newBox = new NewBox();
        $arr += $newBox->getNewAudioAndAlbum($this,$page);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }

        return $this->render('musicWebBundle:sub-menu:new.html.twig', $arr);
    }


    public function getPageAction(Request $request){
        $arr = array();
        $pageNum = $request->get('pageNum');
        $newBox = new NewBox();
        $arr += $newBox->getNewAudioAndAlbum($this,$pageNum);
        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }

        return $this->render('musicWebBundle:inc:thumb-page.html.twig',$arr);
    }

}
