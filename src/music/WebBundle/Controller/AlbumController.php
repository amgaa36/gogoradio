<?php

namespace music\WebBundle\Controller;

use music\WebBundle\Box\GstatBox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use music\WebBundle\Box\ArtistBox;
use music\WebBundle\Box\AlbumBox;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends Controller
{
    public function detailAction(Request $request, $id)
    {
        $arr = array();

        $albumBox = new AlbumBox();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
            $arr += $albumBox->albumDetail($this, $id, $this->getUser()->getId());
        else
            $arr += $albumBox->albumDetail($this, $id);

        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
            $now = new \DateTime('now');
            if($user->getExpireDate() > $now){
                $arr += array('isPremium'=>1);
            }
        }

        $gstatBox = new GstatBox();
        $arr += $gstatBox->setGstatUrl($this, $this->container);
        return $this->render('musicWebBundle:detail:detail-template.html.twig', $arr);
    }


    public function getAllAudioAction(Request $request){

        $albumId = $request->get ( "albumId" );

        $em = $this->getDoctrine()->getManager();

        $results =$em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('aa')
            ->select('IDENTITY(aa.audio) as id, au.name ,au.artist_name, au.is_lyrics, au.is_video, COALESCE (au.img,au.album_img,au.artist_img) as img, au.file_url, au.hitone as hitone')
            ->leftJoin('aa.audio','au')
            ->where('aa.album = :albumId')
            ->andWhere('au.publish_date < :now')
            ->setParameter('now', new \DateTime())
            ->setParameter('albumId', $albumId)
            ->orderBy('aa.order', 'DESC')
            ->getQuery()
            ->getArrayResult();

        return new JsonResponse($results);

    }

}
