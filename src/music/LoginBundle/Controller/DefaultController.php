<?php

namespace music\LoginBundle\Controller;

use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use music\CmsBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('gogoLoginBundle:Default:index.html.twig', array('name' => $name));
    }

    public function loginAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $gogoData = $this->get('myapi');

        if ($request->get('code')) {
            $access = $gogoData->getAccessToken($request->get('code'));

            if (is_array($access) && array_key_exists('access_token', $access)) {
                $info = $gogoData->getUserInfo($access['access_token']);

                if (is_array($info) && $info['code'] == 0) {
                    $info = $info['user'];

                    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
                    $qb->select('u')
                        ->from('musicCmsBundle:User', 'u')
                        ->where('u.socialId = :gid')
                        ->setParameter('gid', $info['id'])
                        ->setMaxResults(1);
                    $result = $qb->getQuery()->getResult();

                    $user = null;

                    if (count($result) === 0) {
                        $user = new User($info['id']);
                        $user->setFirstName($info['firstname']);
                        $user->setLastName($info['lastname']);

//

//                        $request->getSession()->set('test','test');
//                        if ($info['img'] == null){
//                            $user->setUserImageUrl("");
//                        }else{
//                            $user->setUserImageUrl($info['img']);
//                        }

                        $user->setUserBirthday(new \DateTime($info['birthday']));
                        $user->setUserEmail($info['email']);
                        $user->setLoginName($info['firstname'].' '.$info['lastname']);
                        $user->setSocialId($info['id']);
                        $user->setLoginType('gogo');
                        $imgRand = 'http://stat.gogo.mn/musicnew/webstats/thumb_defaults/default'.rand(0,15).'.png';

                        $user->setUserImageUrl($imgRand);

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($user);
                        $em->flush();

                        $request->getSession()->set('is_new', 'new');

                        $result = $qb->getQuery()->getResult();
                        $user = $result[0];
//                        $request->getSession()->set('is_new', 'new');

                    } else {
                        $user = $result[0];
                    }


//                    $request->getSession()->set('id', $user->getSocialId());
                    $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
                    $this->get('security.context')->setToken($token);

                    $request->getSession()->set('gogouser', serialize($user));
                }
            }
        } else {
            if (!is_object($user)) {
                return $this->redirect($gogoData->getAuthenticationUrl());
            }
        }

        return $this->redirect($this->generateUrl('web_homepage'));
    }
}
