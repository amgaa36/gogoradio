<?php
/**
 * Created by PhpStorm.
 * User: Amgaa
 * Date: 1/27/15
 * Time: 7:28 PM
 */
namespace music\LoginBundle\Controller;

use Doctrine\DBAL\Driver\AbstractDriverException;
use Doctrine\ORM\NoResultException;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use music\CmsBundle\Entity\User;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class LoginUserProvider extends OAuthUserProvider {
    protected $session, $doctrine, $admins;

    public function __construct($session, $doctrine, $admins)
    {
        $this->session = $session;
        $this->doctrine = $doctrine;
        $this->admins = $admins;

    }

    public function loadUserByUsername($username)
    {
        $em = $this->doctrine->getManager();
        $qb = $em->getRepository('musicCmsBundle:User')->createQueryBuilder('e');

        try{
            $user = $qb
                ->where($qb->expr()->eq('e.socialId',':socialId'))
                ->andWhere($qb->expr()->neq('e.loginType',':loginType'))
                ->setParameter('loginType','gogo')
                ->setParameter('socialId', $username)
                ->getQuery()
                ->getOneOrNullResult()
            ;
//            $myid = $user->getId();
//            $loginDate =  date_format($user->getNotifDate(), 'Y-m-d H:i:s');

//            $em = $this->doctrine->getManager('default');
//            $connection = $em->getConnection();
//            $statement = $connection->prepare('select count(a.id) as notifcount from msc_audio a, msc_audio_artist as aua, msc_user_artist as usa, msc_artist as art
// where usa.user_id = :myid and a.publish_date > :lastLogin and usa.followDate <= a.publish_date and usa.artist_id = aua.artist_id and aua.audio_id = a.id and art.published_date is not NULL and art.id=usa.artist_id');
//            $statement->execute(array(':myid'=>$myid, ':lastLogin' => $loginDate ));
//            $count = $statement->fetchAll();
//
//            $user->setNotifCount( $count[0]['notifcount']);
//            $em->persist($user);
//            $em->flush();



            if(!$user) throw new NoResultException();

        }catch(NoResultException $e) {
            $gogouser = unserialize($this->session->get('gogouser'));

            if($gogouser) {
//                $this->session->remove('gogouser');
                return $gogouser;
            }

//            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
            throw new NoResultException();
        }

        $this->session->remove('gogouser');
        return $user;
    }

    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {

        //Data from Google response
        $res = $response->getResponse();

        $social_id = $response->getUsername(); /* An ID like: 112259658235204980084 */
        $email = $response->getEmail();

        $nickname = $response->getNickname();
        $realname = $response->getRealName();
        if(!$realname)
            $realname = $nickname;
        if(!$nickname)
            $nickname = $realname;

        $birthday = null;
        if(array_key_exists('birthday',$res)){
            $birthday = $res['birthday'];
        }

        $avatar = $response->getProfilePicture();
//        throw new \Exception(var_dump($res));

        $serviceName = 'gogoAccount';
        if($response->getResourceOwner()) {
            $serviceName = $response->getResourceOwner()->getName();
        }

        $qb = $this->doctrine->getManager()->createQueryBuilder();
        $qb->select('u')
            ->from('musicCmsBundle:User', 'u')
            ->where('u.socialId = :gid')
            ->setParameter('gid', $social_id)
            ->setMaxResults(1);
        $result = $qb->getQuery()->getResult();

        if (count($result) === 0) {

            $user = new User($social_id);
            $user->setSocialId($social_id);
            $user->setLoginName($realname);
            $user->setUserEmail($email);

            if($birthday)
                $user->setUserBirthday(new \DateTime($birthday));
            if($serviceName == "facebook") {

                $user->setUserImageUrl('https://graph.facebook.com/'.$social_id.'/picture?type=large&width=600&height=600');
                $user->setFirstName($res['first_name']);
                $user->setLastName($res['last_name']);

            }
            else if($serviceName == "yahoo"){
                $user->setUserImageUrl($res['profile']['image']["imageUrl"]);
                $user->setFirstName($realname);
                $user->setLastName($res['profile']['familyName']);
                $user->setLoginName($realname.' '.$res['profile']['familyName']);

            }else if($serviceName == "google"){
                $user->setFirstName($res['given_name']);
                $user->setLastName($res['family_name']);
                $user->setUserImageUrl($avatar);
            }else if($serviceName == "twitter"){
                $user->setFirstName($realname);
                $user->setDescription($res['description']);
                $user->setUserImageUrl($avatar);
            }

            if($user->getUserImageUrl() == null || $user->getUserImageUrl() == 'null'){

                $imgRand = 'http://stat.gogo.mn/musicnew/webstats/thumb_defaults/default'.rand(0,15).'.png';

                $user->setUserImageUrl($imgRand);
            }

            $user->setLoginType($serviceName);
            $user->setLoginDate(new \DateTime('now'));
            $user->setNotifCount(0);
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();

            $this->session->set('is_new', 'new');
        }
        else {
            $changed = false;
            $user = $result[0];
            $myid = $user->getId();
            $loginDate =  date_format($user->getLoginDate(), 'Y-m-d H:i:s');

            $em = $this->doctrine->getManager('default');
            $connection = $em->getConnection();
            $statement = $connection->prepare('select count(a.id) as notifcount from msc_audio a, msc_audio_artist as aua, msc_user_artist as usa
 where usa.user_id = :myid and a.created_date > :lastLogin and usa.artist_id = aua.artist_id and aua.audio_id = a.id');
            $statement->execute(array(':myid'=>$myid, ':lastLogin' => $loginDate ));
            $count = $statement->fetchAll();
            $user->setNotifCount($user->getNotifCount() + $count[0]['notifcount']);
            $user->setLoginDate(new \DateTime('now'));
            $em->persist($user);
            $em->flush();

            if($user->getFirstName() != $realname) {
                $user->setFirstName($realname);
                $changed = true;
            }
            if($user->getUserEmail() != $email) {
                $user->setUserEmail($email);
                $changed = true;
            }
            if($serviceName == "facebook") {
                $img = 'https://graph.facebook.com/'.$social_id.'/picture?type=large&width=600&height=600';
                if(!$user->getUserImageUrl()) {
                    $user->setUserImageUrl($img);
                    $changed = true;
                }
            }
            else if($serviceName == "yahoo"){
                $img = $res['profile']['image']["imageUrl"];
                if($user->getUserImageUrl() != $img) {
                    $user->setUserImageUrl($img);
                    $changed = true;
                }
            }
            else{
                if($user->getUserImageUrl() != $avatar) {
                    $user->setUserImageUrl($avatar);
                    $changed = true;
                }
            }

            if($birthday && (!$user->getUserBirthday() || $user->getUserBirthday()->format('Y-m-d') != (new \DateTime($birthday))->format('Y-m-d'))) {
                $user->setUserBirthday(new \DateTime($birthday));
                $changed = true;
            }


            if($user->getUserImageUrl() == null || $user->getUserImageUrl() == 'null'){
                $imgRand = 'http://stat.gogo.mn/musicnew/webstats/thumb_defaults/default'.rand(0,15).'.png';
                $user->setUserImageUrl($imgRand);
                $changed = true;
            }


            if($changed == true) {
                $em = $this->doctrine->getManager();
                $em->persist($user);
                $em->flush();
            }
        }


        $this->session->set('id', $user->getSocialId());
        return $this->loadUserByUsername($response->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'music\\CmsBundle\\Entity\\User';
    }
}
