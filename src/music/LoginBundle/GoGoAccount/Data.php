<?php
/**
 * Created by PhpStorm.
 * User: tsepu
 * Date: 3/2/15
 * Time: 3:58 PM
 */

namespace music\LoginBundle\GoGoAccount;

use Monolog\Handler\StreamHandler;
use Symfony\Bridge\Monolog\Logger;

class Data {

    protected $client;
    protected $key;
    protected $redirectUrl;
    protected $oauthUrl;

    public function __construct($container, $pub_id, $sec_id, $redirects, $url) {
        $request = $container->get('request');
        $myUrl = $request->getSchemeAndHttpHost() . '/';


        $this->redirectUrl = $redirects;
        if(in_array($myUrl, $redirects)) {
            $this->redirectUrl = $myUrl;
        } else {
            $this->redirectUrl = $redirects[0];
        }

        $this->client = $pub_id;
        $this->key = $sec_id;

        $this->oauthUrl = $url;
    }

    private function getData($url, $params, $token = null)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($token) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $token
            ));
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);


        $today = new \DateTime('now');
        $file = 'gogo_account_' . $today->format('Y_m_d');
        $log = new Logger('test_log');
        $log->pushHandler(new StreamHandler(__DIR__ .  '/../../../../app/logs/' .  $file, Logger::WARNING));
        $log->addWarning('', array('url' => $url . '?' . $params, 'ret' => $server_output, 'err' => curl_error($ch)));

        curl_close($ch);

        $response = json_decode($server_output, true);

        return $response;
    }

    public function getAuthenticationUrl() {
        return $this->oauthUrl . 'user/auth?client_id=' . $this->client . '&redirect_uri=' . $this->redirectUrl . '&response_type=code&scope=info';
    }

    public function getAccessToken($authCode)
    {
        $url = $this->oauthUrl . 'user/token';
        $params = 'client_id=' . $this->client . '&client_secret=' . $this->key . '&grant_type=authorization_code&redirect_uri=' . $this->redirectUrl . '&code=' . $authCode;

        return $this->getData($url, $params);
    }

    public function replaceAccessToken($refreshToken) {
        $url = $this->oauthUrl . 'user/token';
        $myParams = 'redirect_uri=' . $this->redirectUrl . '&client_id=' . $this->client . '&client_secret=' . $this->key . '&grant_type=refresh_token&refresh_token=' . $refreshToken;
        return $this->getData($url, $myParams);
    }

    public function getUserInfo($accessToken) {
        $url = $this->oauthUrl . 'api/user/info';
        return $this->getData($url, null, $accessToken);
    }
}