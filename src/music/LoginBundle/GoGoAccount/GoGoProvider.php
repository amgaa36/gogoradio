<?php
/**
 * Created by PhpStorm.
 * User: tsepu
 * Date: 4/1/15
 * Time: 4:52 PM
 */

namespace music\LoginBundle\GoGoAccount;


use music\CmsBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class GoGoProvider implements  UserProviderInterface {
    protected $session, $doctrine, $admins;

    public function __construct($session, $doctrine, $admins) {
        $this->session = $session;
        $this->doctrine = $doctrine;
        $this->admins = $admins;
    }

    public function loadUserByUsername($username) {
        $qb = $this->doctrine->getManager()->createQueryBuilder();
        $qb->select('u')
            ->from('musicCmsBundle:User', 'u')
            ->where('u.socialId = :gid')
            ->setParameter('gid', $username)
            ->setMaxResults(1);
        $result = $qb->getQuery()->getResult();

        if (count($result)>0) {
            return $result[0];
        } else {
            //return new User($username);
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
    }

    public function refreshUser(UserInterface $user) {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }

        return $user;
    }

    public function supportsClass($class) {
        return $class === 'music\\CmsBundle\\Entity\\User';
    }
}