<?php

namespace music\CmsBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use music\CmsBundle\Entity\Audio;
use music\CmsBundle\Entity\Genre;
use music\CmsBundle\Form\GenreType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Genre controller.
 *
 */
class GenreController extends Controller
{

    /**
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('musicCmsBundle:Genre')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new Genre();
        $form = $this->createCreateForm($entity);

        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->uploadImage($this->container);
                $entity->uploadBackgroundImage($this->container);
                $entity->uploadIconImage($this->container);
                $entity->uploadThumbImage($this->container);
                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('genre'));
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @ParamConverter("entity", class="musicCmsBundle:Genre")
     * @Template()
     */
    public function editAction(Request $request, Genre $entity)
    {
        $form = $this->createEditForm($entity);
        $delete_form = $this->createDeleteForm();

        if (strtolower($request->getMethod()) === 'post') {
            $em = $this->getDoctrine()->getManager();
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($form->get('submit')->isClicked()) {
                    $entity->uploadImage($this->container);
                    $entity->uploadBackgroundImage($this->container);
                    $entity->uploadIconImage($this->container);
                    $entity->uploadThumbImage($this->container);
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('genre'));
                }
            }

            $delete_form->handleRequest($request);
            if ($delete_form->isValid()) {
                if ($delete_form->get('submit')->isClicked()) {
                    $em->remove($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('genre'));
                }
            }
        }

        return array(
            'form' => $form->createView(),
            'delete_form' => $delete_form->createView(),
            'entity' => $entity,
        );
    }

    private function createCreateForm(Genre $entity)
    {
        $form = $this->createForm(new GenreType(), $entity, array(
            'method' => 'POST'
        ));
        $form
            ->remove('duration')
            ->remove('track_number')
            ->remove('like_count')
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
                'attr' => array(
                    'class' => 'btn-success',
                )
            ));

        return $form;
    }

    private function createEditForm(Genre $entity)
    {
        $form = $this->createForm(new GenreType(), $entity, array(
            'method' => 'POST'
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Хадгалах',
            'attr' => array(
                'class' => 'btn-success',
            )
        ));

        return $form;
    }

    private function createDeleteForm()
    {
        return $this->createFormBuilder()
            ->setMethod('POST')
            ->add('submit', 'submit', array(
                'label' => 'Устгах',
                'attr' => array(
                    'class' => 'btn-danger confirm'
                ),
            ))
            ->getForm();
    }

    /**
     * @param Request $request
     * @param Genre $entity
     * @param Audio $audio
     *
     * @ParamConverter("entity", class="musicCmsBundle:Genre")
     * @ParamConverter("audio", class="musicCmsBundle:Audio", options={"id"="aid"})
     *
     * @return JsonResponse
     */
    public function removeAudioAction(Request $request, $entity, $audio)
    {
        $em = $this->getDoctrine()->getManager();
        if ($audio->getGenre() == $entity) {
            $audio->setGenre(null);
            $em->persist($audio);
            $em->flush();
            return new JsonResponse(array(
                'status' => 'success',
            ));
        }
        return new JsonResponse(array(
            'status' => 'fail',
        ));
    }

//SELECT msc_genre_audio.audio_id, msc_audio.audio_name, msc_audio.artist_name
//FROM msc_genre_audio
//INNER JOIN msc_audio
//ON msc_audio.id=msc_genre_audio.audio_id
//where msc_genre_audio.genre_id = 3;


    public function listAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('musicCmsBundle:GenreAudio')->createQueryBuilder('s');
        $entities = $qb
            ->leftJoin('s.audio','au')
            ->addSelect('au')
            ->where('s.genre = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();


        return $this->render('musicCmsBundle:Genre:list.html.twig', array(
            'entities' => $entities,
        ));
    }
}
