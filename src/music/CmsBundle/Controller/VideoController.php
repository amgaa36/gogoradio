<?php

namespace music\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\Video;
use music\CmsBundle\Form\VideoType;
use music\CmsBundle\Form\VideoSearchType;

/**
 * Video controller.
 *
 * @Route("/video")
 */
class VideoController extends Controller
{

    /**
     * Lists all Video entities.
     *
     * @Route("/", name="video")
     * @Method("GET")
     * @Template()
     */
    public function indexAction( $page, Request $request)
    {
        $pagesize = 10;
        $em = $this->getDoctrine()->getManager();

//      $entities = $em->getRepository('musicCmsBundle:Video')->findAll();
        $queryBuilder = $em->getRepository ( 'musicCmsBundle:Video' )->createQueryBuilder ( 'p' );

        $searchEntity = new Video ();
        $searchform = $this->createForm ( new VideoSearchType(), $searchEntity );
        $search = false;

        if ($request->get ( "submit" ) == 'submit') {
            $searchform->bind( $request );
            $search = true;
        }


        if ($search)
        {
            if($searchEntity->name && $searchEntity->name!=''){
                $queryBuilder->andWhere('LOWER(p.name) like LOWER(:name)')
                    ->setParameter('name',  '%'.$searchEntity->name.'%');
            }
            if($searchform->get('img')->getData()){
                if($searchEntity->img == '1'){
                    $queryBuilder->andWhere('p.img_is_big=:img_is_big')
                        ->setParameter('img_is_big',true);
                }
                else if($searchEntity->img == '2'){
                    $queryBuilder->andWhere('p.img_is_big=:img_is_big')
                        ->setParameter('img_is_big',false);
                }
            }
        }

        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(p.name)')->getQuery()->getSingleScalarResult();


        $entities = $queryBuilder
            ->select('p.id, p.name, p.img, p.videoUrl, p.artist, p.publish_date, p.like_count, p.view, p.created_date, p.updated_date')
            ->addSelect('(SELECT COUNT(av.id) FROM musicCmsBundle:AudioVideo av WHERE av.video=p) AS videocount')
            ->addSelect('(SELECT COUNT(arv.id) FROM musicCmsBundle:ArtistVideo arv WHERE arv.video=p) AS artistcount')
            ->orderBy('p.name', 'ASC')
            ->setFirstResult(($page-1)*$pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:Video:index.html.twig', array(
            'entities' => $entities,
            'count' => $count,
            'page' => $page,
            'pagecount' => ($count%$pagesize)>0 ? intval($count/$pagesize)+1 : intval($count/$pagesize),
            'test' => $this->container->getParameter('statfolder') ,
            'searchform'=>$searchform->createView(),
            'search'=>$search,
            'test'=>$searchEntity->name
        ));
    }
    /**
     * Creates a new Video entity.
     *
     * @Route("/", name="video_create")
     * @Method("POST")
     * @Template("musicCmsBundle:Video:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Video();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity->getVideoThumb();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('video_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Video entity.
     *
     * @param Video $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Video $entity)
    {
        $form = $this->createForm(new VideoType(), $entity, array(
            'action' => $this->generateUrl('video_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
            'class' => 'btn-success',
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Video entity.
     *
     * @Route("/new", name="video_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Video();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Video entity.
     *
     * @Route("/{id}", name="video_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Video entity.
     *
     * @Route("/{id}/edit", name="video_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Video entity.
    *
    * @param Video $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Video $entity)
    {
        $form = $this->createForm(new VideoType(), $entity, array(
            'action' => $this->generateUrl('video_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array(
            'class' => 'btn-success',
        )));

        return $form;
    }
    /**
     * Edits an existing Video entity.
     *
     * @Route("/{id}", name="video_update")
     * @Method("PUT")
     * @Template("musicCmsBundle:Video:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $entity->getVideoThumb();

            $em->flush();
            return $this->redirect($this->generateUrl('video_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Video entity.
     *
     * @Route("/{id}", name="video_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:Video')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Video entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('video'));
    }

    /**
     * Creates a form to delete a Video entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('video_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Устгах', 'attr' => array(
                'class' => 'btn-danger'
            )))
            ->getForm()
        ;
    }
}
