<?php

namespace music\CmsBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use music\CmsBundle\util\AlbumUpdateUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\ArtistVideo;
use Symfony\Component\HttpFoundation\Request;

/**
 * ArtistVideo controller.
 *
 * @Route("/artistvideo")
 */
class ArtistVideoController extends Controller
{

    /**
     * Lists all ArtistVideo entities.
     *
     * @Route("/", name="artistvideo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('musicCmsBundle:ArtistVideo')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a ArtistVideo entity.
     *
     * @Route("/{id}", name="artistvideo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:ArtistVideo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ArtistVideo entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    public function addAudAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('musicCmsBundle:Video')->find($id);
        if (!$video) {
            throw $this->createNotFoundException('Error');
        }

        $vidarts = $video->getVideoartists();
        $videoname = $video->getName();
        $oldvidarts = new ArrayCollection();
        foreach ($vidarts as $vidart) {
            $oldvidarts->add($vidart);
        }

        $form = $this->createArtistForm();

        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $newartistids = array_map(function ($n) {
                    return intval($n);
                }, explode(',', $form->get('artistids')->getData()));

                $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('e');

                $newartists = $qb
                    ->where($qb->expr()->in('e.id', ':ids'))
                    ->setParameter('ids', $newartistids)
                    ->getQuery()
                    ->getResult();

                foreach ($oldvidarts as $oldaudart) {
                    $found = false;
                    foreach ($newartists as $newartist) {
                        if ($newartist->getId() == $oldaudart->artist->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $em->remove($oldaudart);
                    }
                }

                foreach ($newartists as $newartist) {
                    $found = false;
                    foreach ($oldvidarts as $oldaudart) {
                        if ($oldaudart->artist->getId() == $newartist->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $dundiin = new ArtistVideo();
                        $dundiin->artist = $newartist;
                        $dundiin->video = $video;
                        $dundiin->order = count($newartists) + 1;
                        $em->persist($dundiin);
                    }
                }

                $em->flush();

                $audioupdateutil = new AlbumUpdateUtil();
                $audioupdateutil->updateVideoArtistInfoByVideoObject($this, $video);

                return $this->redirect($this->generateUrl('video', array(
                    'id' => $id,
                )));
            }
        }

        return $this->render('musicCmsBundle:ArtistVideo:index.html.twig', array(

            'form' => $form->createView(),
            'videoname' => $videoname,
            'vidarts' => $vidarts,

        ));

    }


    private function createArtistForm()
    {
        return $this->createFormBuilder()
            ->setMethod("POST")
            ->add('artistids', 'text', array(
                'label' => 'Уран бүтээлчид',
                'required' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
            ))
            ->getForm();
    }
}
