<?php

namespace music\CmsBundle\Controller;


use music\CmsBundle\Entity\Playlist;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\PlaylistAudio;
use Symfony\Component\HttpFoundation\Request;

/**
 * PlaylistAudio controller.
 *
 * @Route("/playlistaudio")
 */
class PlaylistAudioController extends Controller
{

    /**
     * Lists all PlaylistAudio entities.
     *
     * @Route("/", name="playlistaudio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id , Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $playlist = $em->getRepository('musicCmsBundle:Playlist')->find($id);

        if (!$playlist) {

        }
        $playlistPub = $playlist->getIsPublic();
        $form = $this->createCheckForm($playlistPub);
        $plist = $em->getRepository('musicCmsBundle:PlaylistAudio')->createQueryBuilder('e');

        $playlistaudios = $plist
            ->leftJoin('e.audio', 'audio')
            ->addSelect('audio')
            ->where($plist->expr()->eq('e.playlist', ':playlist'))
            ->setParameter('playlist', $playlist)
            ->getQuery()
            ->getArrayResult();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $ispub = $form->get('check')->getData();
            $playlist->setIsPublic($ispub);
            $em->persist($playlist);
        }

        $em->flush();

        $playlistName = $playlist->getName();

        return array(
            'playlistaudios' => $playlistaudios,
            'playlist' => $playlistName,
            'form' => $form->createView(),
        );
    }

    private function createCheckForm($mid)
    {
        return $this->createFormBuilder()

            ->setMethod("POST")
            ->add('check', 'choice', array(
                'choices' => array('1' => 'Тйим', '0' => 'Үгүй'),
                'label' => 'Нийтлэх эсэх',
                'required' => true,
                'data' => $mid,
                'expanded' => true,))
            ->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
                'class' => 'btn btn-success'
            )))
            ->getForm();
    }


    /**
     * Finds and displays a PlaylistAudio entity.
     *
     * @Route("/{id}", name="playlistaudio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:PlaylistAudio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlaylistAudio entity.');
        }

        return array(
            'entity' => $entity,
        );
    }
}
