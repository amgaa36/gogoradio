<?php

namespace music\CmsBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use music\CmsBundle\Entity\GenreAudio;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\Audio;
use music\CmsBundle\Form\AudioType;
use music\CmsBundle\Form\AudioSearchType;

/**
 * Audio controller.
 *
 */
class AudioController extends Controller
{

    /**
     * Lists all Audio entities.
     *
     * @Template()
     */
    public function indexAction($page, Request $request)
    {
        $pagesize = 50;
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('p');

        $searchEntity = new Audio ();
        $searchform = $this->createForm(new AudioSearchType(), $searchEntity);
        $search = false;

        if ($request->get("submit") == 'submit') {
            $searchform->bind($request);
            $search = true;
        }

        if ($search) {
            if ($searchEntity->name && $searchEntity->name != '') {
                $queryBuilder->andWhere('LOWER(p.name) like LOWER(:name)')
                    ->setParameter('name', '%' . $searchEntity->name . '%');
            }
            if ($searchform->get('img')->getData()) {
                if ($searchEntity->img == '1') {
                    $queryBuilder->andWhere('p.img is not null');
                } else if ($searchEntity->img == '2') {
                    $queryBuilder->andWhere('p.img is null');
                }
            }

            if ($searchform->get('publish_date')->getData()) {
                if ($searchEntity->publish_date == '1') {

                    $queryBuilder->andWhere('p.publish_date < :publish_date')

                        ->setParameter('publish_date', new \DateTime('now'));

                } else if ($searchEntity->publish_date == '2') {
                    $queryBuilder->andWhere('p.publish_date > :publish_date')
                        ->setParameter('publish_date', new \DateTime('now'));
                } else if ($searchEntity->publish_date == '3') {
                    $queryBuilder->andWhere('p.publish_date is null');
                }
            }

            if ($searchform->get('is_mongolian')->getData()) {
                if ($searchEntity->is_mongolian == '1') {
                    $queryBuilder->andWhere('p.is_mongolian=:is_mongolian')
                        ->setParameter('is_mongolian', true);
                } else if ($searchEntity->is_mongolian == '2') {
                    $queryBuilder->andWhere('p.is_mongolian=:is_mongolian')
                        ->setParameter('is_mongolian', false);
                }
            }

            if ($searchform->get('hitone')->getData()) {
                if ($searchEntity->hitone == '1') {
                    $queryBuilder->andWhere('p.hitone  is not null');
                } else if ($searchEntity->hitone == '2') {
                    $queryBuilder->andWhere('p.hitone is null');
                }
            }
        }

        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(p.name)')->getQuery()->getSingleScalarResult();
        $audioCountQB = $em->getRepository("musicCmsBundle:AudioArtist")->createQueryBuilder("aua");

        $entities = $queryBuilder
            ->leftJoin('p.genre_audios', 'g')
            ->addSelect('g')
            ->leftJoin('g.genre', 'ge')
            ->addSelect('ge')
//            ->where('p.is_mongolian = 1')
            ->orderBy('p.created_date', 'DESC')
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        foreach ($entities as &$entity) {
            $temp = $entity['id'];

            $audioCount = $audioCountQB->select('count(aua.id)')->where('aua.audio = :arid')->setParameter('arid',
                $temp)->getQuery()->getSingleScalarResult();
            $entity['artistcount'] = $audioCount;
        }

        $albumCountQB = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');

        foreach ($entities as &$entity) {
            $temp = $entity['id'];

            $audioCount = $albumCountQB->select('count(e.id)')->where('e.audio = :arid')->setParameter('arid',
                $temp)->getQuery()->getSingleScalarResult();
            $entity['albumcount'] = $audioCount;
        }
        $video_qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');
        foreach ($entities as &$entity) {
            $temp = $entity['id'];

            $videoCount = $video_qb
                ->select($video_qb->expr()->count('e.id'))
                ->where($video_qb->expr()->eq('e.audio', ':arid'))
                ->andWhere($video_qb->expr()->isNotNull('e.video'))
                ->setParameter('arid', $temp)
                ->getQuery()
                ->getSingleScalarResult();
            $entity['videocount'] = $videoCount;
        }

        return array(
            'entities' => $entities,
            'count' => $count,
            'page' => $page,
            'pagecount' => ($count % $pagesize) > 0 ? intval($count / $pagesize) + 1 : intval($count / $pagesize),
            'test' => $this->container->getParameter('statfolder'),
            'searchform' => $searchform->createView(),
            'search' => $search,
            'test' => $searchEntity->name
        );
    }

    /**
     * Creates a new Audio entity.
     *
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new Audio();
        $form = $this->createCreateForm($entity);
        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();


                foreach ($entity->genres as $genre) {
                    $genre_audio = new GenreAudio();
                    $genre_audio->setGenre($genre);
                    $entity->addGenreAudio($genre_audio);
                }
                if ($entity->name_eng == null) {
                    header('Content-type: text/html; charset=UTF-8'); // So the browser doesn't make our lives harder
                    $results = array();
                    $fin = '';
                    preg_match_all('/./u', $entity->name, $results);
                    foreach ($results[0] as $res) {
                        $fin .= $this->character($res);
                    }

//                    var_dump($fin);
//                    exit();
                    $entity->name_eng = $fin;
                }
                $em->persist($entity);

                if ($entity->lyrics != null) {
                    $entity->getLyrConf();
                } else {
                    $entity->getLyrfalse();
                }

                $entity->getvideofalse();


                $em->persist($entity);
                $em->flush();

                $entity->uploadImage($this->container);
                $entity->uploadAudio($this->container);
                $em->persist($entity);
                $em->flush();
                $util = new \music\CmsBundle\util\GenreUpdateUtil();
                $util->updateGenresByAudio($this->container, $entity->genres);

                return $this->redirect($this->generateUrl('audio'));
            }
        }
        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    public function character($datas)
    {
        $result = '';

        if ($datas == 'А') {
            $result = 'A';
        } else if ($datas == 'а') {
            $result = 'a';
        } else if ($datas == 'Б') {
            $result = 'B';
        } else if ($datas == 'б') {
            $result = 'b';
        } else if ($datas == 'В') {
            $result = 'V';
        } else if ($datas == 'в') {
            $result = 'v';
        } else if ($datas == 'Г') {
            $result = 'G';
        } else if ($datas == 'г') {
            $result = 'g';
        } else if ($datas == 'Д') {
            $result = 'D';
        } else if ($datas == 'д') {
            $result = 'd';
        } else if ($datas == 'Е') {
            $result = 'E';
        } else if ($datas == 'е') {
            $result = 'e';
        } else if ($datas == 'Ё') {
            $result = 'E';
        } else if ($datas == 'ё') {
            $result = 'e';
        } else if ($datas == 'Ж') {
            $result = 'J';
        } else if ($datas == 'ж') {
            $result = 'j';
        } else if ($datas == 'З') {
            $result = 'Z';
        } else if ($datas == 'з') {
            $result = 'z';
        } else if ($datas == 'И') {
            $result = 'I';
        } else if ($datas == 'и') {
            $result = 'i';
        } else if ($datas == 'й') {
            $result = 'i';
        } else if ($datas == 'К') {
            $result = 'K';
        } else if ($datas == 'к') {
            $result = 'k';
        } else if ($datas == 'Л') {
            $result = 'L';
        } else if ($datas == 'л') {
            $result = 'l';
        } else if ($datas == 'М') {
            $result = 'M';
        } else if ($datas == 'м') {
            $result = 'm';
        } else if ($datas == 'Н') {
            $result = 'N';
        } else if ($datas == 'н') {
            $result = 'n';
        } else if ($datas == 'О') {
            $result = 'O';
        } else if ($datas == 'о') {
            $result = 'o';
        } else if ($datas == 'Ө') {
            $result = 'O';
        } else if ($datas == 'ө') {
            $result = 'o';
        } else if ($datas == 'П') {
            $result = 'P';
        } else if ($datas == 'п') {
            $result = 'p';
        } else if ($datas == 'Р') {
            $result = 'R';
        } else if ($datas == 'р') {
            $result = 'r';
        } else if ($datas == 'С') {
            $result = 'S';
        } else if ($datas == 'с') {
            $result = 's';
        } else if ($datas == 'Т') {
            $result = 'T';
        } else if ($datas == 'т') {
            $result = 't';
        } else if ($datas == 'У') {
            $result = 'U';
        } else if ($datas == 'у') {
            $result = 'u';
        } else if ($datas == 'Ү') {
            $result = 'V';
        } else if ($datas == 'ү') {
            $result = 'v';
        } else if ($datas == 'Ф') {
            $result = 'P';
        } else if ($datas == 'ф') {
            $result = 'p';
        } else if ($datas == 'Х') {
            $result = 'H';
        } else if ($datas == 'х') {
            $result = 'h';
        } else if ($datas == 'Ц') {
            $result = 'Ts';
        } else if ($datas == 'ц') {
            $result = 'ts';
        } else if ($datas == 'Ч') {
            $result = 'Ch';
        } else if ($datas == 'ч') {
            $result = 'ch';
        } else if ($datas == 'Ш') {
            $result = 'Sh';
        } else if ($datas == 'ш') {
            $result = 'sh';
        } else if ($datas == 'Щ') {
            $result = 'Sh';
        } else if ($datas == 'щ') {
            $result = 'sh';
        } else if ($datas == 'ь') {
            $result = 'i';
        } else if ($datas == 'ы') {
            $result = 'i';
        } else if ($datas == 'ъ') {
            $result = 'i';
        } else if ($datas == 'Э') {
            $result = 'E';
        } else if ($datas == 'э') {
            $result = 'e';
        } else if ($datas == 'Ю') {
            $result = 'Yu';
        } else if ($datas == 'ю') {
            $result = 'yu';
        } else if ($datas == 'Я') {
            $result = 'Ya';
        } else if ($datas == 'я') {
            $result = 'ya';
        } else if ($datas == 'A') {
            $result = 'A';
        } else if ($datas == 'a') {
            $result = 'a';
        } else if ($datas == 'B') {
            $result = 'B';
        } else if ($datas == 'b') {
            $result = 'b';
        } else if ($datas == 'C') {
            $result = 'C';
        } else if ($datas == 'c') {
            $result = 'c';
        } else if ($datas == 'D') {
            $result = 'D';
        } else if ($datas == 'd') {
            $result = 'd';
        } else if ($datas == 'E') {
            $result = 'E';
        } else if ($datas == 'e') {
            $result = 'e';
        } else if ($datas == 'F') {
            $result = 'F';
        } else if ($datas == 'f') {
            $result = 'f';
        } else if ($datas == 'G') {
            $result = 'G';
        } else if ($datas == 'g') {
            $result = 'g';
        } else if ($datas == 'H') {
            $result = 'H';
        } else if ($datas == 'h') {
            $result = 'h';
        } else if ($datas == 'I') {
            $result = 'I';
        } else if ($datas == 'i') {
            $result = 'i';
        } else if ($datas == 'J') {
            $result = 'J';
        } else if ($datas == 'j') {
            $result = 'j';
        } else if ($datas == 'K') {
            $result = 'K';
        } else if ($datas == 'k') {
            $result = 'k';
        } else if ($datas == 'L') {
            $result = 'L';
        } else if ($datas == 'l') {
            $result = 'l';
        } else if ($datas == 'M') {
            $result = 'M';
        } else if ($datas == 'm') {
            $result = 'm';
        } else if ($datas == 'N') {
            $result = 'N';
        } else if ($datas == 'n') {
            $result = 'n';
        } else if ($datas == 'O') {
            $result = 'O';
        } else if ($datas == 'o') {
            $result = 'o';
        } else if ($datas == 'P') {
            $result = 'P';
        } else if ($datas == 'p') {
            $result = 'p';
        } else if ($datas == 'Q') {
            $result = 'Q';
        } else if ($datas == 'q') {
            $result = 'q';
        } else if ($datas == 'R') {
            $result = 'R';
        } else if ($datas == 'r') {
            $result = 'r';
        } else if ($datas == 'S') {
            $result = 'S';
        } else if ($datas == 's') {
            $result = 's';
        } else if ($datas == 'T') {
            $result = 'T';
        } else if ($datas == 't') {
            $result = 't';
        } else if ($datas == 'U') {
            $result = 'U';
        } else if ($datas == 'u') {
            $result = 'u';
        } else if ($datas == 'V') {
            $result = 'V';
        } else if ($datas == 'v') {
            $result = 'v';
        } else if ($datas == 'W') {
            $result = 'W';
        } else if ($datas == 'w') {
            $result = 'w';
        } else if ($datas == 'X') {
            $result = 'X';
        } else if ($datas == 'x') {
            $result = 'x';
        } else if ($datas == 'Y') {
            $result = 'Y';
        } else if ($datas == 'y') {
            $result = 'y';
        } else if ($datas == 'Z') {
            $result = 'Z';
        } else if ($datas == 'z') {
            $result = 'z';
        } else if ($datas == ' ') {
            $result = ' ';
        } else if ($datas == '.') {
            $result = '.';
        } else if ($datas == ',') {
            $result = ',';
        } else if ($datas == '(') {
            $result = '(';
        } else if ($datas == ')') {
            $result = ')';
        } else if ($datas == '1') {
            $result = '1';
        } else if ($datas == '2') {
            $result = '2';
        } else if ($datas == '3') {
            $result = '3';
        } else if ($datas == '4') {
            $result = '4';
        } else if ($datas == '5') {
            $result = '5';
        } else if ($datas == '6') {
            $result = '6';
        } else if ($datas == '7') {
            $result = '7';
        } else if ($datas == '8') {
            $result = '8';
        } else if ($datas == '9') {
            $result = '9';
        } else if ($datas == '0') {
            $result = '0';
        } else if ($datas == '-') {
            $result = '-';
        } else if ($datas == '=') {
            $result = '=';
        } else if ($datas == ';') {
            $result = ';';
        } else if ($datas == '}') {
            $result = '}';
        } else if ($datas == '{') {
            $result = '{';
        } else if ($datas == '/') {
            $result = '/';
        } else if ($datas == '~') {
            $result = '~';
        } else if ($datas == '>') {
            $result = '>';
        } else if ($datas == '<') {
            $result = '<';
        } else if ($datas == '!') {
            $result = '!';
        } else if ($datas == '*') {
            $result = '*';
        } else if ($datas == '%') {
            $result = '%';
        } else if ($datas == '$') {
            $result = '$';
        } else if ($datas == '₮') {
            $result = '₮';
        } else if ($datas == '#') {
            $result = '#';
        } else if ($datas == '@') {
            $result = '@';
        } else {
            $result = '0';
        }

        return $result;
    }

    /**
     * Creates a form to create a Audio entity.
     *
     * @param Audio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Audio $entity)
    {
        $form = $this->createForm(new AudioType(), $entity, array(
            'method' => 'POST',
        ));

        $selected_genres = array();
        foreach ($entity->getGenreAudios() as $genre_audio) {
            $selected_genres[] = $genre_audio->getGenre();
        }

        $form->get('genres')->setData($selected_genres);

        $form->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
            'class' => 'btn-success',
        )));

        return $form;
    }


    /**
     * Edits an existing Audio entity.
     *
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Audio entity.');
        }

        $old_genre_audios = new ArrayCollection();
        foreach ($entity->getGenreAudios() as $genre_audio) {
            $old_genre_audios->add($genre_audio);
        }
        $changedGenres = array();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createCreateForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {


//            if ($entity->imagefile != null) {
//                if ($entity->img) {
//                    $fs = new Filesystem();
//                    try {
//
//                        $path = $this->container->getParameter('imgstatfolder') . $entity->img;
//
//                        if (strpos($path, 'new_audio')) {
//
//                            if ($fs->exists($path, $this->container)) {
//                                $fs->remove($path, $this->container);
//                            }
//                        }
//
//                    } catch (IOException $e) {
//                    }
//                    $entity->img = null;
//                }
//            }


            $entity->uploadImage($this->container);
//            if (null === $entity->getAudioFile()) {
//
//            } else {
//                $entity->deleteHlsFolder($this->container, $entity->file_url);
//            }
            $entity->uploadAudio($this->container);

            foreach ($entity->genres as $genre) {
                $found = false;
                foreach ($old_genre_audios as $old_genre_audio) {
                    if ($old_genre_audio->getGenre() == $genre) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $genre_audio = new GenreAudio();
                    $genre_audio->setGenre($genre);
                    $entity->addGenreAudio($genre_audio);

                    $changedGenres[] = $genre;
                }
            }
            foreach ($old_genre_audios as $old_genre_audio) {
                $found = false;
                foreach ($entity->genres as $genre) {
                    if ($old_genre_audio->getGenre() == $genre) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $entity->removeGenreAudio($old_genre_audio);
                    $changedGenres[] = $old_genre_audio->genre;
                }
            }

            if ($entity->lyrics != null) {
                $entity->getLyrConf();
            } else {
                $entity->getLyrfalse();
            }


            $em->persist($entity);
            $em->flush();


//            if ($entity->isImage == true) {
//                if ($entity->img) {
//                    $fs = new Filesystem();
//                    try {
//
//                        $path = $this->container->getParameter('imgstatfolder') . $entity->img;
////                        var_dump($path);
////                        exit();
//                        if (strpos($path, 'new_audio')) {
//
//                            if ($fs->exists($path, $this->container)) {
//                                $fs->remove($path, $this->container);
//                            }
//                        }
//
//                    } catch (IOException $e) {
//                    }
//                    $entity->img = null;
//                }
//            }

//            if ($entity->isAudio == true) {
//
//                if ($entity->file_url) {
//
//                    $fs = new Filesystem();
//                    try {
//                        $aa = dirname($entity->file_url);
//                        $path = $this->container->getParameter('statfolder') . $aa . '/';
//
//                        if (strpos($aa, 'new_audio')) {
//
//                            if ($handle = opendir($path)) {
//
//                                while (false !== ($file = readdir($handle))) {
//
//                                    if ($file === '.' || $file === '..') {
//                                        continue;
//                                    }
//
//                                    if ($fs->exists($path . $file, $this->container)) {
//
//                                        $fs->remove($path . $file, $this->container);
//                                    }
//                                }
//
//                                closedir($handle);
//                            }
//                        }
//                    } catch (IOException $e) {
//                    }
//                    $entity->publish_date = null;
//                    $entity->file_url = null;
//                }
//
//            }


            $util = new \music\CmsBundle\util\GenreUpdateUtil();
            $util->updateGenresByAudio($this->container, $changedGenres);

            return $this->redirect($this->generateUrl('audio', array(
                'page' => 1,
            )));


        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Audio entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:Audio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Audio entity.');
            }
            if ($entity->file_url) {
                $fs = new Filesystem();
                try {
                    $audiopath = dirname($entity->file_url);
                    $imagepath = dirname($entity->img);

                    $pathaudio = $this->container->getParameter('statfolder') . $audiopath . '/';
                    $pathimage = $this->container->getParameter('imgstatfolder') . $imagepath . '/';

                    if (strpos($audiopath, 'new_audio')) {
                        if ($handle = opendir($pathaudio)) {

                            while (false !== ($file = readdir($handle))) {

                                if ($file === '.' || $file === '..') {
                                    continue;
                                }
                                if ($fs->exists($pathaudio . $file, $this->container)) {
                                    $fs->remove($pathaudio . $file, $this->container);
                                }
                            }
                            closedir($handle);
                        }
                    }

                    if (strpos($imagepath, 'new_audio')) {

                        if ($handle = opendir($pathimage)) {


                            $path = $this->container->getParameter('imgstatfolder') . $entity->img;

                            if (strpos($path, 'new_audio')) {

                                if ($fs->exists($path, $this->container)) {
                                    $fs->remove($path, $this->container);
                                }
                            }

                            closedir($handle);
                        }
                    }

                } catch (IOException $e) {
                }
            }

            $em->remove($entity);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('audio'));
    }

    /**
     * Creates a form to delete a Audio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('audio_delete', array('id' => $id)))
            ->setMethod('POST')
            ->add('submit', 'submit', array('label' => 'Устгах', 'attr' => array(
                'class' => 'confirm btn-danger'
            )))
            ->getForm();
    }
}
