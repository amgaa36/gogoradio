<?php

namespace music\CmsBundle\Controller;

use music\CmsBundle\Entity\AlbumArtist;
use music\CmsBundle\Entity\AlbumAudio;
use music\CmsBundle\Entity\Audio;
use music\CmsBundle\Entity\Artist;
use music\CmsBundle\Entity\AudioArtist;
use music\CmsBundle\util\AlbumUpdateUtil;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use music\CmsBundle\Entity\Album;
use music\CmsBundle\Form\AlbumType;
use music\CmsBundle\Form\AlbumSearchType;

/**
 * Album controller.
 *
 */
class AlbumController extends Controller
{

    /**
     * Lists all Album entities.
     *
     */
    public function indexAction($page, Request $request)
    {
        $pagesize = 50;
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('p');

        $searchEntity = new Album ();
        $searchform = $this->createForm(new AlbumSearchType(), $searchEntity);
        $search = false;


        if ($request->get("submit") == 'submit') {
            $searchform->bind($request);
            $search = true;
        }

        if ($search) {
            if ($searchEntity->name && $searchEntity->name != '') {
                $queryBuilder->andWhere('LOWER(p.name) like LOWER(:name)')
                    ->setParameter('name', '%' . $searchEntity->name . '%');
            }
            if ($searchform->get('img')->getData()) {
                if ($searchEntity->img == '1') {
                    $queryBuilder->andWhere('p.img is not null');
                } else if ($searchEntity->img == '2') {
                    $queryBuilder->andWhere('p.img is null');
                }
            }

            if ($searchform->get('publish_date')->getData()) {
                if ($searchEntity->publish_date == '1') {

                    $queryBuilder->andWhere('p.publish_date < :publish_date')

                        ->setParameter('publish_date', new \DateTime('now'));

                } else if ($searchEntity->publish_date == '2') {
                    $queryBuilder->andWhere('p.publish_date > :publish_date')
                        ->setParameter('publish_date', new \DateTime('now'));
                } else if ($searchEntity->publish_date == '3') {
                    $queryBuilder->andWhere('p.publish_date is null');
                }
            }

            if ($searchform->get('is_mongolian')->getData()) {
                if ($searchEntity->is_mongolian == '1') {
                    $queryBuilder->andWhere('p.is_mongolian=:is_mongolian')
                        ->setParameter('is_mongolian', true);
                } else if ($searchEntity->is_mongolian == '2') {
                    $queryBuilder->andWhere('p.is_mongolian=:is_mongolian')
                        ->setParameter('is_mongolian', false);
                }
            }

        }

        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(p.name)')->getQuery()->getSingleScalarResult();

        $entities = $queryBuilder
            ->orderBy('p.name', 'ASC')
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        $artistCountQB = $em->getRepository('musicCmsBundle:AlbumArtist')->createQueryBuilder('e');
        $audioCountQB = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
        foreach ($entities as &$entity) {

            $temp = $entity['id'];

            $artistCount = $artistCountQB
                ->select('count(e.artist)')
                ->where('e.album = :albumid')
                ->setParameter('albumid', $temp)
                ->getQuery()
                ->getSingleScalarResult();
            $entity['artistcount'] = $artistCount;
        }

        foreach ($entities as &$entity) {

            $temp = $entity['id'];

            $audioCount = $audioCountQB
                ->select('count(e.audio)')
                ->where('e.album = :albumid')
                ->setParameter('albumid', $temp)
                ->getQuery()
                ->getSingleScalarResult();
            $entity['audiocount'] = $audioCount;
        }

//        $queryBuilder = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('t')
//            ->select('t')
//            ->addSelect('(select count(nt.audio) from musicCmsBundle:AlbumAudio nt where nt.audio=t.id) as audiocount');
//
//        $entity['aa'] = $queryBuilder->getQuery()->getArrayResult();

//        var_dump( $entity['aa']);
//        exit();
        return $this->render('musicCmsBundle:Album:index.html.twig', array(
            'entities' => $entities,
            'count' => $count,
            'page' => $page,
            'pagecount' => ($count % $pagesize) > 0 ? intval($count / $pagesize) + 1 : intval($count / $pagesize),
            'test' => $this->container->getParameter('statfolder'),
            'searchform' => $searchform->createView(),
            'search' => $search,
            'test' => $searchEntity->name
        ));
    }

    /**
     * Creates a new Album entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Album();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->createDate();
            $entity->getAudioTrackNumber();
            $entity->uploadImage($this->container);


            $em->persist($entity);
            $audios_file = $request->files->get('files');
            $file_names = $request->request->get('filename');

            $albumUpdateUtil = new AlbumUpdateUtil();

            $artalbum = new AlbumArtist();
            $newartistids = array_map(function ($n) {
                return intval($n);
            }, explode(',', $form->get('artistids')->getData()));

            $audios = array();

            if ($audios_file[0] != null) {
                foreach ($audios_file as $index => $audiofile) {

                    $audio = new Audio();
//                    var_dump($file_names[$index]);
//                    exit();
                    $audio->setName($file_names[$index]);
                    $audio->setImg($entity->img);
                    $audio->setPublishDate($entity->publish_date);
                    $audio->setGenre($entity->genre);
                    $audio->audiofile = $audiofile;
                    $em->persist($audio);

                    $audios[] = $audio;

                    $audalbum = new AlbumAudio();
                    $audalbum->setAudio($audio);
                    $audalbum->setAlbum($entity);
                    $em->persist($audalbum);


                    if ($form->get('artistids')->getData() != null) {
                        foreach ($newartistids as $ids) {

                            $audart = new AudioArtist();
                            $artist = $em->getRepository('musicCmsBundle:Artist')->find($ids);
                            $audart->setAudio($audio);
                            $audart->setArtist($artist);
                            $em->persist($audart);
                        }
                    }
                }
            }

            if ($form->get('artistids')->getData() != null) {

                foreach ($newartistids as $ids) {

                    $artist = $em->getRepository('musicCmsBundle:Artist')->find($ids);
                    $artalbum->setArtist($artist);
                    $artalbum->setAlbum($entity);
                    $em->persist($artalbum);

                }
            }
            $em->flush();

            foreach ($audios as $a) {
                $a->uploadAudio($this->container);
                $albumUpdateUtil->updateAudioArtistInfoByAudioObject($this, $a);
                $em->persist($a);
            }
            $em->flush();

            if ($audios_file[0] != null) {

                $albumUpdateUtil->updateAudioByAlbumTypeObject($entity, $this);

            }
            if ($form->get('artistids')->getData() != null) {
                $albumUpdateUtil->updateAlbumArtistInfoByAlbumObject($this, $entity);
            }


            $albumUpdateUtil->updateAlbumTrackInfoByAudio($this, $entity);


            return $this->redirect($this->generateUrl('album_show', array('id' => $entity->getId())));
        }

        return $this->render('musicCmsBundle:Album:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }


    public function addAudioAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('musicCmsBundle:Album')->find($id);
        if (!$album) {
            throw $this->createNotFoundException('Error');
        }

        $albumauds = $album->getAudioalbums();
        $dundiin = new AlbumAudio();
        $oldaudios = new ArrayCollection();
        foreach ($albumauds as $albumaud) {
            $oldaudios->add($albumaud);
        }

        $form = $this->createAudioForm();

        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $newaudioids = array_map(function ($n) {
                    return intval($n);
                }, explode(',', $form->get('audioids')->getData()));

                $qb = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('e');
                $newaudios = $qb
                    ->where($qb->expr()->in('e.id', ':ids'))
                    ->setParameter('ids', $newaudioids)
                    ->getQuery()
                    ->getResult();

                foreach ($oldaudios as $oldaudio) {
                    $found = false;
                    foreach ($newaudios as $newaudio) {
                        if ($newaudio->getId() == $oldaudio->audio->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
//                        throw $this->createNotFoundException();
                        $oldaudio->audio->setAlbumName(null);
                        $oldaudio->audio->setAlbumImg(null);
                        $em->persist($oldaudio->audio);
                        $album->removeAudioalbum($oldaudio);
                        //$em->remove($oldaudio);
                        $em->persist($album);
                    }
                }

                foreach ($newaudios as $newaudio) {
                    $found = false;
                    foreach ($oldaudios as $oldaudio) {
                        if ($oldaudio->audio->getId() == $newaudio->getId()) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $dundiin->album = $album;
                        $dundiin->audio = $newaudio;
                        $dundiin->order = count($newaudios) + 1;
                        $album->addAudioalbum($dundiin);
                    }
                }
                $em->persist($album);
                $em->flush();

                $albumupdateutil = new AlbumUpdateUtil();
                $albumupdateutil->updateAlbumAudioInfoByAlbumObject($this, $album);
                $albumupdateutil->updateAlbumTrackInfoByAudio($this, $album);

                return $this->redirect($this->generateUrl('album', array()));
            }

        }
        return $this->render('musicCmsBundle:AlbumAudio:audio.html.twig', array(
            'id' => $id,
            'form' => $form->createView(),
            'albumarts' => $albumauds,

        ));
    }


    /**
     * Creates a form to create a Album entity.
     *
     * @param Album $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Album $entity)
    {
        $form = $this->createForm(new AlbumType(), $entity, array(
            'action' => $this->generateUrl('album_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Album entity.
     *
     */
    public function newAction()
    {
        $entity = new Album();
        $form = $this->createCreateForm($entity);
        $artist = new Artist();


        return $this->render('musicCmsBundle:Album:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'albumarts' => $artist,
        ));
    }

    /**
     * Finds and displays a Album entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Album')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Album entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('musicCmsBundle:Album:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Album entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Album')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Album entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);


        return $this->render('musicCmsBundle:Album:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Album entity.
     *
     * @param Album $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Album $entity)
    {
        $form = $this->createForm(new AlbumType(), $entity, array(
            'action' => $this->generateUrl('album_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
        'class' => 'btn-success',
    )));

        return $form;
    }

    /**
     * Edits an existing Album entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Album')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Album entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->updateDate();
            $entity->uploadImage($this->container);
            $em->flush();

            if ($entity->ispublish == true) {
                $qb = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');

                $albumaudios = $qb
                    ->where($qb->expr()->eq('e.album', ':album'))
                    ->setParameter('album', $entity)
                    ->getQuery()
                    ->getResult();

                foreach ($albumaudios as $albumaudio) {
                    $audio = $albumaudio->audio;
                    $audio->publish_date = $entity->publish_date;
                    $em->persist($audio);
                }
            }
            $em->flush();


            $albumupdateutil = new AlbumUpdateUtil();
            $albumupdateutil->updateAlbumAudioInfoByAlbumObject($this, $entity);
            $albumupdateutil->updateAlbumTrackInfoByAudio($this, $entity);


            return $this->redirect($this->generateUrl('album_edit', array('id' => $id)));
        }

        return $this->render('musicCmsBundle:Album:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Album entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:Album')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Album entity.');
            }

            if ($entity->img) {
                $fs = new Filesystem();
                try {

                    $imagepath = dirname($entity->img);
                    $pathimage = $this->container->getParameter('imgstatfolder') . $imagepath . '/';

                    if (strpos($imagepath, 'new_album')) {

                        if ($handle = opendir($pathimage)) {

                            $path = $this->container->getParameter('imgstatfolder') . $entity->img;

                            if (strpos($path, 'new_album')) {

                                if ($fs->exists($path, $this->container)) {
                                     $fs->remove($path, $this->container);
                                }
                            }
                            closedir($handle);
                        }
                    }
                } catch (IOException $e) {
                }
            }
             $em->remove($entity);
             $em->flush();
        }

        return $this->redirect($this->generateUrl('album'));
    }

    /**
     * Creates a form to delete a Album entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('album_delete', array('id' => $id)))
            ->setMethod('POST')
            ->add('submit', 'submit', array('label' => 'Устгах', 'attr' => array(
                'class' => 'confirm btn-danger'
            )))
            ->getForm();
    }

    public function getAlbumsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $request->query->get('q');

        $qb = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('e');

        $entities = $qb
            ->select('e.id as id, e.name as name')
            ->where($qb->expr()->like('e.name', ':query'))
            ->setParameter('query', '%' . $q . '%')
            ->getQuery()
            ->getArrayResult();

        return new JsonResponse($entities);
    }

}
