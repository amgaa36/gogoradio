<?php

namespace music\CmsBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use music\CmsBundle\util\AlbumUpdateUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use music\CmsBundle\Entity\AudioArtist;
use music\CmsBundle\Form\AudioArtistType;

/**
 * AudioArtist controller.
 *
 */
class ArtistAudioController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $artist = $em->getRepository('musicCmsBundle:Artist')->find($id);

        if (!$artist) {
            throw $this->createNotFoundException('Artist not found');
        }


        $audarts = $artist->getAudarts();

        return $this->render('musicCmsBundle:AudioArtist:index.html.twig', array(
            'artist' => $artist,
            'artistaudios' => $audarts,
        ));
    }

    public function addAudioAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $artist = $em->getRepository('musicCmsBundle:Artist')->find($id);
        if(!$artist)
        {
            throw $this->createNotFoundException('Error');
        }

        $audarts = $artist->getAudarts();

        $artists = new ArrayCollection();
        foreach($audarts as $audart)
        {
            $artists->add($audart->artist);
        }

        $form = $this->createAudioForm($artists);

        if(strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if($form->isValid()) {
                $newartists = $form->get('artists')->getData();
                foreach($newartists as $newartist) {
                    $k = $em->getRepository('musicCmsBundle:AudioArtist')->findBy(array(
                        'audio' => $artist,
                        'artist' => $newartist,
                    ));

                    if(count($k) > 0) {
                        $k = $k[0];
                    } else {
                        $k = null;
                    }
                    if(!$k) {
                        $newaudarts = new AudioArtist();
                        $newaudarts->setAudio($newartist);
                        $artist->addAudart($newaudarts);
                    }
                }
                $em->persist($artist);
                $em->flush();

                return $this->redirect($this->generateUrl('audio', array(
                    'page' => 1,
                )));
            }
        }

        return $this->render('musicCmsBundle:AudioArtist:new.html.twig', array(
            'audioid' => $id,
            'form' => $form->createView(),

        ));
    }

    private function createAudioForm($audio)
    {
        return $this->createFormBuilder()
            ->setMethod("POST")
            ->add('artists', 'entity', array(
                'label' => 'Дуунууд',
                'class' => 'musicCmsBundle:Audio',
                'property' => 'name',
                'multiple' => false,
                'expanded' => true,
                'data' => $audio,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
            ))
            ->getForm()
            ;
    }


    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:AudioArtist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Artist not found');
        }

        $audioid = $entity->audio->getId();
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('artist_audios', array(
            'id' => $audioid,
        )));
    }


    public function indexAudAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$audio) {
            throw $this->createNotFoundException('Audio not found');
        }

        $audi = $audio->getAudioArtists();

        return $this->render('musicCmsBundle:AudioArtist:audio.html.twig', array(
            'audio' => $audio,
            'artistaudios' => $audi,
        ));


    }


    public function addAudAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);
        $audioname = $audio->getName();
        if(!$audio)
        {
            throw $this->createNotFoundException('Error');
        }

        $audarts = $audio->getAudioArtists();

        $oldaudarts = new ArrayCollection();
        foreach($audarts as $audart)
        {
            $oldaudarts->add($audart);
        }

        $form = $this->createArtistForm();

        if(strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if($form->isValid()) {

                $newartistids = array_map(function ($n) {
                    return intval($n);
                }, explode(',', $form->get('artistids')->getData()));

                $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('e');

                $newartists = $qb
                    ->where($qb->expr()->in('e.id', ':ids'))
                    ->setParameter('ids', $newartistids)
                    ->getQuery()
                    ->getResult();

                foreach($oldaudarts as $oldaudart) {
                    $found = false;
                    foreach($newartists as $newartist) {
                        if($newartist->getId() == $oldaudart->artist->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if(!$found) {
                        $em->remove($oldaudart);
                    }
                }

                foreach ($newartists as $newartist) {
                    $found = false;
                    foreach($oldaudarts as $oldaudart) {
                        if($oldaudart->artist->getId() == $newartist->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if(!$found) {
                        $dundiin = new AudioArtist();
                        $dundiin->artist = $newartist;
                        $dundiin->audio = $audio;
                        $dundiin->order = count($newartists) + 1;
                        $em->persist($dundiin);
                    }
                }

                $em->flush();

                $audioupdateutil = new AlbumUpdateUtil();
                $audioupdateutil->updateAudioArtistInfoByAudioObject($this, $audio);

                return $this->redirect($this->generateUrl('audio', array(
                    'id' => $id,
                )));
            }
        }

        return $this->render('musicCmsBundle:AudioArtist:new.html.twig', array(
            'audioid' => $id,
            'form' => $form->createView(),
            'audarts' => $audarts,
            'audioname' => $audioname,

        ));

    }

    private function createArtistForm()
    {
        return $this->createFormBuilder()
            ->setMethod("POST")
            ->add('artistids', 'text', array(
                'label' => 'Уран бүтээлчид',
                'required' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
            ))
            ->getForm()
        ;
    }


    public function addAudRemoveAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:AudioArtist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Audio not found');
        }

        $audio = $entity->audio;
        $em->remove($entity);
        $em->flush();

        $audioupdateutil = new AlbumUpdateUtil();
        $audioupdateutil->updateAudioArtistInfoByAudioObject($this, $audio);

        return $this->redirect($this->generateUrl('audio_art', array(
            'id' => $audio->getId(),
        )));
    }

    public function getArtistsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $request->query->get('q');

        $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('e');

        $entities = $qb
            ->select('e.id as id, e.name as name')
            ->where($qb->expr()->like('e.name', ':query'))
            ->setParameter('query', '%'.$q.'%')
            ->getQuery()
            ->getArrayResult()
        ;

        return new JsonResponse($entities);
    }
}
