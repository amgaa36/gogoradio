<?php

namespace music\CmsBundle\Controller;

use music\CmsBundle\Entity\GenreAudio;
use music\CmsBundle\util\AlbumUpdateUtil;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\Artist;
use music\CmsBundle\Entity\ArtistAudio;
use music\CmsBundle\Form\ArtistType;
use music\CmsBundle\Form\ArtistSearchType;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Artist controller.
 *
 * @Route("/artist")
 */
class ArtistController extends Controller
{

    /**
     * Lists all Artist entities.
     *
     * @Route("/", name="artist")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($page, Request $request)
    {
        $pagesize = 50;
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('p');

        $searchEntity = new Artist ();
        $searchform = $this->createForm(new ArtistSearchType(), $searchEntity);
        $search = false;

        if ($request->get("submit") == 'submit') {
            $searchform->bind($request);
            $search = true;
        }


        if ($search) {
            if ($searchEntity->name && $searchEntity->name != '') {
                $queryBuilder->andWhere('LOWER(p.name) like LOWER(:name)')
                    ->setParameter('name', '%' . $searchEntity->name . '%');
            }
            if ($searchform->get('img')->getData()) {
                if ($searchEntity->img == '1') {
                    $queryBuilder->andWhere('p.img_is_big=:img_is_big')
                        ->setParameter('img_is_big', true);
                } else if ($searchEntity->img == '2') {
                    $queryBuilder->andWhere('p.img_is_big=:img_is_big')
                        ->setParameter('img_is_big', false);
                }
            }

            if ($searchform->get('published_date')->getData()) {
                if ($searchEntity->published_date == '1') {

                    $queryBuilder->andWhere('p.published_date < :published_date')

                        ->setParameter('published_date', new \DateTime('now'));

                } else if ($searchEntity->published_date == '2') {
                    $queryBuilder->andWhere('p.published_date > :published_date')
                        ->setParameter('published_date', new \DateTime('now'));
                } else if ($searchEntity->published_date == '3') {
                    $queryBuilder->andWhere('p.published_date is null');
                }
            }

            if ($searchform->get('is_mongolian')->getData()) {
                if ($searchEntity->is_mongolian == '1') {
                    $queryBuilder->andWhere('p.is_mongolian=:is_mongolian')
                        ->setParameter('is_mongolian', true);
                } else if ($searchEntity->is_mongolian == '2') {
                    $queryBuilder->andWhere('p.is_mongolian=:is_mongolian')
                        ->setParameter('is_mongolian', false);
                }
            }
        }

        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(p.name)')->getQuery()->getSingleScalarResult();

        $audioCountQB = $em->getRepository("musicCmsBundle:AudioArtist")->createQueryBuilder("aua");
        $albumCountQB = $em->getRepository("musicCmsBundle:AlbumArtist")->createQueryBuilder("aia");


        $entities = $queryBuilder
            ->orderBy('p.name', 'ASC')
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();


        foreach ($entities as &$entity) {
            $temp = $entity['id'];

            $audioCount = $audioCountQB->select('count(aua.id)')->where('aua.artist = :arid')->setParameter('arid', $temp)->getQuery()->getSingleScalarResult();
            $entity['audiocount'] = $audioCount;
        }

        foreach ($entities as &$entity) {
            $temp = $entity['id'];

            $albumCount = $albumCountQB->select('count(aia.id)')->where('aia.artist = :arid')->setParameter('arid', $temp)->getQuery()->getSingleScalarResult();
            $entity['albumcount'] = $albumCount;
        }


        return $this->render('musicCmsBundle:Artist:index.html.twig', array(
            'entities' => $entities,
            'count' => $count,
            'page' => $page,
            'pagecount' => ($count % $pagesize) > 0 ? intval($count / $pagesize) + 1 : intval($count / $pagesize),
            'test' => $this->container->getParameter('statfolder'),
            'searchform' => $searchform->createView(),
            'search' => $search,
            'test' => $searchEntity->name
        ));


    }

    public function artaudsAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $audarts = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('e');

        $entities = $audarts
            ->leftJoin('e.artist', 'ar')
            ->leftJoin('e.audio', 'au')
            ->addSelect('ar')
            ->addSelect('au')
            ->where('ar.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:Artist:artistaudios.html.twig', array('entities' => $entities));
    }

    /**
     * Creates a new Artist entity.
     *
     * @Route("/", name="artist_create")
     * @Method("POST")
     * @Template("musicCmsBundle:Artist:new.html.twig")
     */
    public
    function createAction(Request $request)
    {
        $entity = new Artist();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $entity->uploadImage($this->container);
            $entity->uploadCover($this->container);
            if ($entity->img and $entity->cover) {
                $em->persist($entity);
                $em->flush();
            }
//            $entity->uploadAudio($this->container);


            $em->flush();
            return $this->redirect($this->generateUrl('artist_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Artist entity.
     *
     * @param Artist $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createCreateForm(Artist $entity)
    {
        $form = $this->createForm(new ArtistType(), $entity, array(
            'action' => $this->generateUrl('artist_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
            'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Artist entity.
     *
     * @Route("/new", name="artist_new")
     * @Method("GET")
     * @Template()
     */
    public
    function newAction()
    {
        $entity = new Artist();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Artist entity.
     *
     * @Route("/{id}", name="artist_show")
     * @Method("GET")
     * @Template()
     */
    public
    function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Artist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artist entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Artist entity.
     *
     * @Route("/{id}/edit", name="artist_edit")
     * @Method("GET")
     * @Template()
     */
    public
    function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Artist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artist entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Artist entity.
     *
     * @param Artist $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createEditForm(Artist $entity)
    {
        $form = $this->createForm(new ArtistType(), $entity, array(
            'action' => $this->generateUrl('artist_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Хадгалах', 'attr' => array(
            'class' => 'btn-success',
        )));

        return $form;
    }

    /**
     * Edits an existing Artist entity.
     *
     * @Route("/{id}", name="artist_update")
     * @Method("PUT")
     * @Template("musicCmsBundle:Artist:edit.html.twig")
     */
    public
    function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Artist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artist entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->uploadImage($this->container);
            $entity->uploadCover($this->container);
            $em->persist($entity);
            $em->flush();

            $albumarts = $em->getRepository('musicCmsBundle:AlbumArtist')->findBy(array(
                'artist' => $entity,
            ));

            $audarts = $em->getRepository('musicCmsBundle:AudioArtist')->findBy(array(
                'artist' => $entity,
            ));

            $albumupdateutil = new AlbumUpdateUtil();

            foreach ($albumarts as $albumart) {
                $albumupdateutil->updateAlbumArtistInfoByAlbumObject($this, $albumart->album);

            }

            foreach ($audarts as $audart) {
                $albumupdateutil->updateAudioArtistInfoByAudioObject($this, $audart->audio);
            }

            $qb = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('e');

            $artistaudios = $qb
                ->where($qb->expr()->eq('e.artist', ':artist'))
                ->setParameter('artist', $entity)
                ->getQuery()
                ->getResult();

            $dt = new \DateTime('2015-08-13 00:0:00.000000');
            $dm = new \DateTime('2100-08-13 00:0:00.000000');
            if ($entity->ispublish == true) {
                foreach ($artistaudios as $artistaudio) {
                    $audio = $artistaudio->audio;
                    $audio->publish_date = $dt;
                    $em->persist($audio);
                }
            } else {
                foreach ($artistaudios as $artistaudio) {
                    $audio = $artistaudio->audio;
                    $audio->publish_date = $dm;
                    $em->persist($audio);
                }
            }


            $em->flush();


//            return $this->redirect($this->generateUrl('artist_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Artist entity.
     *
     * @Route("/{id}", name="artist_delete")
     * @Method("DELETE")
     */
    public
    function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:Artist')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Artist entity.');
            }

            if ($entity->img) {
                $fs = new Filesystem();
                try {
                    $imagepath = dirname($entity->img);
                    $pathimage = $this->container->getParameter('imgstatfolder') . $imagepath . '/';

                    if (strpos($imagepath, 'new_artist')) {

                        if ($handle = opendir($pathimage)) {

                            $path = $this->container->getParameter('imgstatfolder') . $entity->img;

                            if (strpos($path, 'new_artist')) {

                                if ($fs->exists($path, $this->container)) {
                                    $fs->remove($path, $this->container);
                                }
                            }
                            closedir($handle);
                        }
                    }
                } catch (IOException $e) {
                }
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('artist'));
    }

    /**
     * Creates a form to delete a Artist entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('artist_delete', array('id' => $id)))
            ->setMethod('POST')
            ->add('submit', 'submit', array('label' => 'Устгах', 'attr' => array(
                'class' => 'confirm btn-danger'
            )))
            ->getForm();
    }

    public
    function setGenreAudioAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:Artist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artist entity.');
        }

        $form = $this->createAudioForm();
        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $qb = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('e');

                $artistaudios = $qb
                    ->where($qb->expr()->eq('e.artist', ':artist'))
                    ->setParameter('artist', $entity)
                    ->getQuery()
                    ->getResult();

                $orig_audios = array();

                foreach ($artistaudios as $k) {
                    $orig_audios[] = $k->audio;
                }


                $qb = $em->getRepository('musicCmsBundle:GenreAudio')->createQueryBuilder('e');
                $qb1 = clone $qb;
                $genreaudios = $qb
                    ->where($qb->expr()->in('e.audio', ':audios'))
                    ->setParameter('audios', $orig_audios)
                    ->getQuery()
                    ->getResult();

                $qb1
                    ->delete()
                    ->where($qb1->expr()->in('e.audio', ':audios'))
                    ->setParameter('audios', $orig_audios)
                    ->getQuery()
                    ->execute();

                foreach ($orig_audios as $orig_audio) {
                    foreach ($form->get('genres')->getData() as $genre) {
                        $genreaudio = new GenreAudio();
                        $genreaudio->setGenre($genre);
                        $genreaudio->setAudio($orig_audio);
                        $em->persist($genreaudio);
                    }
                }

                $em->flush();
                return $this->redirect($this->generateUrl('artist'));
            }
        }
        return $this->render('musicCmsBundle:Artist:audio.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    private
    function createAudioForm()
    {
        return $this->createFormBuilder()
            ->setMethod("POST")
            ->add('genres', 'entity', array(
                'label' => 'Жанр',
                'class' => 'musicCmsBundle:Genre',
                'property' => 'name',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
            ))
            ->getForm();
    }


}
