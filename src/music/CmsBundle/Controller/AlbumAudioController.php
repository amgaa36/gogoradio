<?php

namespace music\CmsBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use music\CmsBundle\Entity\AlbumArtist;
use music\CmsBundle\util\AlbumUpdateUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\AlbumAudio;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * AlbumAudio controller.
 */
class AlbumAudioController extends Controller
{


    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Audio not found');
        }

        $aud = $entity->getAudioalbums();
        return $this->render('musicCmsBundle:AlbumAudio:index.html.twig', array(
            'entity' => $entity,
            'audioalbums' => $aud,
        ));

    }


    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:AlbumAudio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AlbumAudio entity.');
        }

        return array(
            'entity' => $entity,
        );
    }


    public function addAlbumAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);
        if (!$audio) {
            throw $this->createNotFoundException('Error');
        }

        $albumarts = $audio->getAudioalbums();

        $oldalbums = new ArrayCollection();
        foreach ($albumarts as $album) {
            $oldalbums->add($album);
        }

        $form = $this->createAlbumForm();

        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $remalbumids = array();
                $newalbumsids = array_map(function ($n) {
                    return intval($n);
                }, explode(',', $form->get('albumids')->getData()));

                $qb = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('e');
                $newalbums = $qb
                    ->where($qb->expr()->in('e.id', ':ids'))
                    ->setParameter('ids', $newalbumsids)
                    ->getQuery()
                    ->getResult();

                foreach ($oldalbums as $oldalbum) {
                    $found = false;
                    foreach ($newalbums as $newalbum) {
                        if ($newalbum->getId() == $oldalbum->album->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $remalbumids[] = $oldalbum->album->getId();
                        $em->remove($oldalbum);
                    }
                }
                $max_time = null; //new \DateTime('2000-01-01 01:01:01');
                $audioImageurl = "";
                foreach ($newalbums as $newalbum) {
                    if ($max_time == null || $max_time < $newalbum->publish_date)
                        $max_time = $newalbum->publish_date;
                    $found = false;
                    if($newalbum->getImg()){
                        $audioImageurl = $newalbum->getImg();
                    }
                    foreach ($oldalbums as $oldalbum) {
                        if ($oldalbum->album->getId() == $newalbum->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $dundiin = new AlbumAudio();
                        $dundiin->album = $newalbum;
                        $dundiin->audio = $audio;
                        $dundiin->order = count($newalbums) + 1;
                        $em->persist($dundiin);
                    }

                }

                if($audioImageurl != ""){
                    $audio->setAlbumImg($audioImageurl);
                }

                if ($audio->publish_date < $max_time) {
                    $audio->setBeforeAlbum(true);
                    $em->persist($audio);
                } else {
                    $audio->setBeforeAlbum(false);
                    $em->persist($audio);
                }

                $em->flush();

                $albumupdateutil = new AlbumUpdateUtil();
                $albumupdateutil->updateAudioAlbumInfoByAudioObject($this, $audio);
                $albumupdateutil->updateAudioTrackInfoByAlbum($this, $audio, $remalbumids);

                return $this->redirect($this->generateUrl('audio', array()));
            }


        }

        $qb = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
        $audioid = $qb
            ->where('e.audio = :id')
            ->setParameter('id', $audio)
            ->getQuery()
            ->getResult();


        return $this->render('musicCmsBundle:AlbumAudio:show.html.twig', array(
            'id' => $id,
            'form' => $form->createView(),
            'albumarts' => $albumarts,
            'audioid'=>$audioid,

        ));
    }

    private function createAlbumForm()
    {
        return $this->createFormBuilder()
            ->setMethod("POST")
            ->add('albumids', 'text', array(
                'label' => 'Цомгууд',
                'required' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
            ))
            ->getForm();
    }

    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:AlbumAudio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Album not found');
        }

        $audio = $entity->audio;

        $em->remove($entity);
        $em->flush();

        $albumupdateutil = new AlbumUpdateUtil();
        $albumupdateutil->updateAudioAlbumInfoByAudioObject($this, $audio);

        return $this->redirect($this->generateUrl('audio_album_add', array(
            'id' => $audio->getId(),

        )));
    }

    public function getSelectedAlbumAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $request->query->get('q');

        $qb = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('e');
        $entities = $qb
            ->select('e.id as id, e.name as name')
            ->where($qb->expr()->like('e.name', ':query'))
            ->setParameter('query', '%' . $q . '%')
            ->getQuery()
            ->getArrayResult();

        return new JsonResponse($entities);
    }

    /**
     * Lists all AlbumAudio entities.
     *
     * @Route("/", name="albumaudio")
     * @Method("GET")
     * @Template()
     */
    public function addAudioAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $album = $em->getRepository('musicCmsBundle:Album')->find($id);

        if (!$album) {

        }
        $plist = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
        $albumaudios = $plist
            ->leftJoin('e.audio', 'audio')
            ->addSelect('audio')
            ->where($plist->expr()->eq('e.album', ':album'))
            ->setParameter('album', $album)
            ->orderBy('e.order', 'ASC')
            ->getQuery()
            ->getResult();


        return $this->render('musicCmsBundle:AlbumAudio:audio.html.twig', array(
            'albumaudios' => $albumaudios,
            'album' => $album,


        ));

    }


    public function getSelectedAudioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $request->query->get('q');

        $qb = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('e');
        $entities = $qb
            ->select('e.id as id, e.name as name')
            ->where($qb->expr()->like('e.name', ':query'))
            ->orderBy('e.name', 'ASC')
            ->setParameter('query', '%' . $q . '%')
            ->setMaxResults(20)
            ->getQuery()
            ->getArrayResult();

        return new JsonResponse($entities);
    }


    public function changeOrderAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $ids = $request->request->get('ids');

        foreach ($ids as $index => $alauid) {
            $entity = $em->getRepository('musicCmsBundle:AlbumAudio')->find($alauid);
            if (!$entity) continue;
            $entity->order = $index;
            $em->persist($entity);
        }

        $em->flush();

        return new JsonResponse(array(
            'status' => 'success',
        ));
    }


}
