<?php

namespace music\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use music\CmsBundle\Entity\AudioBanner;
use music\CmsBundle\Form\AudioBannerType;

/**
 * AudioBanner controller.
 *
 */
class AudioBannerController extends Controller
{

    /**
     * Lists all AudioBanner entities.
     *
     */
    public function indexAction($page, Request $request)
    {
        $pagesize = 50;
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('musicCmsBundle:AudioBanner')->createQueryBuilder('p');

        $entities = $queryBuilder
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(p.id)')->getQuery()->getSingleScalarResult();


        return $this->render('musicCmsBundle:AudioBanner:index.html.twig', array(
            'entities' => $entities,
            'page' => $page,
            'count' => $count,
            'pagecount' => ($count % $pagesize) > 0 ? intval($count / $pagesize) + 1 : intval($count / $pagesize),
        ));
    }
    /**
     * Creates a new AudioBanner entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new AudioBanner();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->uploadAudio($this->container);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('audiobanner'));
        }

        return $this->render('musicCmsBundle:AudioBanner:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a AudioBanner entity.
     *
     * @param AudioBanner $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AudioBanner $entity)
    {
        $form = $this->createForm(new AudioBannerType(), $entity, array(
            'action' => $this->generateUrl('audiobanner_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AudioBanner entity.
     *
     */
    public function newAction()
    {
        $entity = new AudioBanner();
        $form   = $this->createCreateForm($entity);

        return $this->render('musicCmsBundle:AudioBanner:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a AudioBanner entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:AudioBanner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AudioBanner entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('musicCmsBundle:AudioBanner:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AudioBanner entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:AudioBanner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AudioBanner entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

//        $editForm->handleRequest($request);
//
//        if ($editForm->isValid()) {
//            $entity->uploadAudio($this->container);
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('audiobanner', array(
//                'page' => 1,
//            )));
//        }

        return $this->render('musicCmsBundle:AudioBanner:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a AudioBanner entity.
    *
    * @param AudioBanner $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AudioBanner $entity)
    {
        $form = $this->createForm(new AudioBannerType(), $entity, array(
            'action' => $this->generateUrl('audiobanner_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing AudioBanner entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:AudioBanner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AudioBanner entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->uploadAudio($this->container);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('audiobanner', array(
                'page' => 1,
            )));
        }

        return $this->render('musicCmsBundle:AudioBanner:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a AudioBanner entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:AudioBanner')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AudioBanner entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('audiobanner'));
    }

    /**
     * Creates a form to delete a AudioBanner entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('audiobanner_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
