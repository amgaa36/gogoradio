<?php

namespace music\CmsBundle\Controller;

use music\CmsBundle\Form\PlaylistSearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\Playlist;
use music\CmsBundle\Form\PlaylistType;

/**
 * Playlist controller.
 *
 * @Route("/playlist")
 */
class PlaylistController extends Controller
{

    /**
     * Lists all Playlist entities.
     *
     * @Route("/", name="playlist")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($page, Request $request)
    {
        $pagesize = 20;
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('musicCmsBundle:Playlist')->createQueryBuilder('p');

        $searchEntity = new Playlist ();
        $searchform = $this->createForm(new PlaylistSearchType(), $searchEntity);
        $search = false;

        if ($request->get("submit") == 'submit') {
            $searchform->bind($request);
            $search = true;
        }

        if ($search) {
            if($searchEntity->created_date!=null)
            {
            $aa = $searchEntity->created_date->format('Y-m-d');}

            if ($searchEntity->name && $searchEntity->name != '') {
                $queryBuilder->andWhere('LOWER(p.name) like LOWER(:name)')
                    ->setParameter('name', '%' . $searchEntity->name . '%');
            }

            if ($searchEntity->track_number && $searchEntity->track_number != '') {
                $queryBuilder->andWhere('LOWER(p.track_number) like LOWER(:track_number)')
                    ->setParameter('track_number', '%' . $searchEntity->track_number . '%');
            }

            if ($searchEntity->created_date && $searchEntity->created_date != '') {
                $queryBuilder->andWhere('LOWER(p.created_date) like LOWER(:created_date)')
                    ->setParameter('created_date','%'.$aa.'%');
            }

            if ($searchEntity->likeCount && $searchEntity->likeCount != '') {
                $queryBuilder->andWhere('LOWER(p.likeCount) like LOWER(:likeCount)')
                    ->setParameter('likeCount', '%' . $searchEntity->likeCount . '%');
            }
        }
        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(p.name)')->getQuery()->getSingleScalarResult();

        $entities = $queryBuilder
            ->orderBy('p.name', 'ASC')
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:Playlist:index.html.twig', array(
            'entities' => $entities,
            'page' => $page,
            'count' => $count,
            'pagecount' => ($count % $pagesize) > 0 ? intval($count / $pagesize) + 1 : intval($count / $pagesize),
            'test' => $this->container->getParameter('statfolder'),
            'searchform' => $searchform->createView(),
            'search' => $search,
            'test' => $searchEntity->name
        ));
    }

    /**
     * Creates a new Playlist entity.
     *
     * @Route("/", name="playlist_create")
     * @Method("POST")
     * @Template("musicCmsBundle:Playlist:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Playlist();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('playlist_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Playlist entity.
     *
     * @param Playlist $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Playlist $entity)
    {
        $form = $this->createForm(new PlaylistType(), $entity, array(
            'action' => $this->generateUrl('playlist_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Шинээр үүсгэх', 'attr' => array(

                'class' => 'btn btn-success',
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Playlist entity.
     *
     * @Route("/new", name="playlist_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Playlist();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Playlist entity.
     *
     * @Route("/{id}", name="playlist_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Playlist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Playlist entity.
     *
     * @Route("/{id}/edit", name="playlist_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Playlist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Playlist entity.
     *
     * @param Playlist $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Playlist $entity)
    {
        $form = $this->createForm(new PlaylistType(), $entity, array(
            'action' => $this->generateUrl('playlist_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Засах', 'attr' => array(

            'class' => 'btn btn-success',
        )));

        return $form;
    }

    /**
     * Edits an existing Playlist entity.
     *
     * @Route("/{id}", name="playlist_update")
     * @Method("PUT")
     * @Template("musicCmsBundle:Playlist:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Playlist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('playlist'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Playlist entity.
     *
     * @Route("/{id}", name="playlist_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:Playlist')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Playlist entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('playlist'));
    }

    /**
     * Creates a form to delete a Playlist entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('playlist_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Устгах', 'attr' => array(

                'class' => 'confirm btn-danger',
            )))
            ->getForm();
    }
}
