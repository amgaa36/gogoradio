<?php

namespace music\CmsBundle\Controller;

use music\CmsBundle\Entity\AudioVideo;
use music\CmsBundle\Entity\Audio;
use music\CmsBundle\util\VideoUpdateUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * VideoAudio controller.
 *
 */
class VideoAudioController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $video = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$video) {
            throw $this->createNotFoundException('video not found');
        }

        $qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');

        $entities = $qb
            ->leftJoin('e.audio', 'audio')
            ->addSelect('audio')
            ->where($qb->expr()->eq('e.video', ':video'))
            ->setParameter('video', $video)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:VideoAudio:index.html.twig', array(
            'entities' => $entities,
            'video'    => $video,
        ));
    }


    private function createSearchForm($link = null)
    {
        return $this->createFormBuilder()
            ->setMethod('GET')
            ->setAction($link)
            ->add('name', 'text', array(
                'label' => 'Нэр',
                'required' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хайх',
                'attr'  => array('class' => 'btn btn-success'),
            ))
            ->getForm();
    }

    public function seekAction(Request $request, $id, $page)
    {
        $pagesize = 20;

        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$video) {
            throw $this->createNotFoundException('video');
        }

        $qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');
        $sel_vidoe_ids = $qb
            ->select('e.audio_id')
            ->where($qb->expr()->eq('e.video', ':video'))
            ->setParameter('video', $video)
            ->getQuery()
            ->getScalarResult();

        $qb = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('e');
        if(count($sel_vidoe_ids) > 0) {
            $qb
                ->where($qb->expr()->notIn('e.id', ':ids'))
                ->setParameter('ids', $sel_vidoe_ids);
        }
        $search_form = $this->createSearchForm($this->generateUrl('video_audio_seek', array(
            'id' => $id,
        )));

        $search_form->handleRequest($request);

        if($search_form->isValid()) {
            $data = $search_form->get('name')->getData();
            if(!is_null($data)) {
                $qb
                    ->andWhere($qb->expr()->like($qb->expr()->lower('e.name'), $qb->expr()->lower(':name')))
                    ->setParameter('name', '%' . $data . '%');
            }
        }

        $qb_total = clone $qb;
        $total = $qb_total->select($qb_total->expr()->count('e.id'))->getQuery()->getSingleScalarResult();

        $entities = $qb
            ->setFirstResult(($page-1)*$pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:VideoAudio:seek.html.twig', array(
            'video' => $video,
            'entities' => $entities,
            'search_form' => $search_form->createView(),
            'total' => $total,
            'pagesize' => $pagesize,
            'page' => $page,
        ));
    }

    public function addAction($id, $sid)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$video) {
            throw $this->createNotFoundException('video');
        }

        $audio = $em->getRepository('musicCmsBundle:Audio')->find($sid);

        if (!$audio) {
            throw $this->createNotFoundException('audio');
        }

        $entity = new AudioVideo();
        $entity->setVideo($video);
        $entity->setAudio($audio);

        $em->persist($entity);
        $em->flush();

        $videoUtil = new VideoUpdateUtil();
        $videoUtil->updateVideoArtist($this->container, $video);

        return new JsonResponse(array(
            'status' => 'success',
        ));
    }

    public function removeAction($id, $sid)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('musicCmsBundle:Video')->find($id);

        if (!$video) {
            throw $this->createNotFoundException('video');
        }

        $audio = $em->getRepository('musicCmsBundle:Audio')->find($sid);

        if (!$audio) {
            throw $this->createNotFoundException('audio');
        }

        $qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');
        $qb->delete()
            ->where($qb->expr()->eq('e.video', ':video'))
            ->andWhere($qb->expr()->eq('e.audio', ':audio'))
            ->setParameter('video', $video)
            ->setParameter('audio', $audio)
            ->getQuery()
            ->execute();

        $videoUtil = new VideoUpdateUtil();
        $videoUtil->updateVideoArtist($this->container, $video);

        return new JsonResponse(array(
            'status' => 'success',
        ));
    }
}
