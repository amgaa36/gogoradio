<?php

namespace music\CmsBundle\Controller;

use music\CmsBundle\Entity\AudioVideo;
use music\CmsBundle\Entity\Video;
use music\CmsBundle\util\VideoUpdateUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * AudioVideo controller.
 *
 */
class AudioVideoController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$audio) {
            throw $this->createNotFoundException('audio not found');
        }

        $qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');

        $entities = $qb
            ->leftJoin('e.video', 'video')
            ->addSelect('video')
            ->where($qb->expr()->eq('e.audio', ':audio'))
            ->setParameter('audio', $audio)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:AudioVideo:index.html.twig', array(
            'entities' => $entities,
            'audio' => $audio,
        ));
    }


    private function createSearchForm($link = null)
    {
        return $this->createFormBuilder()
            ->setMethod('GET')
            ->setAction($link)
            ->add('name', 'text', array(
                'label' => 'Нэр',
                'required' => false,
            ))
            ->add('url', 'text', array(
                'required' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хайх',
                'attr' => array('class' => 'btn btn-success'),
            ))
            ->getForm();
    }

    public function seekAction(Request $request, $id, $page)
    {
        $pagesize = 20;

        $em = $this->getDoctrine()->getManager();
        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$audio) {
            throw $this->createNotFoundException('audio');
        }

        $qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');
        $sel_vidoe_ids = $qb
            ->select('e.video_id')
            ->where($qb->expr()->eq('e.audio', ':audio'))
            ->setParameter('audio', $audio)
            ->getQuery()
            ->getScalarResult();

        $qb = $em->getRepository('musicCmsBundle:Video')->createQueryBuilder('e');
        if (count($sel_vidoe_ids) > 0) {
            $qb
                ->where($qb->expr()->notIn('e.id', ':ids'))
                ->setParameter('ids', $sel_vidoe_ids);
        }
        $search_form = $this->createSearchForm($this->generateUrl('audio_video_seek', array(
            'id' => $id,
        )));

        $search_form->handleRequest($request);

        if ($search_form->isValid()) {
            $data = $search_form->get('name')->getData();
            if (!is_null($data)) {
                $qb
                    ->andWhere($qb->expr()->like($qb->expr()->lower('e.name'), $qb->expr()->lower(':name')))
                    ->setParameter('name', '%' . $data . '%');
            }
            $data = $search_form->get('url')->getData();
            if (filter_var($data, FILTER_VALIDATE_URL)) {
                $data = Video::getYoutubeID($data);
            }
            if (!is_null($data)) {
                $qb
                    ->andWhere($qb->expr()->eq('e.videoUrl', ':videoUrl'))
                    ->setParameter('videoUrl', $data);
            }
        }

        $qb_total = clone $qb;
        $total = $qb_total->select($qb_total->expr()->count('e.id'))->getQuery()->getSingleScalarResult();

        $entities = $qb
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getArrayResult();

        return $this->render('musicCmsBundle:AudioVideo:seek.html.twig', array(
            'audio' => $audio,
            'entities' => $entities,
            'search_form' => $search_form->createView(),
            'total' => $total,
            'pagesize' => $pagesize,
            'page' => $page,
        ));
    }

    public function addAction($id, $sid)
    {
        $em = $this->getDoctrine()->getManager();
        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$audio) {
            throw $this->createNotFoundException('audio');
        }

        $video = $em->getRepository('musicCmsBundle:Video')->find($sid);

        if (!$video) {
            throw $this->createNotFoundException('video');
        }

        $entity = new AudioVideo();
        $entity->setAudio($audio);
        $entity->setVideo($video);

        $audio->is_video = 1;

        $em->persist($audio);
        $em->flush();

        $em->persist($entity);
        $em->flush();

        $videoUtil = new VideoUpdateUtil();
        $videoUtil->updateVideoArtist($this->container, $video);

        return new JsonResponse(array(
            'status' => 'success',
        ));
    }

    public function removeAction($id, $sid)
    {
        $em = $this->getDoctrine()->getManager();
        $audio = $em->getRepository('musicCmsBundle:Audio')->find($id);

        if (!$audio) {
            throw $this->createNotFoundException('audio');
        }

        $video = $em->getRepository('musicCmsBundle:Video')->find($sid);

        if (!$video) {
            throw $this->createNotFoundException('video');
        }

        $qb_audio = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');




        $em->persist($audio);
        $em->flush();

        $qb = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');
        $qb->delete()
            ->where($qb->expr()->eq('e.audio', ':audio'))
            ->andWhere($qb->expr()->eq('e.video', ':video'))
            ->setParameter('audio', $audio)
            ->setParameter('video', $video)
            ->getQuery()
            ->execute();

        $audioalbum = $qb_audio
            ->leftJoin('e.video', 'a')
            ->addSelect('a')
            ->where($qb_audio->expr()->eq('e.audio', ':audio'))
            ->setParameter('audio', $audio)
            ->getQuery()
            ->getArrayResult();


        $count = count($audioalbum);

        if ($count == 0) {
            $audio->is_video = 0;
        }

        $videoUtil = new VideoUpdateUtil();
        $videoUtil->updateVideoArtist($this->container, $video);
        return $this->redirect($this->generateUrl('audio_video', array(
            'id' => $id,
        )));
    }
}
