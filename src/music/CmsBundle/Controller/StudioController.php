<?php

namespace music\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\Studio;
use music\CmsBundle\Form\StudioType;

/**
 * Studio controller.
 *
 * @Route("/studio")
 */
class StudioController extends Controller
{

    /**
     * Lists all Studio entities.
     *
     * @Route("/", name="studio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('musicCmsBundle:Studio')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Studio entity.
     *
     * @Route("/", name="studio_create")
     * @Method("POST")
     * @Template("musicCmsBundle:Studio:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Studio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('studio_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Studio entity.
     *
     * @param Studio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Studio $entity)
    {
        $form = $this->createForm(new StudioType(), $entity, array(
            'action' => $this->generateUrl('studio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Studio entity.
     *
     * @Route("/new", name="studio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Studio();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Studio entity.
     *
     * @Route("/{id}", name="studio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Studio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Studio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Studio entity.
     *
     * @Route("/{id}/edit", name="studio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Studio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Studio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Studio entity.
    *
    * @param Studio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Studio $entity)
    {
        $form = $this->createForm(new StudioType(), $entity, array(
            'action' => $this->generateUrl('studio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Оруулах', 'attr'=> array(
                 'class'=>'btn btn-success',
             )));

        return $form;
    }
    /**
     * Edits an existing Studio entity.
     *
     * @Route("/{id}", name="studio_update")
     * @Method("PUT")
     * @Template("musicCmsBundle:Studio:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:Studio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Studio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('studio_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Studio entity.
     *
     * @Route("/{id}", name="studio_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('musicCmsBundle:Studio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Studio entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('studio'));
    }

    /**
     * Creates a form to delete a Studio entity by id.s
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('studio_delete', array('id' => $id)))
            ->setMethod('POST')
            ->add('submit', 'submit', array('label' => 'Устгах', 'attr' =>array(

                'class'=>'confirm btn-danger'

            )))
            ->getForm()
        ;
    }
}
