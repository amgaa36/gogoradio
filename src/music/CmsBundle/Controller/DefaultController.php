<?php

namespace music\CmsBundle\Controller;

use music\CmsBundle\util\CheckUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        if ($request->query->has('ffmpeg')) {
            $output = array();
            $res = -1;
//        $command = "/usr/local/bin/ffmpeg -version 2>&1" ;
            $command = "/usr/local/bin/ffmpeg -version 2>&1";
            echo $command . '<br>';
            exec($command, $output, $res);
            var_dump($output);
            var_dump($res);
        }
//        $output = shell_exec('/usr/local/bin/ffmpeg -version');
//        echo "<pre>$output</pre>";
        return $this->render('musicCmsBundle:Default:index.html.twig');
    }

    public function usersAction()
    {

        $a = array('username' => 'gombo');
        return $this->render('musicCmsBundle:Default:users.html.twig', $a);
    }

    public function runQueryAction()
    {

        $em = $this->getDoctrine()->getManager();
        $videos = $em->getRepository('musicCmsBundle:Video')->findAll();
        if (!$videos) {
            throw $this->createNotFoundException('Error');
        }

        foreach ($videos as $video) {
           //img.youtube.com/vi/HLphrgQFHUQ/0.jpg


            $video->setImg('http://img.youtube.com/vi/'.$video->videoUrl.'/0.jpg');
            $em->persist($video);
        }
        $em->flush();

        return $this->render('musicCmsBundle:Default:runQuery.html.twig');
    }

    public function loginAction()
    {
        //sdfsdfawfe wfawe
        return $this->render('musicCmsBundle:Default:login.html.twig');
    }

    public function checkFilesAction()
    {
        $checkUtis = new CheckUtil();
        $checkUtis->audioCheck($this->container, $this->getDoctrine()->getManager());
        return $this->render('musicCmsBundle:Default:checkFiles.html.twig');
    }

    public function checkAudioAlbumAction()
    {
        $checkUtis = new CheckUtil();
        $checkUtis->HTMlupdateAudioAlbum($this);
        return $this->render('musicCmsBundle:Default:checkAudioAlbum.html.twig');
    }

    public function checkAudioArtistAction()
    {
        $checkUtis = new CheckUtil();
        $checkUtis->HTMlupdateAudioArtist($this);

        return $this->render('musicCmsBundle:Default:checkAudioArtist.html.twig');
    }

    public function checkAlbumArtistAction()
    {
        $checkUtis = new CheckUtil();
        $checkUtis->HTMlupdateAlbumArtist($this);

        return $this->render('musicCmsBundle:Default:checkAlbumArtist.html.twig');
    }


    public function checkPublishArtistAction()
    {
        $checkUtis = new CheckUtil();
        $checkUtis->publishArtistNoAudio($this);

        return $this->render('musicCmsBundle:Default:publish_artist.html.twig');
    }

    public function checkGenreAudioAction($page)
    {
        $checkUtis = new CheckUtil();
        $checkUtis->showgenreAudio($this, $page);

        return $this->render('musicCmsBundle:Default:genre_audio.html.twig');
    }

    public function checkGenreArtistAction($page)
    {
        $checkUtis = new CheckUtil();
        $checkUtis->showgenreArtist($this, $page);

        return $this->render('musicCmsBundle:Default:genre_artist.html.twig');
    }


}
