<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserFollow
 *
 * @ORM\Table(name="msc_user_follow")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserFollow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="leader_id", referencedColumnName="id")
     */
    public $leader_d;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="follower_id", referencedColumnName="id")
     */
    public $follower_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set leader_d
     *
     * @param \music\CmsBundle\Entity\User $leaderD
     * @return UserFollow
     */
    public function setLeaderD(\music\CmsBundle\Entity\User $leaderD = null)
    {
        $this->leader_d = $leaderD;

        return $this;
    }

    /**
     * Get leader_d
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getLeaderD()
    {
        return $this->leader_d;
    }

    /**
     * Set follower_id
     *
     * @param \music\CmsBundle\Entity\User $followerId
     * @return UserFollow
     */
    public function setFollowerId(\music\CmsBundle\Entity\User $followerId = null)
    {
        $this->follower_id = $followerId;

        return $this;
    }

    /**
     * Get follower_id
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getFollowerId()
    {
        return $this->follower_id;
    }
}
