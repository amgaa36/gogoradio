<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AudioArtist
 *
 * @ORM\Table(name="msc_audio_video")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class AudioVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $audio;

    /**
     * @ORM\Column(name="audio_id", type="integer", nullable=true)
     */
    public $audio_id;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $video;

    /**
     * @ORM\Column(name="video_id", type="integer", nullable=true)
     */
    public $video_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return AudioVideo
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Set video
     *
     * @param \music\CmsBundle\Entity\Video $video
     * @return AudioVideo
     */
    public function setVideo(\music\CmsBundle\Entity\Video $video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \music\CmsBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }
}
