<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArtistVideo
 * @ORM\Table(name="msc_artist_video")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ArtistVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    public $video;

    /**
     * @ORM\ManyToOne(targetEntity="Artist")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    public $artist;

    /**
     * @var integer
     *
     * @ORM\Column(name="artist_order", type="integer",nullable=true)
     */
    public $order;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return ArtistVideo
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set video
     *
     * @param \music\CmsBundle\Entity\Video $video
     * @return ArtistVideo
     */
    public function setVideo(\music\CmsBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \music\CmsBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set artist
     *
     * @param \music\CmsBundle\Entity\Artist $artist
     * @return ArtistVideo
     */
    public function setArtist(\music\CmsBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return \music\CmsBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }
}
