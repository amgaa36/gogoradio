<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserLog
 *
 * @ORM\Table(name="msc_user_log")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 *
 */
class UserLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Audio", inversedBy="audiosub")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id",onDelete="CASCADE", nullable=true)
     */
    public $audio_id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="listen_date", type="datetime")
     */
    public $listen_date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listen_date
     *
     * @param \DateTime $listenDate
     * @return UserLog
     */
    public function setListenDate($listenDate)
    {
        $this->listen_date = $listenDate;

        return $this;
    }

    /**
     * Get listen_date
     *
     * @return \DateTime 
     */
    public function getListenDate()
    {
        return $this->listen_date;
    }

    /**
     * Set user_id
     *
     * @param \music\CmsBundle\Entity\User $userId
     * @return UserLog
     */
    public function setUserId(\music\CmsBundle\Entity\User $userId = null)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set audio_id
     *
     * @param \music\CmsBundle\Entity\Audio $audioId
     * @return UserLog
     */
    public function setAudioId(\music\CmsBundle\Entity\Audio $audioId = null)
    {
        $this->audio_id = $audioId;

        return $this;
    }

    /**
     * Get audio_id
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudioId()
    {
        return $this->audio_id;
    }
}
