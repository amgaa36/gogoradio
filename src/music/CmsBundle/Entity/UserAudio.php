<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAudio
 *
 * @ORM\Table(name="msc_user_audio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserAudio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    public $user;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $audio;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $audio_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserAudio
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set audio_id
     *
     * @param integer $audioId
     * @return UserAudio
     */
    public function setAudioId($audioId)
    {
        $this->audio_id = $audioId;

        return $this;
    }

    /**
     * Get audio_id
     *
     * @return integer 
     */
    public function getAudioId()
    {
        return $this->audio_id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserAudio
     */
    public function setUser(\music\CmsBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return UserAudio
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }
}
