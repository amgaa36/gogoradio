<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * PaymentLog
 *
 * @ORM\Table(name="msc_payment_log")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class PaymentLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="transactionDate", type="datetime", nullable=true)
     */
    private $transactionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="paidValue", type="integer", nullable=true)
     */
    private $paidValue;

    /**
     * @var string
     *
     * @ORM\Column(name="transId", type="integer", nullable=true)
     */
    private $transId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserAudio
     */
    public function setUser(\music\CmsBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set transactionDate
     *
     * @param \DateTime $transactionDate
     * @return PaymentLog
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;

        return $this;
    }

    /**
     * Get transactionDate
     *
     * @return \DateTime 
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return PaymentLog
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return PaymentLog
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set paidValue
     *
     * @param integer $paidValue
     * @return PaymentLog
     */
    public function setPaidValue($paidValue)
    {
        $this->paidValue = $paidValue;

        return $this;
    }

    /**
     * Get paidValue
     *
     * @return integer 
     */
    public function getPaidValue()
    {
        return $this->paidValue;
    }

    /**
     * Set transId
     *
     * @param integer $transId
     * @return PaymentLog
     */
    public function setTransId($transId)
    {
        $this->transId = $transId;

        return $this;
    }

    /**
     * Get transId
     *
     * @return integer
     */
    public function getTransId()
    {
        return $this->transId;
    }
}
