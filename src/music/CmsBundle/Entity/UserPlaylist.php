<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAudio
 *
 * @ORM\Table(name="msc_user_playlist")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserPlaylist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    public $user;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Playlist")
     * @ORM\JoinColumn(name="playlist_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $playlist;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $playlist_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserPlaylist
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set playlist_id
     *
     * @param integer $playlistId
     * @return UserPlaylist
     */
    public function setPlaylistId($playlistId)
    {
        $this->playlist_id = $playlistId;

        return $this;
    }

    /**
     * Get playlist_id
     *
     * @return integer 
     */
    public function getPlaylistId()
    {
        return $this->playlist_id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserPlaylist
     */
    public function setUser(\music\CmsBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set playlist
     *
     * @param \music\CmsBundle\Entity\Playlist $playlist
     * @return UserPlaylist
     */
    public function setPlaylist(\music\CmsBundle\Entity\Playlist $playlist)
    {
        $this->playlist = $playlist;

        return $this;
    }

    /**
     * Get playlist
     *
     * @return \music\CmsBundle\Entity\Playlist 
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }
}
