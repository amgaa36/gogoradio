<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserDislike
 *
 * @ORM\Table(name="msc_user_dislike")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserDislike
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $audio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserDislike
     */
    public function setUser(\music\CmsBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return UserDislike
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio = null)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }
}
