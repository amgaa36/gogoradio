<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserBanner
 *
 * @ORM\Table(name="msc_user_banner")
 * @ORM\Entity
 */
class UserBanner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AudioBanner")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    private $banner;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="listenDate", type="datetime")
     */
    private $listenDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listenDate
     *
     * @param \DateTime $listenDate
     * @return UserBanner
     */
    public function setListenDate($listenDate)
    {
        $this->listenDate = $listenDate;

        return $this;
    }

    /**
     * Get listenDate
     *
     * @return \DateTime 
     */
    public function getListenDate()
    {
        return $this->listenDate;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserBanner
     */
    public function setUser(\music\CmsBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set banner
     *
     * @param \music\CmsBundle\Entity\AudioBanner $banner
     * @return UserBanner
     */
    public function setBanner(\music\CmsBundle\Entity\AudioBanner $banner)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \music\CmsBundle\Entity\AudioBanner
     */
    public function getBanner()
    {
        return $this->banner;
    }
}
