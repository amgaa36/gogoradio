<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserSkip
 *
 * @ORM\Table(name="msc_user_skip")
 * @ORM\Entity
 */
class UserSkip
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Genre")
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    private $genre;


    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    public $audio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="skipDate", type="datetime")
     */
    public $skipDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set skipDate
     *
     * @param \DateTime $skipDate
     * @return UserSkip
     */
    public function setSkipDate($skipDate)
    {
        $this->skipDate = $skipDate;

        return $this;
    }

    /**
     * Get skipDate
     *
     * @return \DateTime 
     */
    public function getSkipDate()
    {
        return $this->skipDate;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserSkip
     */
    public function setUser(\music\CmsBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set genre
     *
     * @param \music\CmsBundle\Entity\Genre $genre
     * @return UserSkip
     */
    public function setGenre(\music\CmsBundle\Entity\Genre $genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return \music\CmsBundle\Entity\Genre
     */
    public function getGenre()
    {
        return $this->genre;
    }


    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return UserSkip
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio
     */
    public function getAudio()
    {
        return $this->audio;
    }
}
