<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PriceSchedule
 *
 * @ORM\Table(name="msc_price_schedule")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class PriceSchedule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="priceTug", type="integer")
     */
    private $priceTug;

    /**
     * @var integer
     *
     * @ORM\Column(name="priceDollar", type="integer")
     */
    private $priceDollar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beginDate", type="datetime")
     */
    private $beginDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime")
     */
    private $endDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set priceTug
     *
     * @param integer $priceTug
     * @return PriceSchedule
     */
    public function setPriceTug($priceTug)
    {
        $this->priceTug = $priceTug;

        return $this;
    }

    /**
     * Get priceTug
     *
     * @return integer 
     */
    public function getPriceTug()
    {
        return $this->priceTug;
    }

    /**
     * Set priceDollar
     *
     * @param integer $priceDollar
     * @return PriceSchedule
     */
    public function setPriceDollar($priceDollar)
    {
        $this->priceDollar = $priceDollar;

        return $this;
    }

    /**
     * Get priceDollar
     *
     * @return integer 
     */
    public function getPriceDollar()
    {
        return $this->priceDollar;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     * @return PriceSchedule
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime 
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return PriceSchedule
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
}
