<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlaylistAudio
 *
 * @ORM\Table(name="msc_playlist_audio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class PlaylistAudio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Playlist")
     * @ORM\JoinColumn(name="playlist_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $playlist;

    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $audio;

    /**
     * @var integer
     *
     * @ORM\Column(name="audio_orders", type="integer", nullable=false)
     */
    public $audio_orders;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set audio_orders
     *
     * @param integer $audioOrders
     * @return PlaylistAudio
     */
    public function setAudioOrders($audioOrders)
    {
        $this->audio_orders = $audioOrders;

        return $this;
    }

    /**
     * Get audio_orders
     *
     * @return integer 
     */
    public function getAudioOrders()
    {
        return $this->audio_orders;
    }

    /**
     * Set playlist
     *
     * @param \music\CmsBundle\Entity\Playlist $playlist
     * @return PlaylistAudio
     */
    public function setPlaylist(\music\CmsBundle\Entity\Playlist $playlist)
    {
        $this->playlist = $playlist;

        return $this;
    }

    /**
     * Get playlist
     *
     * @return \music\CmsBundle\Entity\Playlist 
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return PlaylistAudio
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }
}
