<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUser;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Translation\Tests\String;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * User
 *
 * @ORM\Table(name="msc_user")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class User extends OAuthUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="socialId", type="string", length=255, nullable=false)
     */
    private $socialId;

    /**
     * @var string
     *
     * @ORM\Column(name="loginName", type="string", length=255, nullable=false)
     */
    public $loginName;

    /**
     * @var string
     *
     * @ORM\Column(name="loginType", type="string", length=20, nullable=false)
     */
    public $loginType;

    /**
     * @var string
     *
     * @ORM\Column(name="myroles", type="simple_array", nullable=true)
     */
    public $myroles;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_url", type="string", length=255, nullable=true)
     */
    public $fb_url;

    /**
     * @var string
     *
     * @ORM\Column(name="tw_url", type="string", length=255, nullable=true)
     */
    public $tw_url;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    public $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    public $firstName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="userBirthday", type="date", nullable=true)
     */
    public $userBirthday;

    /**
     * @var string
     *
     * @ORM\Column(name="userEmail", type="string", length=255, nullable=true)
     */
    public $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=3000, nullable=true)
     */
    public $description;

    /**
     * @var string
     *
     *
     *
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $userImageUrl;


    /**
     *
     * @Assert\Image(
     * 		mimeTypesMessage = "Зурган файл биш байна!"
     * )
     *      minWidth = 40,
     *      maxWidth = 40,
     */
    public $imagefile;


    /**
     * @var integer
     *
     * @ORM\Column(name="following", type="integer", nullable=true)
     */
    public $following;

    /**
     * @var integer
     *
     * @ORM\Column(name="followers", type="integer", nullable=true)
     */
    public $followers;

    /**
     * @var integer
     *
     * @ORM\Column(name="notifCount", type="integer", nullable=true)
     */
    public $notifCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="loginDate", type="datetime", nullable=true)
     */
    public $loginDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPremium", type="boolean", nullable=true)
     */
    public $isPremium;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expireDate", type="datetime", nullable=true)
     */
    public $expireDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="notifDate", type="datetime", nullable=true)
     */
    public $notifDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="streakCount", type="integer", options={"default" = 0})
     */
    public $streakCount = 0;


    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     * @return PurchaseLog
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;

        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    public function __construct($username = null) {
        parent::__construct($username);
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set socialId
     *
     * @param integer $socialId
     * @return User
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;

        return $this;
    }

    /**
     * Get socialId
     *
     * @return integer
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * Set loginName
     *
     * @param string $loginName
     * @return User
     */
    public function setLoginName($loginName)
    {
        $this->loginName = $loginName;

        return $this;
    }

    /**
     * Get loginName
     *
     * @return string
     */
    public function getLoginName()
    {
        return $this->loginName;
    }

    /**
     * Set loginType
     *
     * @param string $loginType
     * @return User
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;

        return $this;
    }

    /**
     * Get loginType
     *
     * @return string
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set userBirthday
     *
     * @param \DateTime $userBirthday
     * @return User
     */
    public function setUserBirthday($userBirthday)
    {
        $this->userBirthday = $userBirthday;

        return $this;
    }

    /**
     * Get userBirthday
     *
     * @return \DateTime
     */
    public function getUserBirthday()
    {
        return $this->userBirthday;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return User
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userImageUrl
     *
     * @param string $userImageUrl
     * @return Emotic
     */
    public function setUserImageUrl($userImageUrl)
    {
        $this->userImageUrl = $userImageUrl;
        return $this;
    }

    /**
     * Get userImageUrl
     *
     * @return string 
     */
    public function getUserImageUrl()
    {
        return $this->userImageUrl;
    }

    /**
     * Set Image
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imagefile = $file;
    }
    /**
     * Get Image
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imagefile;
    }




    /**
     *
     * @param $container
     */
    public function uploadImage(Container $container) {
        if (null === $this->getImagefile()) {
            return;
        }

        $statfolder = $container->getParameter('statfolder');
        $staturl = $container->getParameter('staturl');

        $dir = 'user';

        $filename = $this->id.'-'.date('dmY-U').'-'.rand(). '-'.$this->getImageFile()->getFilename() .'.'.$this->getImagefile()->guessExtension();
        $this->getImagefile()->move(
            $statfolder . '/' . $dir, $filename
        );

        $path = $staturl . '/' . $dir . "/" . $filename;

        $this->userImageUrl = $path;

        $this->imagefile = null;
    }

    /**
     * Set following
     *
     * @param integer $following
     * @return User
     */
    public function setFollowing($following)
    {
        $this->following = $following;

        return $this;
    }

    /**
     * Get following
     *
     * @return integer 
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * Set followers
     *
     * @param integer $followers
     * @return User
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;

        return $this;
    }

    /**
     * Get followers
     *
     * @return integer 
     */
    public function getFollowers()
    {
        return $this->followers;
    }


    /**
     * Set notifCount
     *
     * @param integer $notifCount
     * @return User
     */
    public function setNotifCount($notifCount)
    {
        $this->notifCount = $notifCount;

        return $this;
    }

    /**
     * Get notifCount
     *
     * @return integer
     */
    public function getNotifCount()
    {
        return $this->notifCount;
    }


    /**
     * Set notifDate
     *
     * @param \DateTime $notifDate
     * @return User
     */
    public function setNotifDate($notifDate)
    {
        $this->notifDate = $notifDate;

        return $this;
    }

    /**
     * Get notifDate
     *
     * @return \DateTime
     */
    public function getNotifDate()
    {
        return $this->notifDate;
    }



    /**
     * Set loginDate
     *
     * @param \DateTime $loginDate
     * @return User
     */
    public function setLoginDate($loginDate)
    {
        $this->loginDate = $loginDate;

        return $this;
    }

    /**
     * Get loginDate
     *
     * @return \DateTime 
     */
    public function getLoginDate()
    {
        return $this->loginDate;
    }

    /**
     * Set isPremium
     *
     * @param boolean $isPremium
     * @return User
     */
    public function setIsPremium($isPremium)
    {
        $this->isPremium = $isPremium;

        return $this;
    }

    /**
     * Get isPremium
     *
     * @return boolean 
     */
    public function getIsPremium()
    {
        return $this->isPremium;
    }

    public function getRoles()
    {
        $roles = array('ROLE_USER');
        if($this->myroles)
        {
//            $myroles = explode(' ',$this->myroles);
            foreach($this->myroles as $r)
            {
                $roles[] = trim($r);
            }
        }

        return $roles; //$this->myroles;


    }

//    public function getSalt()
//    {
//        return null;
//    }
//
//    public function eraseCredentials()
//    {
//        return true;
//    }
    public function getUsername()
    {
        return $this->socialId;
    }
//    public function getPassword()
//    {
//        return null;
//    }

//    /**
//     * @see \Serializable::serialize()
//     */
//    public function serialize()
//    {
//        return serialize(
//            array(
//                $this->id,
//            )
//        );
//    }
//
//    /**
//     * @see \Serializable::unserialize()
//     */
//    public function unserialize($serialized)
//    {
//        list (
//            $this->id,
//            ) = unserialize($serialized);
//    }

    /**
     * Set myroles
     *
     * @param array $myroles
     * @return User
     */
    public function setMyroles($myroles)
    {
        $this->myroles = $myroles;

        return $this;
    }

    /**
     * Get myroles
     *
     * @return array 
     */
    public function getMyroles()
    {
        return $this->myroles;
    }

    /**
     * Set fb_url
     *
     * @param string $fb_url
     * @return User
     */
    public function setFb_url($fb_url)
    {
        $this->fb_url = $fb_url;

        return $this;
    }

    /**
     * Get fb_url
     *
     * @return string
     */
    public function getFb_url()
    {
        return $this->fb_url;
    }

    /**
     * Set tw_url
     *
     * @param string $tw_url
     * @return User
     */
    public function setTw_url($tw_url)
    {
        $this->tw_url = $tw_url;

        return $this;
    }

    /**
     * Get tw_url
     *
     * @return string
     */
    public function getTw_url()
    {
        return $this->tw_url;
    }

    public function __toString()
    {
        return strval( $this->getId() );
    }



    /**
     * Set streakCount
     *
     * @param integer $streakCount
     * @return User
     */
    public function setStreakCount($streakCount)
    {
        $this->streakCount = $streakCount;

        return $this;
    }

    /**
     * Get streakCount
     *
     * @return integer
     */
    public function getStreakCount()
    {
        return $this->streakCount;
    }
}
