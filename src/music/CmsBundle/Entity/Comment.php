<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="msc_comment")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=250, nullable=false)
     */
    public $path;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    public $user;

    /**
     * @var string
     *
     * @ORM\Column(name="posted_by", type="string", length=100, nullable=false)
     */
    public $posted_by;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    public $body;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=40, nullable=false)
     */
    public $ip_address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="posted_date", type="datetime", nullable=false)
     */
    public $posted_date;

    /**
     * @var integer
     *
     * @ORM\Column(name="like_count", type="integer", nullable=false)
     */
    public $like_count;

    /**
     * @var integer
     *
     * @ORM\Column(name="dislike_count", type="integer", nullable=false)
     */
    public $dislike_count;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_show", type="boolean", nullable=true)
     */
    public $is_show;

    /**
     * @var integer
     *
     * @ORM\Column(name="content_type", type="integer", nullable=true)
     */
    public $content_type;

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if(is_null($this->like_count)) {
            $this->like_count = 0;
        }
        if(is_null($this->dislike_count)) {
            $this->dislike_count = 0;
        }
        if(is_null($this->is_show)) {
            $this->is_show = true;
        }
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set posted_by
     *
     * @param string $postedBy
     * @return Comment
     */
    public function setPostedBy($postedBy)
    {
        $this->posted_by = $postedBy;

        return $this;
    }

    /**
     * Get posted_by
     *
     * @return string 
     */
    public function getPostedBy()
    {
        return $this->posted_by;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Comment
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set ip_address
     *
     * @param string $ipAddress
     * @return Comment
     */
    public function setIpAddress($ipAddress)
    {
        $this->ip_address = $ipAddress;

        return $this;
    }

    /**
     * Get ip_address
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * Set posted_date
     *
     * @param \DateTime $postedDate
     * @return Comment
     */
    public function setPostedDate($postedDate)
    {
        $this->posted_date = $postedDate;

        return $this;
    }

    /**
     * Get posted_date
     *
     * @return \DateTime 
     */
    public function getPostedDate()
    {
        return $this->posted_date;
    }

    /**
     * Set like_count
     *
     * @param integer $likeCount
     * @return Comment
     */
    public function setLikeCount($likeCount)
    {
        $this->like_count = $likeCount;

        return $this;
    }

    /**
     * Get like_count
     *
     * @return integer 
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }

    /**
     * Set dislike_count
     *
     * @param integer $dislikeCount
     * @return Comment
     */
    public function setDislikeCount($dislikeCount)
    {
        $this->dislike_count = $dislikeCount;

        return $this;
    }

    /**
     * Get dislike_count
     *
     * @return integer 
     */
    public function getDislikeCount()
    {
        return $this->dislike_count;
    }

    /**
     * Set is_show
     *
     * @param boolean $isShow
     * @return Comment
     */
    public function setIsShow($isShow)
    {
        $this->is_show = $isShow;

        return $this;
    }

    /**
     * Get is_show
     *
     * @return boolean 
     */
    public function getIsShow()
    {
        return $this->is_show;
    }

    /**
     * Set content_type
     *
     * @param integer $contentType
     * @return Comment
     */
    public function setContentType($contentType)
    {
        $this->content_type = $contentType;

        return $this;
    }

    /**
     * Get content_type
     *
     * @return integer 
     */
    public function getContentType()
    {
        return $this->content_type;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Comment
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return Comment
     */
    public function setUser(\music\CmsBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
