<?php

namespace music\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\Container;

/**
 * Album
 *
 * @ORM\Table(name="msc_album")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Album
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="album_name", type="string", length=255, nullable=false)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="album_img", type="string", length=255, nullable=true)
     */
    public $img;

    /**
     *
     * @Assert\Image(
     *        mimeTypesMessage = "Зурган файл биш байна!",
     *      minWidth = 400,
     *      minHeight = 400
     * )
     */

    public $imagefile;


    /**
     * @var boolean
     *
     * @ORM\Column(name="img_is_big", type="boolean", nullable=false)
     */
    public $img_is_big;

    /**
     * @var integer
     *
     * @ORM\Column(name="track_number", type="integer", nullable=true)
     */
    public $track_number;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    public $duration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="released_date", type="datetime", nullable=true)
     */
    public $released_date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mongolian", type="boolean", nullable=true)
     */
    public $is_mongolian;

    /**
     * @var integer
     *
     * @ORM\Column(name="like_count", type="integer", nullable=true)
     */
    public $like_count;

    /**
     * @ORM\ManyToOne(targetEntity="Studio")
     * @ORM\JoinColumn(name="studio_id", referencedColumnName="id", nullable=true)
     */
    public $studio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    public $created_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    public $updated_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="datetime", nullable=true)
     */
    public $publish_date;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name", type="string", length=255, nullable=true)
     */
    public $artist_name;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name_html", type="string", length=300, nullable=true)
     */
    public $artist_name_html;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_img", type="string", length=255, nullable=true)
     */
    public $artist_img;

    /**
     * @ORM\OneToMany(targetEntity="AlbumArtist", mappedBy="album", cascade={"persist", "remove"})
     * @ORM\OrderBy({"order" = "ASC"})
     **/
    public $artistalbums;

    /**
     * @ORM\OneToMany(targetEntity="AlbumAudio", mappedBy="album", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"order" = "ASC"})
     **/
    public $audioalbums;


    public $artistids;

    /**
     */
    public $audioalbums_file;

    public $ispublish;

    /**
     * @ORM\PreUpdate
     */
    public function createDate()
    {
        $this->created_date = new \DateTime("now");
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->updated_date = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param $container
     */
    public function uploadImage(Container $container)
    {
        if (null === $this->imagefile) {
            return;
        }

        $statfolder = $container->getParameter('imgstatfolder');
        $staturl = $container->getParameter('imgstaturl');

        $dir = '/new_album_img/' . ($this->id % 100);

        $filename = $this->id . '-' . date('dmYU') . '_' . rand() . '.' . $this->imagefile->guessExtension();
        $filepath = $statfolder . $dir . '/' . $filename;
        $this->imagefile->move(
            $statfolder . $dir, $filename
        );

        $path = $dir . "/" . $filename;

        $this->img = $path;
        $this->img_is_big = true;

        $this->imagefile = null;

        //make thumbs
        $imageGod = $container->get('imagegod');
        $size = $imageGod->getWidthHeight($filepath);
        if ($size[0] > 0 && $size[1] > 0) {
            $makethumbs = $imageGod->makeThumbs($filepath);
            if (!$makethumbs) {
                return 'cant make image thumbs';
            }
        } else {
            return 'cant find image size ' . $size[0] . 'x' . $size[1];
        }
        return 'no';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->artistalbums = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Album
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set img_is_big
     *
     * @param boolean $imgIsBig
     * @return Album
     */
    public function setImgIsBig($imgIsBig)
    {
        $this->img_is_big = $imgIsBig;

        return $this;
    }

    /**
     * Get img_is_big
     *
     * @return boolean
     */
    public function getImgIsBig()
    {
        return $this->img_is_big;
    }

    /**
     * Set track_number
     *
     * @param integer $trackNumber
     * @return Album
     */
    public function setTrackNumber($trackNumber)
    {
        $this->track_number = $trackNumber;

        return $this;
    }

    /**
     * Get track_number
     *
     * @return integer
     */
    public function getTrackNumber()
    {
        return $this->track_number;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Album
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set released_date
     *
     * @param \DateTime $releasedDate
     * @return Album
     */
    public function setReleasedDate($releasedDate)
    {
        $this->released_date = $releasedDate;

        return $this;
    }

    /**
     * Get released_date
     *
     * @return \DateTime
     */
    public function getReleasedDate()
    {
        return $this->released_date;
    }

    /**
     * Set is_mongolian
     *
     * @param boolean $isMongolian
     * @return Album
     */
    public function setIsMongolian($isMongolian)
    {
        $this->is_mongolian = $isMongolian;

        return $this;
    }

    /**
     * Get is_mongolian
     *
     * @return boolean
     */
    public function getIsMongolian()
    {
        return $this->is_mongolian;
    }

    /**
     * Set like_count
     *
     * @param integer $likeCount
     * @return Album
     */
    public function setLikeCount($likeCount)
    {
        $this->like_count = $likeCount;

        return $this;
    }

    /**
     * Get like_count
     *
     * @return integer
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Album
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set updated_date
     *
     * @param \DateTime $updatedDate
     * @return Album
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updated_date = $updatedDate;

        return $this;
    }

    /**
     * Get updated_date
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * Set publish_date
     *
     * @param \DateTime $publishDate
     * @return Album
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;

        return $this;
    }

    /**
     * Get publish_date
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * Set artist_name
     *
     * @param string $artistName
     * @return Album
     */
    public function setArtistName($artistName)
    {
        $this->artist_name = $artistName;

        return $this;
    }

    /**
     * Get artist_name
     *
     * @return string
     */
    public function getArtistName()
    {
        return $this->artist_name;
    }

    /**
     * Set artist_name_html
     *
     * @param string $artistNameHtml
     * @return Album
     */
    public function setArtistNameHtml($artistNameHtml)
    {
        $this->artist_name_html = $artistNameHtml;

        return $this;
    }

    /**
     * Get artist_name_html
     *
     * @return string
     */
    public function getArtistNameHtml()
    {
        return $this->artist_name_html;
    }

    /**
     * Set artist_img
     *
     * @param string $artistImg
     * @return Album
     */
    public function setArtistImg($artistImg)
    {
        $this->artist_img = $artistImg;

        return $this;
    }

    /**
     * Get artist_img
     *
     * @return string
     */
    public function getArtistImg()
    {
        return $this->artist_img;
    }

    /**
     * Set studio
     *
     * @param \music\CmsBundle\Entity\Studio $studio
     * @return Album
     */
    public function setStudio(\music\CmsBundle\Entity\Studio $studio = null)
    {
        $this->studio = $studio;

        return $this;
    }

    /**
     * Get studio
     *
     * @return \music\CmsBundle\Entity\Studio
     */
    public function getStudio()
    {
        return $this->studio;
    }

    /**
     * Add artistalbums
     *
     * @param \music\CmsBundle\Entity\AlbumArtist $artistalbums
     * @return Album
     */
    public function addArtistalbum(\music\CmsBundle\Entity\AlbumArtist $artistalbums)
    {
        $artistalbums->album = $this;
        $this->artistalbums[] = $artistalbums;

        return $this;
    }

    /**
     * Remove artistalbums
     *
     * @param \music\CmsBundle\Entity\AlbumArtist $artistalbums
     */
    public function removeArtistalbum(\music\CmsBundle\Entity\AlbumArtist $artistalbums)
    {
        $this->artistalbums->removeElement($artistalbums);
    }

    /**
     * Get artistalbums
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArtistalbums()
    {
        return $this->artistalbums;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist()
    {
        if (is_null($this->img_is_big)) {
            $this->img_is_big = false;
        }
    }

    /**
     * Add audioalbums
     *
     * @param \music\CmsBundle\Entity\AlbumAudio $audioalbums
     * @return Album
     */
    public function addAudioalbum(\music\CmsBundle\Entity\AlbumAudio $audioalbums)
    {
        $audioalbums->album = $this;
        $this->audioalbums[] = $audioalbums;

        return $this;
    }

    /**
     * Remove audioalbums
     *
     * @param \music\CmsBundle\Entity\AlbumAudio $audioalbums
     */
    public function removeAudioalbum(\music\CmsBundle\Entity\AlbumAudio $audioalbums)
    {
        $audioalbums->album = null;
        $this->audioalbums->removeElement($audioalbums);
    }

    /**
     * Get audioalbums
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudioalbums()
    {
        return $this->audioalbums;
    }


    public function getAudioTrackNumber()
    {
        return $this->track_number = 10;
    }


    /**
     * Set file_url
     *
     * @param string $fileUrl
     * @return Album
     */
    public function setFileUrl($fileUrl)
    {
        $this->file_url = $fileUrl;

        return $this;
    }

    /**
     * Get file_url
     *
     * @return string
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }


    function reArrayFiles(Container $container)
    {

        if (null === $this->getFileUrl()) {
            return;
        }

        $file_ary = array();
        $file_count = count($container['name']);
        $file_keys = array_keys($container);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $container[$key][$i];
            }
        }

        return $file_ary;
    }


}
