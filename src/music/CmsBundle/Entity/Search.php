<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Search
 *
 * @ORM\Table(name="msc_search")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Search
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="search_key_word", type="string", length=500)
     */
    public $search_key_word;

    /**
     * @var integer
     *
     * @ORM\Column(name="content_type", type="integer", nullable=false)
     */
    public $content_type;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set search_key_word
     *
     * @param string $searchKeyWord
     * @return Search
     */
    public function setSearchKeyWord($searchKeyWord)
    {
        $this->search_key_word = $searchKeyWord;

        return $this;
    }

    /**
     * Get search_key_word
     *
     * @return string 
     */
    public function getSearchKeyWord()
    {
        return $this->search_key_word;
    }

    /**
     * Set content_type
     *
     * @param integer $contentType
     * @return Search
     */
    public function setContentType($contentType)
    {
        $this->content_type = $contentType;

        return $this;
    }

    /**
     * Get content_type
     *
     * @return integer 
     */
    public function getContentType()
    {
        return $this->content_type;
    }
}
