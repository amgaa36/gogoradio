<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video
 *
 * @ORM\Table(name="msc_video")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Video
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="video_name", type="string", length=255,nullable = true)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="video_descr", type="text", nullable=true)
     */
    public $descr;

    /**
     * @var string
     *
     * @ORM\Column(name="video_img", type="string", length=255,nullable = true)
     */
    public $img;

    /**
     * @var string
     *
     * @ORM\Column(name="artist", type="string", length=255,nullable = true)
     */
    public $artist;

    /**
     * @var string
     *
     * @ORM\Column(name="videoUrl", type="string", length=500)
     */
    public $videoUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="likeCount", type="integer", nullable=true)
     */
    public $like_count;

    /**
     * @var integer
     *
     * @ORM\Column(name="view", type="integer",nullable=true)
     */
    public $view;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime" ,nullable = true)
     */
    public $created_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime" ,nullable = true)
     */
    public $updated_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="datetime", nullable = true)
     */
    public $publish_date;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name", type="string", length=255, nullable=true)
     */
    public $artist_name;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name_html", type="text", nullable=true)
     */
    public $artist_name_html;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_img", type="string", length=255, nullable=true)
     */
    public $artist_img;

    /**
     * @ORM\OneToMany(targetEntity="ArtistVideo", mappedBy="video", cascade={"persist", "remove"}, orphanRemoval=true)
     **/
    public $videoartists;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function createdDate()
    {
        $this->created_date = new \DateTime("now");
        if(is_null($this->view)) {
            $this->view = 0;
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function updatedDate()
    {
        $this->updated_date= new \DateTime("now");
    }

    /**
     * @ORM\PrePersist
     */
    public function LikeCount()
    {
        $this->like_count = 0;
    }

    public function getVideoThumb()
    {
        $this->img = 'http://img.youtube.com/vi/'.$this->videoUrl.'/0.jpg';

    }

    /**
     * Set name
     *
     * @param string $name
     * @return Video
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Video
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string 
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set videoUrl
     *
     * @param string $videoUrl
     * @return Video
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * Get videoUrl
     *
     * @return string 
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * Set like_count
     *
     * @param integer $likeCount
     * @return Video
     */
    public function setLikeCount($likeCount)
    {
        $this->like_count = $likeCount;

        return $this;
    }

    /**
     * Get like_count
     *
     * @return integer 
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Video
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set updated_date
     *
     * @param \DateTime $updatedDate
     * @return Video
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updated_date = $updatedDate;

        return $this;
    }

    /**
     * Get updated_date
     *
     * @return \DateTime 
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * Set publish_date
     *
     * @param \DateTime $publishDate
     * @return Video
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;

        return $this;
    }

    /**
     * Get publish_date
     *
     * @return \DateTime 
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    public static function getYoutubeID($url)
    {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        if(array_key_exists('v', $my_array_of_vars))
            return $my_array_of_vars['v'];
        return null;
    }

    /**
     * Set artist
     *
     * @param string $artist
     * @return Video
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return string 
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Set view
     *
     * @param integer $view
     * @return Video
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return integer 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return Video
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set artist_name
     *
     * @param string $artistName
     * @return Video
     */
    public function setArtistName($artistName)
    {
        $this->artist_name = $artistName;

        return $this;
    }

    /**
     * Get artist_name
     *
     * @return string 
     */
    public function getArtistName()
    {
        return $this->artist_name;
    }

    /**
     * Set artist_name_html
     *
     * @param string $artistNameHtml
     * @return Video
     */
    public function setArtistNameHtml($artistNameHtml)
    {
        $this->artist_name_html = $artistNameHtml;

        return $this;
    }

    /**
     * Get artist_name_html
     *
     * @return string 
     */
    public function getArtistNameHtml()
    {
        return $this->artist_name_html;
    }

    /**
     * Set artist_img
     *
     * @param string $artistImg
     * @return Video
     */
    public function setArtistImg($artistImg)
    {
        $this->artist_img = $artistImg;

        return $this;
    }

    /**
     * Get artist_img
     *
     * @return string 
     */
    public function getArtistImg()
    {
        return $this->artist_img;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->videoartists = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add videoartists
     *
     * @param \music\CmsBundle\Entity\ArtistVideo $videoartists
     * @return Video
     */
    public function addVideoartist(\music\CmsBundle\Entity\ArtistVideo $videoartists)
    {
        $this->videoartists[] = $videoartists;

        return $this;
    }

    /**
     * Remove videoartists
     *
     * @param \music\CmsBundle\Entity\ArtistVideo $videoartists
     */
    public function removeVideoartist(\music\CmsBundle\Entity\ArtistVideo $videoartists)
    {
        $this->videoartists->removeElement($videoartists);
    }

    /**
     * Get videoartists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideoartists()
    {
        return $this->videoartists;
    }
}
