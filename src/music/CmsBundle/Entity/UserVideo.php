<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserVideo
 *
 * @ORM\Table(name="msc_user_video")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $video;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $video_id;

    /**
     * @var bool
     *
     * @ORM\Column(name="liked", type="boolean", nullable=true)
     */
    public $liked;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserVideo
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set video_id
     *
     * @param integer $videoId
     * @return UserVideo
     */
    public function setVideoId($videoId)
    {
        $this->video_id = $videoId;

        return $this;
    }

    /**
     * Get video_id
     *
     * @return integer 
     */
    public function getVideoId()
    {
        return $this->video_id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserVideo
     */
    public function setUser(\music\CmsBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set video
     *
     * @param \music\CmsBundle\Entity\Video $video
     * @return UserVideo
     */
    public function setVideo(\music\CmsBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \music\CmsBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set liked
     *
     * @param boolean $liked
     * @return UserVideo
     */
    public function setLiked($liked)
    {
        $this->liked = $liked;

        return $this;
    }

    /**
     * Get liked
     *
     * @return boolean 
     */
    public function getLiked()
    {
        return $this->liked;
    }
}
