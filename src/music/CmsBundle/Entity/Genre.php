<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Radio
 *
 * @ORM\Table(name="msc_genre")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Genre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="genre_name", type="string", length=255)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="genre_name_en", type="string", length=255, nullable=true)
     */
    public $name_en;

    /**
     * @var string
     *
     * @ORM\Column(name="genre_description", type="string", length=1000, nullable=true)
     */
    public $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="track_number", type="integer")
     */
    private $track_number;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=255, nullable=true)
     */
    public $img;

    /**
     * @var string
     *
     * @ORM\Column(name="background_img", type="string", length=255, nullable=true)
     */
    public $backgroundImg;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    public $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="$thumb_android", type="string", length=255, nullable=true)
     */
    public $thumb_android;

    /**
     * @var UploadedFile
     * @Assert\Image()
     */
    public $img_file;

    /**
     * @var UploadedFile
     * @Assert\Image()
     */
    public $backgroundImg_file;

    /**
     * @var UploadedFile
     * @Assert\Image()
     */
    public $icon_file;

    /**
     * @var UploadedFile
     * @Assert\Image()
     */
    public $thumb_file;

    /**
     * @var integer
     *
     * @ORM\Column(name="like_count", type="integer")
     */
    public $like_count;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private $created_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updated_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="datetime", nullable=true)
     */
    private $publish_date;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_show", type="boolean", nullable=true)
     */
    private $show;

    /**
     * @ORM\OneToMany(targetEntity="GenreAudio", mappedBy="genre", cascade={"persist"})
     **/
    public $genre_audios;

    /**
     * @var integer
     *
     * @ORM\Column(name="listen_count", type="integer", nullable=true)
     */
    public $listen_count;


    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new \DateTime('now');
        if (is_null($this->like_count)) {
            $this->like_count = 0;
        }
        if (is_null($this->track_number)) {
            $this->track_number = 0;
        }
        if (is_null($this->duration)) {
            $this->duration = 0;
        }
    }
    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new \DateTime('now');
    }

    public function uploadImage(Container $container)
    {
        if (null === $this->img_file) {
            return;
        }
        $statfolder = $container->getParameter('imgstatfolder');

        $dir = 'genre';

        $filename = $this->id . '-' . date('dmY-U') . '-' . rand() . '.' . $this->img_file->guessExtension();
        $filepath = $statfolder . '/' . $dir . '/' . $filename;
        $this->img_file->move(
            $statfolder . '/' . $dir, $filename
        );

        $path = '/' . $dir . "/" . $filename;

        $this->img = $path;

        $this->img_file = null;

        //make thumbs
        $imageGod = $container->get('imagegod');
        $size = $imageGod->getWidthHeight($filepath);
        if ($size[0] > 0 && $size[1] > 0) {
            $makethumbs = $imageGod->makeThumbs($filepath);
            if (!$makethumbs) {
                return 'cant make image thumbs';
            }
        }
        else {
            return 'cant find image size ' . $size[0] . 'x' . $size[1];
        }

        return 'no';
    }


    public function uploadBackgroundImage(Container $container)
    {
        if (null === $this->backgroundImg_file) {
            return;
        }
        $statfolder = $container->getParameter('imgstatfolder');

        $dir = 'genre';

        $filename = $this->id . '-' . date('dmY-U') . '-' . rand() . '.' . $this->backgroundImg_file->guessExtension();
        $filepath = $statfolder . '/' . $dir . '/' . $filename;
        $this->backgroundImg_file->move(
            $statfolder . '/' . $dir, $filename
        );

        $path = '/' . $dir . "/" . $filename;

        $this->backgroundImg = $path;

        $this->backgroundImg_file = null;


    }

    public function uploadIconImage(Container $container)
    {
        if (null === $this->icon_file) {
            return;
        }
        $statfolder = $container->getParameter('imgstatfolder');

        $dir = 'genre';

        $filename = $this->id . '-' . date('dmY-U') . '-' . rand() . '.' . $this->icon_file->guessExtension();
        $filepath = $statfolder . '/' . $dir . '/' . $filename;
        $this->icon_file->move(
            $statfolder . '/' . $dir, $filename
        );

        $path = '/' . $dir . "/" . $filename;

        $this->icon = $path;

        $this->icon_file = null;


    }

    public function uploadThumbImage(Container $container)
    {
        if (null === $this->thumb_file) {
            return;
        }
        $statfolder = $container->getParameter('imgstatfolder');

        $dir = 'genre';

        $filename = $this->id . '-' . date('dmY-U') . '-' . rand() . '.' . $this->thumb_file->guessExtension();
        $filepath = $statfolder . '/' . $dir . '/' . $filename;
        $this->thumb_file->move(
            $statfolder . '/' . $dir, $filename
        );

        $path = '/' . $dir . "/" . $filename;

        $this->thumb_android = $path;

        $this->thumb_file = null;


    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Genre
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Genre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Genre
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set track_number
     *
     * @param integer $trackNumber
     * @return Genre
     */
    public function setTrackNumber($trackNumber)
    {
        $this->track_number = $trackNumber;

        return $this;
    }

    /**
     * Get track_number
     *
     * @return integer
     */
    public function getTrackNumber()
    {
        return $this->track_number;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Genre
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set backgroundImg
     *
     * @param string $backgroundImg
     * @return Genre
     */
    public function setBackgroundImg($backgroundImg)
    {
        $this->backgroundImg = $backgroundImg;

        return $this;
    }

    /**
     * Get backgroundImg
     *
     * @return string
     */
    public function getBackgroundImg()
    {
        return $this->backgroundImg;
    }


    /**
     * Set icon
     *
     * @param string $icon
     * @return Genre
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set like_count
     *
     * @param integer $likeCount
     * @return Genre
     */
    public function setLikeCount($likeCount)
    {
        $this->like_count = $likeCount;

        return $this;
    }

    /**
     * Get like_count
     *
     * @return integer
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Genre
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set updated_date
     *
     * @param \DateTime $updatedDate
     * @return Genre
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updated_date = $updatedDate;

        return $this;
    }

    /**
     * Get updated_date
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * Set is_show
     *
     * @param boolean $show
     * @return Genre
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return boolean
     */
    public function getShow()
    {
        return $this->show;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->genre_audios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set publish_date
     *
     * @param \DateTime $publishDate
     * @return Genre
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;

        return $this;
    }

    /**
     * Get publish_date
     *
     * @return \DateTime 
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * Set name_en
     *
     * @param string $nameEn
     * @return Genre
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get name_en
     *
     * @return string 
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Add genre_audios
     *
     * @param \music\CmsBundle\Entity\GenreAudio $genreAudios
     * @return Genre
     */
    public function addGenreAudio(\music\CmsBundle\Entity\GenreAudio $genreAudios)
    {
        $this->genre_audios[] = $genreAudios;

        return $this;
    }

    /**
     * Remove genre_audios
     *
     * @param \music\CmsBundle\Entity\GenreAudio $genreAudios
     */
    public function removeGenreAudio(\music\CmsBundle\Entity\GenreAudio $genreAudios)
    {
        $this->genre_audios->removeElement($genreAudios);
    }

    /**
     * Get genre_audios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGenreAudios()
    {
        return $this->genre_audios;
    }

    /**
     * Set listen_count
     *
     * @param integer $listenCount
     * @return Audio
     */
    public function setListenCount($listenCount)
    {
        $this->listen_count = $listenCount;

        return $this;
    }

    /**
     * Get listen_count
     *
     * @return integer
     */
    public function getListenCount()
    {
        return $this->listen_count;
    }





    /**
     * Set thumb_android
     *
     * @param string $thumbAndroid
     * @return Genre
     */
    public function setThumbAndroid($thumbAndroid)
    {
        $this->thumb_android = $thumbAndroid;

        return $this;
    }

    /**
     * Get thumb_android
     *
     * @return string 
     */
    public function getThumbAndroid()
    {
        return $this->thumb_android;
    }


    /**
     * Get img_file
     *
     * @return UploadedFile
     */
    public function getImgFile(){
        return $this->img_file;
    }

    /**
     * Get icon_file
     *
     * @return UploadedFile
     */
    public function getIconFile(){
        return $this->icon_file;
    }

    /**
     * Get backgroundImg_file
     *
     * @return UploadedFile
     */
    public function getBackgroundImgFile(){
        return $this->backgroundImg_file;
    }

    /**
     * Get thumb_file
     *
     * @return UploadedFile
     */
    public function getThumbFile(){
        return $this->thumb_file;
    }
}
