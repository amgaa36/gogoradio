<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUser;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Translation\Tests\String;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use music\CmsBundle\util\AudioDurationUtil;

/**
 * Artist
 *
 * @ORM\Table(name="msc_artist")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Artist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name", type="string", length=255, nullable=false)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_img", type="string", length=255, nullable=true)
     */
    public $img;


    /**
     * @var string
     *
     * @ORM\Column(name="artist_cover", type="string", length=255, nullable=true)
     */
    public $cover;


    /**
     *
     * @Assert\Image(
     * 		mimeTypesMessage = "Зурган файл биш байна!",
     *      minWidth = 400,
     *      minHeight = 400
     * )
     */

    private $imagefile;

    /**
     *
     * @Assert\Image(
     * 		mimeTypesMessage = "Зурган файл биш байна!",
     *
     *
     * )
     */

    private $coverfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="img_is_big", type="boolean", nullable=true)
     */
    public $img_is_big;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    public $description;

    /**
     * @var string
     *
     * @ORM\Column(name="career", type="string", length=1000, nullable=true)
     */
    public $career;

    /**
     * @var string
     *
     * @ORM\Column(name="web_url", type="string", length=255, nullable=true)
     */
    public $web_url;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_url", type="string", length=255, nullable=true)
     */
    public $fb_url;

    /**
     * @var string
     *
     * @ORM\Column(name="tw_url", type="string", length=255, nullable=true)
     */
    public $tw_url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_band", type="boolean", nullable=true)
     */
    public $is_band;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    public $birthday;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     */
    public $height;

    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=100, nullable=true)
     */
    public $nationality;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mongolian", type="boolean", nullable=false)
     */
    public $is_mongolian;

    /**
     * @var integer
     *
     * @ORM\Column(name="like_count", type="integer", nullable=true)
     */
    public $like_count;

    /**
     * @ORM\OneToMany(targetEntity="AudioArtist", mappedBy="artist", cascade={"persist", "remove"})
     **/
    public $audarts;

    /**
     * @ORM\OneToMany(targetEntity="ArtistVideo", mappedBy="artist", cascade={"persist", "remove"})
     **/
    public $vidarts;

    /**
     * @ORM\OneToMany(targetEntity="AlbumArtist", mappedBy="artist", cascade={"persist", "remove"})
     **/
    public $albumarts;

    /**
     * @var \DateTime
     *
         * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    public $created_date;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_date", type="datetime", nullable=true)
     */
    public $published_date;


    /**
     * @ORM\OneToMany(targetEntity="AudioArtist", mappedBy="artist", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"order" = "ASC"})
     **/
    public $artistaudio;


    /**
     * @ORM\OneToMany(targetEntity="ArtistVideo", mappedBy="artist", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"order" = "ASC"})
     **/
    public $artistvideo;

    public $ispublish;

    public $isgenre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set Name
     *
     * @param string $name
     * @return Artist
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Img
     *
     * @param string $img
     * @return Artist
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }



    /**
     * Set Image
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imagefile = $file;
    }
    /**
     * Get Image
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imagefile;
    }




    /**
     * Set Image
     *
     * @param UploadedFile $file
     */
    public function setCoverFile(UploadedFile $file = null)
    {
        $this->coverfile = $file;
    }
    /**
     * Get Image
     *
     * @return UploadedFile
     */
    public function getCoverFile()
    {
        return $this->coverfile;
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return Artist
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set Career
     *
     * @param string $career
     * @return Artist
     */
    public function setCareer($career)
    {
        $this->career = $career;

        return $this;
    }

    /**
     * Get Career
     *
     * @return string
     */
    public function getCareer()
    {
        return $this->career;
    }

    /**
     * Set Is_band
     *
     * @param boolean $is_band
     * @return Artist
     */
    public function setIs_band($is_band)
    {
        $this->page_url = $is_band;

        return $this;
    }

    /**
     * Get Is_band
     *
     * @return boolean
     */
    public function getIs_band()
    {
        return $this->is_band;
    }

    /**
     * Set Birthday
     *
     * @param /Datetime $birthday
     * @return Artist
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get Birthday
     *
     * @return \Datetime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }


    /**
     * Set Height
     *
     * @param integer $height
     * @return Artist
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get Height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Set Nationality
     *
     * @param string $nationality
     * @return Artist
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get Nationality
     *
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set Is_mongolian
     *
     * @param boolean $is_mongolian
     * @return Artist
     */
    public function setIs_mongolian($is_mongolian)
    {
        $this->is_mongolian = $is_mongolian;

        return $this;
    }

    /**
     * Get Is_mongolian
     *
     * @return boolean
     */
    public function getIs_mongolian()
    {
        return $this->is_mongolian;
    }


    /**
     *
     * @param $container
     */
    public function uploadImage(Container $container) {
        if (null === $this->getImagefile()) {
            return;
        }

        $statfolder = $container->getParameter('imgstatfolder');
        $staturl = $container->getParameter('imgstaturl');

        $dir = '/new_artist_img/'.($this->id%100);

        $filename = $this->id.'-'.date('dmY-U').'-'.rand().'.'.$this->getImagefile()->guessExtension();
        $filepath = $statfolder . $dir .'/'. $filename;
        $this->getImagefile()->move(
            $statfolder .$dir, $filename
        );
        echo $statfolder . $dir.'<br>';
        echo $filename;

        $path = $dir . "/" . $filename;

        $this->img = $path;

        $this->img_is_big = true;

        $this->imagefile = null;

        //make thumbs
        $imageGod = $container->get('imagegod');
        $size = $imageGod->getWidthHeight($filepath);
        if($size[0]>0 && $size[1]>0){
            $makethumbs = $imageGod->makeThumbs($filepath);
            if(!$makethumbs){
                return 'cant make image thumbs';
            }
        }
        else{
            return 'cant find image size '.$size[0].'x'.$size[1];
        }
        return 'no';
    }



    public function uploadCover(Container $container) {
        if (null === $this->getCoverFile()) {
            return;
        }

        $statfolder = $container->getParameter('imgstatfolder');
        $staturl = $container->getParameter('imgstaturl');

        $dir = '/new_artist_cover_img/'.($this->id%100);

        $filename = $this->id.'-'.date('dmY-U').'-'.rand().'.'.$this->getCoverFile()->guessExtension();
        $filepath = $statfolder . $dir .'/'. $filename;
        $this->getCoverFile()->move(
            $statfolder .$dir, $filename
        );
        echo $statfolder . $dir.'<br>';
        echo $filename;

        $path = $dir . "/" . $filename;

        $this->cover = $path;

        $this->img_is_big = true;

        $this->coverfile = null;

        //make thumbs
        $imageGod = $container->get('imagegod');
        $size = $imageGod->getWidthHeight($filepath);
        if($size[0]>0 && $size[1]>0){
            $makethumbs = $imageGod->makeThumbs($filepath);
            if(!$makethumbs){
                return 'cant make image thumbs';
            }
        }
        else{
            return 'cant find image size '.$size[0].'x'.$size[1];
        }
        return 'no';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->audarts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vidarts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->albumarts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set more_url
     *
     * @param string $moreUrl
     * @return Artist
     */
    public function setMoreUrl($moreUrl)
    {
        $this->more_url = $moreUrl;

        return $this;
    }

    /**
     * Get more_url
     *
     * @return string 
     */
    public function getMoreUrl()
    {
        return $this->more_url;
    }

    /**
     * Set page_url
     *
     * @param string $pageUrl
     * @return Artist
     */
    public function setPageUrl($pageUrl)
    {
        $this->page_url = $pageUrl;

        return $this;
    }

    /**
     * Get page_url
     *
     * @return string 
     */
    public function getPageUrl()
    {
        return $this->page_url;
    }

    /**
     * Set is_band
     *
     * @param boolean $isBand
     * @return Artist
     */
    public function setIsBand($isBand)
    {
        $this->is_band = $isBand;

        return $this;
    }

    /**
     * Get is_band
     *
     * @return boolean 
     */
    public function getIsBand()
    {
        return $this->is_band;
    }

    /**
     * Set is_mongolian
     *
     * @param boolean $isMongolian
     * @return Artist
     */
    public function setIsMongolian($isMongolian)
    {
        $this->is_mongolian = $isMongolian;

        return $this;
    }

    /**
     * Get is_mongolian
     *
     * @return boolean 
     */
    public function getIsMongolian()
    {
        return $this->is_mongolian;
    }

    /**
     * Add audarts
     *
     * @param \music\CmsBundle\Entity\AudioArtist $audarts
     * @return Artist
     */
    public function addAudart(\music\CmsBundle\Entity\AudioArtist $audarts)
    {
        $audarts->artist = $this;
        $this->audarts[] = $audarts;

        return $this;
    }


    /**
     * Add addalbumarts
     *
     * @param \music\CmsBundle\Entity\AlbumArtist $albumarts
     * @return Artist
     */
    public function albumarts(\music\CmsBundle\Entity\AlbumArtist $albumarts)
    {
        $albumarts->artist = $this;
        $this->addalbumarts[] = $albumarts;

        return $this;
    }

    /**
     * Remove audarts
     *
     * @param \music\CmsBundle\Entity\AudioArtist $audarts
     */
    public function removeAudart(\music\CmsBundle\Entity\AudioArtist $audarts)
    {
        $this->audarts->removeElement($audarts);
    }

    /**
     * Get audarts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAudarts()
    {
        return $this->audarts;
    }


    /**
     * Set img_is_big
     *
     * @param boolean $imgIsBig
     * @return Artist
     */
    public function setImgIsBig($imgIsBig)
    {
        $this->img_is_big = $imgIsBig;

        return $this;
    }

    /**
     * Get img_is_big
     *
     * @return boolean 
     */
    public function getImgIsBig()
    {
        return $this->img_is_big;
    }

    /**
     * Set like_count
     *
     * @param integer $likeCount
     * @return Artist
     */
    public function setLikeCount($likeCount)
    {
        $this->like_count = $likeCount;

        return $this;
    }

    /**
     * Get like_count
     *
     * @return integer 
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }

    /**
     * Add albumarts
     *
     * @param \music\CmsBundle\Entity\AlbumArtist $albumarts
     * @return Artist
     */
    public function addAlbumart(\music\CmsBundle\Entity\AlbumArtist $albumarts)
    {
        $this->albumarts[] = $albumarts;

        return $this;
    }

    /**
     * Remove albumarts
     *
     * @param \music\CmsBundle\Entity\AlbumArtist $albumarts
     */
    public function removeAlbumart(\music\CmsBundle\Entity\AlbumArtist $albumarts)
    {
        $this->albumarts->removeElement($albumarts);
    }

    /**
     * Get albumarts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlbumarts()
    {
        return $this->albumarts;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Artist
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set published_date
     *
     * @param \DateTime $publishedDate
     * @return Artist
     */
    public function setPublishedDate($publishedDate)
    {
        $this->published_date = $publishedDate;

        return $this;
    }

    /**
     * Get published_date
     *
     * @return \DateTime 
     */
    public function getPublishedDate()
    {
        return $this->published_date;
    }



//    /**
//     * Add artistaudio
//     *
//     * @param \music\CmsBundle\Entity\ArtistAudio $artistaudio
//     * @return Artist
//     */
//    public function addArtistaudio(\music\CmsBundle\Entity\ArtistAudio $artistaudio)
//    {
//        $this->artistaudio[] = $artistaudio;
//
//        return $this;
//    }

//    /**
//     * Remove artistaudio
//     *
//     * @param \music\CmsBundle\Entity\ArtistAudio $artistaudio
//     */
//    public function removeArtistaudio(\music\CmsBundle\Entity\ArtistAudio $artistaudio)
//    {
//        $this->artistaudio->removeElement($artistaudio);
//    }

    /**
     * Get artistaudio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtistaudio()
    {
        return $this->artistaudio;
    }




    /**
     * Add artistaudio
     *
     * @param \music\CmsBundle\Entity\AudioArtist $artistaudio
     * @return Artist
     */
    public function addArtistaudio(\music\CmsBundle\Entity\AudioArtist $artistaudio)
    {
        $this->artistaudio[] = $artistaudio;

        return $this;
    }

    /**
     * Remove artistaudio
     *
     * @param \music\CmsBundle\Entity\AudioArtist $artistaudio
     */
    public function removeArtistaudio(\music\CmsBundle\Entity\AudioArtist $artistaudio)
    {
        $this->artistaudio->removeElement($artistaudio);
    }

    /**
     * Set web_url
     *
     * @param string $webUrl
     * @return Artist
     */
    public function setWebUrl($webUrl)
    {
        $this->web_url = $webUrl;

        return $this;
    }

    /**
     * Get web_url
     *
     * @return string 
     */
    public function getWebUrl()
    {
        return $this->web_url;
    }

    /**
     * Set fb_url
     *
     * @param string $fbUrl
     * @return Artist
     */
    public function setFbUrl($fbUrl)
    {
        $this->fb_url = $fbUrl;

        return $this;
    }

    /**
     * Get fb_url
     *
     * @return string 
     */
    public function getFbUrl()
    {
        return $this->fb_url;
    }

    /**
     * Set tw_url
     *
     * @param string $twUrl
     * @return Artist
     */
    public function setTwUrl($twUrl)
    {
        $this->tw_url = $twUrl;

        return $this;
    }

    /**
     * Get tw_url
     *
     * @return string 
     */
    public function getTwUrl()
    {
        return $this->tw_url;
    }


    /**
     * Set cover
     *
     * @param string $cover
     * @return Artist
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string 
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Add vidarts
     *
     * @param \music\CmsBundle\Entity\ArtistVideo $vidarts
     * @return Artist
     */
    public function addVidart(\music\CmsBundle\Entity\ArtistVideo $vidarts)
    {
        $this->vidarts[] = $vidarts;

        return $this;
    }

    /**
     * Remove vidarts
     *
     * @param \music\CmsBundle\Entity\ArtistVideo $vidarts
     */
    public function removeVidart(\music\CmsBundle\Entity\ArtistVideo $vidarts)
    {
        $this->vidarts->removeElement($vidarts);
    }

    /**
     * Get vidarts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVidarts()
    {
        return $this->vidarts;
    }

    /**
     * Add artistvideo
     *
     * @param \music\CmsBundle\Entity\ArtistVideo $artistvideo
     * @return Artist
     */
    public function addArtistvideo(\music\CmsBundle\Entity\ArtistVideo $artistvideo)
    {
        $this->artistvideo[] = $artistvideo;

        return $this;
    }

    /**
     * Remove artistvideo
     *
     * @param \music\CmsBundle\Entity\ArtistVideo $artistvideo
     */
    public function removeArtistvideo(\music\CmsBundle\Entity\ArtistVideo $artistvideo)
    {
        $this->artistvideo->removeElement($artistvideo);
    }

    /**
     * Get artistvideo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtistvideo()
    {
        return $this->artistvideo;
    }
}
