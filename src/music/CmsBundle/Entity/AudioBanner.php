<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use music\CmsBundle\util\AudioDurationUtil;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\DependencyInjection\Container;

/**
 * AudioBanner
 *
 * @ORM\Table(name="msc_audio_banner")
 * @ORM\Entity
 */
class AudioBanner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    public $url;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=255)
     */
    private $provider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime")
     */
    private $endDate;


    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     *
     * @Assert\File(
     *        mimeTypesMessage = "mp3 файл биш байна!"
     * )
     *
     *
     */
    private $audiofile;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return AudioBanner
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set provider
     *
     * @param string $provider
     * @return AudioBanner
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string 
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return AudioBanner
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return AudioBanner
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set Audio
     *
     * @param UploadedFile $file
     */
    public function setAudioFile(UploadedFile $file = null)
    {
        $this->audiofile = $file;
    }

    /**
     * Get Audio
     *
     * @return UploadedFile
     */
    public function getAudioFile()
    {
        return $this->audiofile;
    }


    /**
     * Set duration
     *
     * @param integer $duration
     * @return AudioBanner
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }



    public function uploadAudio(Container $container)
    {
        if (null === $this->getAudioFile()) {
            return;
        }

        $statfolder = $container->getParameter('statfolder');
        $keyfolder = $container->getParameter('keyfolder');

        $mp3dir = '/new_banner_dwn/' . date('dmYU');


        $filename = rand();
        $filename_mp3 = $filename . '.mp3';


        // mp3
        $this->getAudioFile()->move(
            $statfolder . $mp3dir, $filename_mp3
        );


        $fs = new Filesystem();
        $keyfile = $this->getWebDirectory() . 'stat/sample.mp3.key';
        $newkey = $keyfolder . $mp3dir .'/'. $filename_mp3 . '.key';
        $fs->copy($keyfile, $newkey, true);


        $path = $mp3dir . "/" . $filename_mp3;

        $mp3file = new AudioDurationUtil($statfolder . $mp3dir . '/' . $filename_mp3);
        $duration = $mp3file->getDuration();


        $this->setUrl($path);
        $this->setDuration($duration);
        $this->audiofile = null;
    }
}
