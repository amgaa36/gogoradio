<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AlbumArtist
 *
 * @ORM\Table(name="msc_album_artist")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class AlbumArtist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Album")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $album;

    /**
     * @ORM\ManyToOne(targetEntity="Artist")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $artist;

    /**
     * @var integer
     *
     * @ORM\Column(name="artist_order", type="integer", nullable=true, options={"default":1})
     */

    public $order;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * ORM\PrePersist()
     */
    public function onPrePersist()
    {
        if(is_null($this->order)) {
            $this->order = 1;
        }
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return AlbumArtist
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set album
     *
     * @param \music\CmsBundle\Entity\Album $album
     * @return AlbumArtist
     */
    public function setAlbum(\music\CmsBundle\Entity\Album $album)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \music\CmsBundle\Entity\Album 
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Set artist
     *
     * @param \music\CmsBundle\Entity\Artist $artist
     * @return AlbumArtist
     */
    public function setArtist(\music\CmsBundle\Entity\Artist $artist)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return \music\CmsBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }
}
