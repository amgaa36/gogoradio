<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GenreMood
 *
 * @ORM\Table(name="msc_genre_mood")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class GenreMood
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="genreMood", type="string", length=255)
     */
    private $genreMood;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="genreMoodDescription", type="string", length=1000, nullable=true)
     */
    private $genreMoodDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="genreMoodDuration", type="integer", nullable=true)
     */
    private $genreMoodDuration;

    /**
     * @var integer
     *
     * @ORM\Column(name="genreMoodTrackNumber", type="integer", nullable=true)
     */
    private $genreMoodTrackNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="likeCount", type="integer", nullable=true)
     */
    private $likeCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedDate", type="datetime", nullable=true)
     */
    private $updatedDate;


    /**
     * @ORM\ManyToOne(targetEntity="GenreMood")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id", nullable=true)
     */
    private $parentId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set genreMood
     *
     * @param string $genreMood
     * @return GenreMood
     */
    public function setGenreMood($genreMood)
    {
        $this->genreMood = $genreMood;

        return $this;
    }

    /**
     * Get genreMood
     *
     * @return string
     */
    public function getGenreMood()
    {
        return $this->genreMood;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return GenreMood
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="genreMoodCoverImgUrl", type="string", length=1000)
     */
    private $genreMoodCoverImgUrl;

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set genreMoodDescription
     *
     * @param string $genreMoodDescription
     * @return GenreMood
     */
    public function setGenreMoodDescription($genreMoodDescription)
    {
        $this->genreMoodDescription = $genreMoodDescription;

        return $this;
    }

    /**
     * Get genreMoodDescription
     *
     * @return string
     */
    public function getGenreMoodDescription()
    {
        return $this->genreMoodDescription;
    }

    /**
     * Set genreMoodDuration
     *
     * @param string $genreMoodDuration
     * @return GenreMood
     */
    public function setGenreMoodDuration($genreMoodDuration)
    {
        $this->genreMoodDuration = $genreMoodDuration;

        return $this;
    }

    /**
     * Get genreMoodDuration
     *
     * @return string
     */
    public function getGenreMoodDuration()
    {
        return $this->genreMoodDuration;
    }

    /**
     * Set likeCount
     *
     * @param integer $likeCount
     * @return GenreMood
     */
    public function setLikeCount($likeCount)
    {
        $this->likeCount = $likeCount;

        return $this;
    }

    /**
     * Get likeCount
     *
     * @return integer
     */
    public function getLikeCount()
    {
        return $this->likeCount;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return GenreMood
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     * @return GenreMood
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getGenreMoodTrackNumber()
    {
        return $this->genreMoodTrackNumber;
    }

    /**
     * @param int $genreMoodTrackNumber
     */
    public function setGenreMoodTrackNumber($genreMoodTrackNumber)
    {
        $this->genreMoodTrackNumber = $genreMoodTrackNumber;
    }

    /**
     * @return string
     */
    public function getGenreMoodCoverImgUrl()
    {
        return $this->genreMoodCoverImgUrl;
    }

    /**
     * @param string $genreMoodCoverImgUrl
     */
    public function setGenreMoodCoverImgUrl($genreMoodCoverImgUrl)
    {
        $this->genreMoodCoverImgUrl = $genreMoodCoverImgUrl;
    }
}
