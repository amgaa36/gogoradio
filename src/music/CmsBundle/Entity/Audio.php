<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUser;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Translation\Tests\String;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use music\CmsBundle\util\AudioDurationUtil;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Audio
 *
 * @ORM\Table(name="msc_audio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Audio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="audio_name", type="string", length=255, nullable=false)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="audio_name_eng", type="string", length=255, nullable=false)
     */
    public $name_eng;

    /**
     * @var string
     *
     * @ORM\Column(name="audio_img", type="string", length=255, nullable=true)
     */
    public $img;

    /**
     *
     * @Assert\Image(
     *        mimeTypesMessage = "Зурган файл биш байна!"
     * )
     */

    public $imagefile;

    /**
     *
     * @Assert\File(
     *        mimeTypesMessage = "mp3 файл биш байна!"
     * )
     *
     *
     */
    public $audiofile;

    /**
     * @var string
     *
     * @ORM\Column(name="file_url", type="string", length=255,nullable=true)
     */
    public $file_url;

    /**
     * @var text
     *
     * @ORM\Column(name="lyrics", type="text", nullable=true)
     */
    public $lyrics;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="released_year", type="datetime", nullable=true)
     */
    public $released_year;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    public $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="listen_count", type="integer", nullable=true)
     */
    public $listen_count;

    /**
     * @var integer
     *
     * @ORM\Column(name="like_count", type="integer", nullable=true)
     */
    public $like_count;

    /**
     * @var integer
     *
     * @ORM\Column(name="dislike_count", type="integer", nullable=true)
     */
    public $dislike_count;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mongolian", type="boolean", nullable=false)
     */
    public $is_mongolian;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_lyrics", type="boolean", nullable=false, options={"default":0})
     */
    public $is_lyrics;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_video", type="boolean", nullable=false, options={"default":0})
     */
    public $is_video;

    /**
     * @var boolean
     *
     * @ORM\Column(name="img_is_big", type="boolean", nullable=true)
     */
    public $img_is_big;

    /**
     * @var boolean
     *
     * @ORM\Column(name="before_album", type="boolean", nullable=true)
     */
    public $beforeAlbum;

    /**
     * @ORM\ManyToOne(targetEntity="Studio")
     * @ORM\JoinColumn(name="studio_id", referencedColumnName="id", nullable=true)
     */
    public $studio;

    /**
     * @ORM\OneToMany(targetEntity="GenreAudio", mappedBy="audio", cascade={"persist"}, orphanRemoval=true)
     **/
    public $genre_audios;

    public $genres;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $created_date;

    /**
     * @ORM\OneToMany(targetEntity="AudioArtist", mappedBy="audio", cascade={"persist", "remove"}, orphanRemoval=true)
     **/
    public $audioartists;


    /**
     * @ORM\OneToMany(targetEntity="AlbumAudio", mappedBy="audio", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"order" = "ASC"})
     **/
    public $audioalbums;

    /**
     * @ORM\OneToMany(targetEntity="AudioVideo", mappedBy="audio", cascade={"persist"}, orphanRemoval=true)
     **/
    public $audio_videos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    public $updated_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="datetime", nullable=true)
     */
    public $publish_date;

    /**
     * @var string
     *
     * @ORM\Column(name="album_name", type="string", length=255, nullable=true)
     */
    public $album_name;

    /**
     * @var string
     *
     * @ORM\Column(name="album_name_html", type="string", length=300, nullable=true)
     */
    public $album_name_html;

    /**
     * @var string
     *
     * @ORM\Column(name="album_img", type="string", length=255, nullable=true)
     */
    public $album_img;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name", type="string", length=255, nullable=true)
     */
    public $artist_name;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_name_html", type="text", nullable=true)
     */
    public $artist_name_html;

    /**
     * @var string
     *
     * @ORM\Column(name="artist_img", type="string", length=255, nullable=true)
     */
    public $artist_img;

    /**
     * @var boolean
     *
     * @ORM\Column(name="album_count", type="integer", nullable=true, options={"default":0})
     */
    public $album_count;

    public $path;

    /**
     * @var boolean
     */

    public $isImage;

    /**
     * @var boolean
     */
    public $isAudio;


    /**
     * @var string
     *
     * @ORM\Column(name="hitone", type="string", length=255, nullable=true)
     */
    public $hitone;

    /**
     * @var string
     *
     * @ORM\Column(name="hitone_dahilt", type="string", length=255, nullable=true)
     */
    public $hitone_dahilt;

    /**
     * @var string
     *
     * @ORM\Column(name="hitone_badag", type="string", length=255, nullable=true)
     */
    public $hitone_badag;



    /**
     * @var string
     *
     * @ORM\Column(name="unimusic", type="string", length=255, nullable=true)
     */
    public $unimusic;

    /**
     * @var string
     *
     * @ORM\Column(name="unimusic_dahilt", type="string", length=255, nullable=true)
     */
    public $unimusic_dahilt;

    /**
     * @var string
     *
     * @ORM\Column(name="unimusic_badag", type="string", length=255, nullable=true)
     */
    public $unimusic_badag;


    /**
     * @var string
     *
     * @ORM\Column(name="doremi", type="string", length=255, nullable=true)
     */
    public $doremi;

    /**
     * @var string
     *
     * @ORM\Column(name="doremi_dahilt", type="string", length=255, nullable=true)
     */
    public $doremi_dahilt;

    /**
     * @var string
     *
     * @ORM\Column(name="doremi_badag", type="string", length=255, nullable=true)
     */
    public $doremi_badag;



    /**
     * @var boolean
     *
     * @ORM\Column(name="encrypted", type="boolean", nullable=true)
     */
    public $isEncrypted = false;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->audioartists = new \Doctrine\Common\Collections\ArrayCollection();
        $this->audio_videos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genre_audios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Audio
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Audio
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }


    /**
     * Set Image
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imagefile = $file;
    }

    /**
     * Get Image
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imagefile;
    }


    /**
     * Set Audio
     *
     * @param UploadedFile $file
     */
    public function setAudioFile(UploadedFile $file = null)
    {
        $this->audiofile = $file;
    }

    /**
     * Get Audio
     *
     * @return UploadedFile
     */
    public function getAudioFile()
    {
        return $this->audiofile;
    }

    /**
     * @ORM\OneToMany(targetEntity="UserLog", mappedBy="audio_id")
     */
    protected $audiosub;

    /**
     * Set file_url
     *
     * @param string $fileUrl
     * @return Audio
     */
    public function setFileUrl($fileUrl)
    {
        $this->file_url = $fileUrl;

        return $this;
    }

    /**
     * Get file_url
     *
     * @return string
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }

    /**
     * Set lyrics
     *
     * @param string $lyrics
     * @return Audio
     */
    public function setLyrics($lyrics)
    {
        $this->lyrics = $lyrics;

        return $this;
    }

    /**
     * Get lyrics
     *
     * @return string
     */
    public function getLyrics()
    {
        return $this->lyrics;
    }

    /**
     * Set released_year
     *
     * @param \DateTime $releasedYear
     * @return Audio
     */
    public function setReleasedYear($releasedYear)
    {
        $this->released_year = $releasedYear;

        return $this;
    }

    /**
     * Get released_year
     *
     * @return \DateTime
     */
    public function getReleasedYear()
    {
        return $this->released_year;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Audio
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set name_eng
     *
     * @param string $nameEng
     * @return Audio
     */
    public function setNameEng($nameEng)
    {
        $this->name_eng = $nameEng;
        return $this;
    }

    /**
     * Get name_eng
     *
     * @return string
     */
    public function getNameEng()
    {
        return $this->name_eng;
    }


    /**
     * Set listen_count
     *
     * @param integer $listenCount
     * @return Audio
     */
    public function setListenCount($listenCount)
    {
        $this->listen_count = $listenCount;

        return $this;
    }

    /**
     * Get listen_count
     *
     * @return integer
     */
    public function getListenCount()
    {
        return $this->listen_count;
    }

    /**
     * Set like_count
     *
     * @param integer $likeCount
     * @return Audio
     */
    public function setLikeCount($likeCount)
    {
        $this->like_count = $likeCount;

        return $this;
    }

    /**
     * Get like_count
     *
     * @return integer
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }



    /**
     * Set dislike_count
     *
     * @param integer $dislikeCount
     * @return Audio
     */
    public function setDislikeCount($dislikeCount)
    {
        $this->dislike_count = $dislikeCount;

        return $this;
    }

    /**
     * Get dislike_count
     *
     * @return integer
     */
    public function getDislikeCount()
    {
        return $this->dislike_count;
    }

    /**
     * Set is_mongolian
     *
     * @param boolean $isMongolian
     * @return Audio
     */
    public function setIsMongolian($isMongolian)
    {
        $this->is_mongolian = $isMongolian;

        return $this;
    }

    /**
     * Get is_mongolian
     *
     * @return boolean
     */
    public function getIsMongolian()
    {
        return $this->is_mongolian;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Audio
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set updated_date
     *
     * @param \DateTime $updatedDate
     * @return Audio
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updated_date = $updatedDate;

        return $this;
    }

    /**
     * Get updated_date
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * Set publish_date
     *
     * @param \DateTime $publishDate
     * @return Audio
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;

        return $this;
    }

    /**
     * Get publish_date
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * Set studio
     *
     * @param \music\CmsBundle\Entity\Studio $studio
     * @return Audio
     */
    public function setStudio(\music\CmsBundle\Entity\Studio $studio = null)
    {
        $this->studio = $studio;

        return $this;
    }

    /**
     * Get studio
     *
     * @return \music\CmsBundle\Entity\Studio
     */
    public function getStudio()
    {
        return $this->studio;
    }


    public function uploadImage(Container $container)
    {
        if (null === $this->getImagefile()) {
            return;
        }

        $statfolder = $container->getParameter('imgstatfolder');
        $staturl = $container->getParameter('imgstaturl');

        $dir = '/new_audio_img/' . ($this->id % 100);

        $filename = $this->id . '-' . date('dmYU') . '_' . rand() . '.' . $this->getImagefile()->guessExtension();
        $filepath = $statfolder . $dir . '/' . $filename;
        $this->getImagefile()->move(
            $statfolder . $dir, $filename
        );

        $path = $dir . "/" . $filename;

        $this->img = $path;
        $this->img_is_big = true;

        $this->imagefile = null;

        //make thumbs
        $imageGod = $container->get('imagegod');
        $size = $imageGod->getWidthHeight($filepath);
        if ($size[0] > 0 && $size[1] > 0) {
            $makethumbs = $imageGod->makeThumbs($filepath);
            if (!$makethumbs) {
                return 'cant make image thumbs';
            }
        } else {
            return 'cant find image size ' . $size[0] . 'x' . $size[1];
        }
        return 'no';
    }


    public function uploadAudio(Container $container)
    {
        if (null === $this->getAudioFile()) {
            return;
        }

        $statfolder = $container->getParameter('statfolder');
        $keyfolder = $container->getParameter('keyfolder');
        $mp3dir = '/new_audio_dwn/' . ($this->id % 100) . '/' . $this->id;


        $filename = $this->id . '_' . date('dmYU') . '_' . rand();
        $filename_mp3 = $filename . '.mp3';


        // mp3
        $this->getAudioFile()->move(
            $statfolder . $mp3dir, $filename_mp3
        );



        $fs = new Filesystem();
        $keyfile = $this->getWebDirectory() . 'stat/sample.mp3.key';
        $newkey = $keyfolder . $mp3dir .'/'. $filename_mp3 . '.key';
        $fs->copy($keyfile, $newkey, true);



        $path = $mp3dir . "/" . $filename_mp3;


//        $builder = new ProcessBuilder(array('/Users/amgaa/Desktop/cms_encode.sh', '02.GS6.mp3'));
//        $process =$builder->getProcess();
//        $process->start();


        $mp3file = new AudioDurationUtil($statfolder . $mp3dir . '/' . $filename_mp3);
        $duration = $mp3file->getDuration();


        $this->file_url = $path;
        $this->duration = $duration;
        $this->path = $mp3dir;
        $this->audiofile = null;
    }

    public function getWebDirectory()
    {
        return __DIR__ . "/../../../../web/";
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if (is_null($this->is_mongolian)) {
            $this->is_mongolian = false;
        }

        if (is_null($this->listen_count)) {
            $this->listen_count = 0;
        }

        if (is_null($this->like_count)) {
            $this->like_count = 0;
        }
        $this->created_date = new \DateTime("now");
    }


    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new \DateTime("now");
    }


    /**
     * Add audioartists
     *
     * @param \music\CmsBundle\Entity\AudioArtist $audioartists
     * @return Audio
     */
    public function addAudioartist(\music\CmsBundle\Entity\AudioArtist $audioartists)
    {
        $audioartists->setAudio($this);
        $this->audioartists[] = $audioartists;

        return $this;
    }

    /**
     * Remove audioartists
     *
     * @param \music\CmsBundle\Entity\AudioArtist $audioartists
     */
    public function removeAudioartist(\music\CmsBundle\Entity\AudioArtist $audioartists)
    {
        $audioartists->setAudio(null);
        $this->audioartists->removeElement($audioartists);
    }

    /**
     * Get audioartists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudioartists()
    {
        return $this->audioartists;
    }

    public function deletefolder($id)
    {
        //$command0 ="rmd -rf ".$statfolder.$m3u8dir;

        //exec ($command0);

    }

//    public function deleteHlsFolder(Container $container, $hlspath)
//    {
//        if ($hlspath) {
//            if (strpos($hlspath, 'new_audio_hls')) {
//                $statfolder = $container->getParameter('statfolder');
//                $hlsFolder = substr($hlspath, 0, strrpos($hlspath, '/'));
//
////                echo $hlspath.'<br>'.PHP_EOL;
////                echo $hlsFolder;
//
//                echo '---deleteHlsFolder---------------<br>' . PHP_EOL;
//                $output = array();
//                $res = -1;
//                $command = 'rm -rf ' . $statfolder . $hlsFolder;
//                echo $command . '<br>';
//                exec($command, $output, $res);
//                var_dump($output);
//                var_dump($res);
//                echo '<br>';
//
////                $fs = new Filesystem();
////                try {
////                    if($fs->exists($mp3path,$this->container))
////                        $fs->remove($mp3path,$this->container);
////                    $mp3 = str_replace('.m3u8','.mp3',$entity->file_url);
////                    if($fs->exists($statfolder.$mp3,$this->container))
////                        $fs->remove($statfolder.$mp3,$this->container);
////                } catch (IOException $e) {}
//            }
//        }
//    }

//    private function rrmdir($dir){
//
//        if (is_dir($dir))
//        {
//
//            $objects = scandir($dir);
//            foreach ($objects as $object) {
//                if ($object != "." && $object != "..") {
//                    var_dump("kk");
//                    if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
//                }
//            }
//            reset($objects);
//            rmdir($dir);
//        }
//    }


    /**
     * Set img_is_big
     *
     * @param boolean $imgIsBig
     * @return Audio
     */
    public function setImgIsBig($imgIsBig)
    {
        $this->img_is_big = $imgIsBig;

        return $this;
    }

    /**
     * Get img_is_big
     *
     * @return boolean
     */
    public function getImgIsBig()
    {
        return $this->img_is_big;
    }

    /**
     * Set beforeAlbum
     *
     * @param boolean $beforeAlbum
     * @return Audio
     */
    public function setBeforeAlbum($beforeAlbum)
    {
        $this->beforeAlbum = $beforeAlbum;

        return $this;
    }

    /**
     * Get beforeAlbum
     *
     * @return boolean
     */
    public function getBeforeAlbum()
    {
        return $this->beforeAlbum;
    }

    /**
     * Set album_name
     *
     * @param string $albumName
     * @return Audio
     */
    public function setAlbumName($albumName)
    {
        $this->album_name = $albumName;

        return $this;
    }

    /**
     * Get album_name
     *
     * @return string
     */
    public function getAlbumName()
    {
        return $this->album_name;
    }

    /**
     * Set album_name_html
     *
     * @param string $albumNameHtml
     * @return Audio
     */
    public function setAlbumNameHtml($albumNameHtml)
    {
        $this->album_name_html = $albumNameHtml;

        return $this;
    }

    /**
     * Get album_name_html
     *
     * @return string
     */
    public function getAlbumNameHtml()
    {
        return $this->album_name_html;
    }

    /**
     * Set album_img
     *
     * @param string $albumImg
     * @return Audio
     */
    public function setAlbumImg($albumImg)
    {
        $this->album_img = $albumImg;

        return $this;
    }

    /**
     * Get album_img
     *
     * @return string
     */
    public function getAlbumImg()
    {
        return $this->album_img;
    }

    /**
     * Set artist_name
     *
     * @param string $artistName
     * @return Audio
     */
    public function setArtistName($artistName)
    {
        $this->artist_name = $artistName;

        return $this;
    }

    /**
     * Get artist_name
     *
     * @return string
     */
    public function getArtistName()
    {
        return $this->artist_name;
    }

    /**
     * Set artist_name_html
     *
     * @param string $artistNameHtml
     * @return Audio
     */
    public function setArtistNameHtml($artistNameHtml)
    {
        $this->artist_name_html = $artistNameHtml;

        return $this;
    }

    /**
     * Get artist_name_html
     *
     * @return string
     */
    public function getArtistNameHtml()
    {
        return $this->artist_name_html;
    }

    /**
     * Set artist_img
     *
     * @param string $artistImg
     * @return Audio
     */
    public function setArtistImg($artistImg)
    {
        $this->artist_img = $artistImg;

        return $this;
    }

    /**
     * Get artist_img
     *
     * @return string
     */
    public function getArtistImg()
    {
        return $this->artist_img;
    }

    /**
     * Add audioalbums
     *
     * @param \music\CmsBundle\Entity\AlbumAudio $audioalbums
     * @return Audio
     */
    public function addAudioalbum(\music\CmsBundle\Entity\AlbumAudio $audioalbums)
    {
        $audioalbums->audio = $this;
        $this->audioalbums[] = $audioalbums;

        return $this;
    }

    /**
     * Remove audioalbums
     *
     * @param \music\CmsBundle\Entity\AlbumAudio $audioalbums
     */
    public function removeAudioalbum(\music\CmsBundle\Entity\AlbumAudio $audioalbums)
    {
        $this->audioalbums->removeElement($audioalbums);
    }

    /**
     * Get audioalbums
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudioalbums()
    {
        return $this->audioalbums;
    }

    /**
     * Add audio_videos
     *
     * @param \music\CmsBundle\Entity\AudioVideo $audioVideos
     * @return Audio
     */
    public function addAudioVideo(\music\CmsBundle\Entity\AudioVideo $audioVideos)
    {
        $audioVideos->setAudio($this);
        $this->audio_videos[] = $audioVideos;

        return $this;
    }

    /**
     * Remove audio_videos
     *
     * @param \music\CmsBundle\Entity\AudioVideo $audioVideos
     */
    public function removeAudioVideo(\music\CmsBundle\Entity\AudioVideo $audioVideos)
    {
        $audioVideos->setAudio(null);
        $this->audio_videos->removeElement($audioVideos);
    }

    /**
     * Get audio_videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudioVideos()
    {
        return $this->audio_videos;
    }

    /**
     * Set album_count
     *
     * @param integer $albumCount
     * @return Audio
     */
    public function setAlbumCount($albumCount)
    {
        $this->album_count = $albumCount;

        return $this;
    }

    /**
     * Get album_count
     *
     * @return integer
     */
    public function getAlbumCount()
    {
        return $this->album_count;
    }

    /**
     * Add genre_audios
     *
     * @param \music\CmsBundle\Entity\GenreAudio $genreAudios
     * @return Audio
     */
    public function addGenreAudio(\music\CmsBundle\Entity\GenreAudio $genreAudios)
    {
        $genreAudios->setAudio($this);
        $this->genre_audios[] = $genreAudios;

        return $this;
    }

    /**
     * Remove genre_audios
     *
     * @param \music\CmsBundle\Entity\GenreAudio $genreAudios
     */
    public function removeGenreAudio(\music\CmsBundle\Entity\GenreAudio $genreAudios)
    {
        $genreAudios->setAudio(null);
        $this->genre_audios->removeElement($genreAudios);
    }

    /**
     * Get genre_audios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenreAudios()
    {
        return $this->genre_audios;
    }

    /**
     * Set is_lyrics
     *
     * @param boolean $isLyrics
     * @return Audio
     */
    public function setIsLyrics($isLyrics)
    {
        $this->is_lyrics = $isLyrics;

        return $this;
    }

    /**
     * Get is_lyrics
     *
     * @return boolean 
     */
    public function getIsLyrics()
    {
        return $this->is_lyrics;
    }

    /**
     * Set is_video
     *
     * @param boolean $isVideo
     * @return Audio
     */
    public function setIsVideo($isVideo)
    {
        $this->is_video = $isVideo;

        return $this;
    }

    /**
     * Get is_video
     *
     * @return boolean 
     */
    public function getIsVideo()
    {
        return $this->is_video;
    }


    public function getLyrConf()
    {
        $this->is_lyrics = true;
    }

    public function getLyrfalse()
    {
        $this->is_lyrics = false;
    }
    public function getvideofalse()
    {
        $this->is_video = false;
    }

    /**
     * Set hitone
     *
     * @param string $hitone
     * @return Audio
     */
    public function setHitone($hitone)
    {
        $this->hitone = $hitone;

        return $this;
    }

    /**
     * Get hitone
     *
     * @return string 
     */
    public function getHitone()
    {
        return $this->hitone;
    }
}
