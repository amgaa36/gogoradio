<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserArtist
 *
 * @ORM\Table(name="msc_user_artist")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserArtist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Artist")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $artist;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $artist_id;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="followDate", type="datetime", nullable=true)
     */
    public $followDate;


    /**
     * Set followDate
     *
     * @param \DateTime $followDate
     * @return UserArtist
     */
    public function setFollowDate($followDate)
    {
        $this->followDate = $followDate;

        return $this;
    }

    /**
     * Get followDate
     *
     * @return \DateTime
     */
    public function getFollowDate()
    {
        return $this->followDate;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserArtist
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set artist_id
     *
     * @param integer $artistId
     * @return UserArtist
     */
    public function setArtistId($artistId)
    {
        $this->artist_id = $artistId;

        return $this;
    }

    /**
     * Get artist_id
     *
     * @return integer 
     */
    public function getArtistId()
    {
        return $this->artist_id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserArtist
     */
    public function setUser(\music\CmsBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set artist
     *
     * @param \music\CmsBundle\Entity\Artist $artist
     * @return UserArtist
     */
    public function setArtist(\music\CmsBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return \music\CmsBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }
}
