<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AlbumAudio
 *
 * @ORM\Table(name="msc_album_audio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class AlbumAudio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Album")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $album;

    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    public $audio;

    /**
     * @var integer
     *
     * @ORM\Column(name="audio_order", type="integer", nullable=true)
     */
    public $order;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return AlbumAudio
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set album
     *
     * @param \music\CmsBundle\Entity\Album $album
     * @return AlbumAudio
     */
    public function setAlbum(\music\CmsBundle\Entity\Album $album)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \music\CmsBundle\Entity\Album 
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return AlbumAudio
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }
}
