<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * GenreAudio
 *
 * @ORM\Table(name="msc_genre_audio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class GenreAudio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="genre_audios")
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $genre;

    /**
     * @ORM\ManyToOne(targetEntity="Audio", inversedBy="genre_audios")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $audio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set genre
     *
     * @param \music\CmsBundle\Entity\Genre $genre
     * @return GenreAudio
     */
    public function setGenre(\music\CmsBundle\Entity\Genre $genre = null)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return \music\CmsBundle\Entity\Genre 
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return GenreAudio
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio = null)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }
}
