<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAlbum
 *
 * @ORM\Table(name="msc_user_album")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserAlbum
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Album")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $album;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    public $album_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserAlbum
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set album_id
     *
     * @param integer $albumId
     * @return UserAlbum
     */
    public function setAlbumId($albumId)
    {
        $this->album_id = $albumId;

        return $this;
    }

    /**
     * Get album_id
     *
     * @return integer 
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * Set user
     *
     * @param \music\CmsBundle\Entity\User $user
     * @return UserAlbum
     */
    public function setUser(\music\CmsBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \music\CmsBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set album
     *
     * @param \music\CmsBundle\Entity\Album $album
     * @return UserAlbum
     */
    public function setAlbum(\music\CmsBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \music\CmsBundle\Entity\Album 
     */
    public function getAlbum()
    {
        return $this->album;
    }
}
