<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AudioArtist
 *
 * @ORM\Table(name="msc_audio_artist")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class AudioArtist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Audio")
     * @ORM\JoinColumn(name="audio_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    public $audio;

    /**
     * @ORM\ManyToOne(targetEntity="Artist")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    public $artist;

    /**
     * @var integer
     *
     * @ORM\Column(name="artist_order", type="integer",nullable=true)
     */
    public $order;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return AudioArtist
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set audio
     *
     * @param \music\CmsBundle\Entity\Audio $audio
     * @return AudioArtist
     */
    public function setAudio(\music\CmsBundle\Entity\Audio $audio = null)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return \music\CmsBundle\Entity\Audio 
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Set artist
     *
     * @param \music\CmsBundle\Entity\Artist $artist
     * @return AudioArtist
     */
    public function setArtist(\music\CmsBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return \music\CmsBundle\Entity\Artist 
     */
    public function getArtists()
    {
        return $this->artist;
    }


    /**
     * ORM\PrePersist()
     */
    public function onPrePersist()
    {
        if(is_null($this->order)) {
            $this->order = 0;
        }
    }

    /**
     * Get artist
     *
     * @return \music\CmsBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }
}
