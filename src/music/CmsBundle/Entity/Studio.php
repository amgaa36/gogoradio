<?php

namespace music\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Studios
 *
 * @ORM\Table(name="msc_studio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Studio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="established_date", type="date", nullable=true)
     */
    public $established_date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Studio
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set established_date
     *
     * @param \DateTime $establishedDate
     * @return Studio
     */
    public function setEstablishedDate($establishedDate)
    {
        $this->established_date = $establishedDate;

        return $this;
    }

    /**
     * Get established_date
     *
     * @return \DateTime 
     */
    public function getEstablishedDate()
    {
        return $this->established_date;
    }
}
