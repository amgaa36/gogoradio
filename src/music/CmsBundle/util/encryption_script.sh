#!/bin/bash

keyFile="video.key"
openssl rand 16 > $keyFile
encryptionKey=`cat $keyFile | hexdump -e '16/1 "%02x"'`

splitFilePrefix=$1
encryptedSplitFilePrefix="$enc/{splitFilePrefix}"

numberOfTsFiles=`ls ${splitFilePrefix}*.ts | wc -l`

mkdir enc
for (( i=0; i<$numberOfTsFiles; i++ ))
do
initializationVector=`printf '%032x' $i`
openssl aes-128-cbc -e -in ${splitFilePrefix}$i.ts -out ${encryptedSplitFilePrefix}$i.ts -nosalt -iv $initializationVector -K $encryptionKey
done

mv $keyFile ./enc
