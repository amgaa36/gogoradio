<?php
/**
 * Created by PhpStorm.
 * User: ankhaa
 * Date: 2/27/15
 * Time: 3:10 PM
 */

namespace music\CmsBundle\util;

use music\CmsBundle\Entity\Album;
use music\CmsBundle\Entity\Audio;
use music\CmsBundle\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

class AlbumUpdateUtil
{
    public function updateAudioByAlbumTypeArray($album, Controller $controller)
    {
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
          update msc_audio au
          left join msc_album_audio alau on au.id=alau.audio_id
          set
          au.album_name = :album_name,
          au.album_name_html = :album_name_html,
          au.album_img = :album_img
          where alau.album_id=:album_id
        ");
        $statement->execute(array(
            ':album_id' => $album['id'],
            ':album_name' => $album['name'],
            ':album_name_html' => $album['name'] . ' (html)',
            ':album_img' => $album['img'],
        ));
    }

    public function updateAudioByAlbumTypeObject($album, Controller $controller)
    {
        $em = $controller->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
          update msc_audio au
          left join msc_album_audio alau on au.id=alau.audio_id
          set
          au.album_name = :album_name,
          au.album_name_html = :album_name_html,
          au.album_img = :album_img
          where alau.album_id=:album_id
        ");
        $statement->execute(array(
            ':album_id' => $album->getId(),
            ':album_name' => $album->name,
            ':album_name_html' => $album->name . ' (html)',
            ':album_img' => $album->img,
        ));
    }

    public function updateAlbumArtistInfoByAlbumObject(Controller $controller, Album $album)
    {
        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AlbumArtist')->createQueryBuilder('e');

        $albumartists = $qb
            ->leftJoin('e.artist', 'artist')
            ->addSelect('artist')
            ->where($qb->expr()->eq('e.album', ':album'))
            ->setParameter('album', $album)
            ->getQuery()
            ->getArrayResult();

        $album_names = array();
        $album_imgs = array();
        $album_html = array();
        foreach ($albumartists as $albumartist) {
            $album_names[] = $albumartist['artist']['name'];
            $album_imgs = $albumartist['artist']['img'];
            $path = $controller->generateUrl('web_artist_detail', array('id' => $albumartist['artist']['id']));
            $album_html [] = '<a class="pjx-url" href="' . $path . '">' . $albumartist['artist']['name'].'</a>';
        }


        if (count($album_names) > 0)
            $album->setArtistName(implode(', ', $album_names));
        else
            $album->setArtistName(null);
        if ($album_imgs && trim($album_imgs) != '')
            $album->setArtistImg($album_imgs);
        else
            $album->setArtistImg(null);
        if (count($album_html) > 0)
            $album->setArtistNameHtml(implode(', ', $album_html));
        else
            $album->setArtistNameHtml(null);


        $em->persist($album);
        $em->flush();
    }


    public function updateAudioArtistInfoByAudioObject(Controller $controller, Audio $audio)
    {
        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('e');

        $audioartists = $qb
            ->leftJoin('e.artist', 'artist')
            ->addSelect('artist')
            ->where($qb->expr()->eq('e.audio', ':audio'))
            ->setParameter('audio', $audio)
            ->getQuery()
            ->getArrayResult();

        $audio_names = array();
        $audio_imgs = array();
        $audio_html = array();
        foreach ($audioartists as $audioartist) {
            $audio_names[] = $audioartist['artist']['name'];
            $audio_imgs = $audioartist['artist']['img'];
            $path = $controller->generateUrl('web_artist_detail', array('id' => $audioartist['artist']['id']));
            $audio_html [] = '<a class="pjx-url" href="' . $path . '">' . $audioartist['artist']['name'].'</a>';
        }


        if (count($audio_names) > 0)
            $audio->setArtistName(implode(', ', $audio_names));
        else
            $audio->setArtistName(null);
        if ($audio_imgs && trim($audio_imgs) != '')
            $audio->setArtistImg($audio_imgs);
        else
            $audio->setArtistImg(null);
        if (count($audio_html) > 0)
            $audio->setArtistNameHtml(implode(', ', $audio_html));
        else
            $audio->setArtistNameHtml(null);

        $em->persist($audio);
        $em->flush();
    }

    public function updateAudioAlbumInfoByAudioObject(Controller $controller, Audio $audio)
    {
        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');

        $audioalbums = $qb
            ->leftJoin('e.album', 'album')
            ->addSelect('album')
            ->where($qb->expr()->eq('e.audio', ':audio'))
            ->setParameter('audio', $audio)
            ->getQuery()
            ->getArrayResult();

        $album_names = array();
        $album_imgs = array();
        $album_html = array();

        foreach ($audioalbums as $audioalbum) {

            $album_names[] = $audioalbum['album']['name'];
            $album_imgs = $audioalbum['album']['img'];

            $path = $controller->generateUrl('web_album_detail', array('id' => $audioalbum['album']['id']));
            $album_html [] = '<a class="pjx-url" href="' . $path . '">' . $audioalbum['album']['name'].'</a>';
        }

        if (count($album_names) > 0)
            $audio->setAlbumName(implode(', ', $album_names));
        else
            $audio->setAlbumName(null);
        if ($album_imgs && trim($album_imgs) != '')
            $audio->setAlbumImg($album_imgs);
        else
            $audio->setAlbumImg(null);

        if (count($album_html) > 0)
            $audio->setAlbumNameHtml(implode(', ', $album_html));
        else
            $audio->setAlbumNameHtml(null);


        $em->persist($audio);
        $em->flush();
    }

    public function updateAlbumAudioInfoByAlbumObject(Controller $controller, Album $album)
    {
        $em = $controller->getDoctrine()->getManager();

        $albumaudios = $album->getAudioalbums();

        $kaka = array();
        foreach ($albumaudios as $albumaud) {

            $audioalbums = $albumaud->audio->getAudioalbums();

            $album_names = array();
            $album_imgs = '';
            $album_html = array();

            foreach ($audioalbums as $audioalbum) {
                $album_names[] = $audioalbum->album->name;
                $album_imgs = $audioalbum->album->img;
                $path = $controller->generateUrl('web_album_detail', array('id' => $audioalbum->album->getId()));
                $album_html [] = '<a class="pjx-url" href="' . $path . '">' . $audioalbum->album->name.'</a>';

            }

            if (count($album_names) > 0) {
                $albumaud->audio->setAlbumName(implode(', ', $album_names));
            } else
                $albumaud->audio->setAlbumName(null);

            if ($album_imgs && trim($album_imgs) != '')
                $albumaud->audio->setAlbumImg($album_imgs);
            else
                $albumaud->audio->setAlbumImg(null);

            if (count($album_html) > 0) {
                $albumaud->audio->setAlbumNameHtml(implode(', ', $album_html));
            } else
                $albumaud->audio->setAlbumNameHtml(null);

            $em->persist($albumaud->audio);
            $kaka[] = array('id' => $albumaud->audio->getId(), 'name' => $albumaud->audio->album_name, 'html' =>  $albumaud->audio->album_name_html);
        }
        $em->flush();
        //throw $controller->createNotFoundException(var_dump($kaka));
    }

    public function updateAlbumTrackInfoByAudio(Controller $controller, Album $album)
    {
        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');

        $albumaudios = $qb
            ->leftJoin('e.audio', 'audio')
            ->addSelect('audio')
            ->where($qb->expr()->eq('e.album', ':album'))
            ->setParameter('album', $album)
            ->getQuery()
            ->getArrayResult();

        $album_duration = 0;

        foreach ($albumaudios as $albumaudio) {
            $album_duration += $albumaudio['audio']['duration'];
        }


        if (count($albumaudios) > 0) {
            $album->setTrackNumber(count($albumaudios));
        } else
            $album->setTrackNumber(null);

        if ($album_duration && trim($album_duration) != '')
            $album->setDuration($album_duration);
        else
            $album->setDuration(null);

        $em->persist($album);
        $em->flush();
    }


    public function updateAudioTrackInfoByAlbum(Controller $controller, Audio $audio, $albumids)
    {
        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');

        $albumaudios = $qb
            ->where($qb->expr()->eq('e.audio', ':audio'))
            ->setParameter('audio', $audio)
            ->getQuery()
            ->getResult();
        foreach ($albumaudios as $albumaudio) {
            $album = $albumaudio->album;
            $qb_audio = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
            $audioalbum = $qb_audio
                ->leftJoin('e.audio', 'a')
                ->addSelect('a')
                ->where($qb_audio->expr()->eq('e.album', ':album'))
                ->setParameter('album', $albumaudio->album)
                ->getQuery()
                ->getArrayResult();
            $count = count($audioalbum);

            $album_duration = 0;

            foreach ($audioalbum as $audioalbum) {
                $album_duration += $audioalbum['audio']['duration'];
            }

            if ($count > 0) {
                $album->setTrackNumber($count);
            } else
                $album->setTrackNumber(null);

            if ($album_duration && trim($album_duration) != '')
                $album->setDuration($album_duration);
            else
                $album->setDuration(null);

            $em->persist($album);
        }

        $qb_album = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('e');

        $rem_albums = $qb_album
            ->where($qb_album->expr()->in('e.id', ':albumids'))
            ->setParameter('albumids', $albumids)
            ->getQuery()
            ->getResult();

        foreach ($rem_albums as $rem_album) {
            $qb_audio = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
            $audioalbum = $qb_audio
                ->leftJoin('e.audio', 'a')
                ->addSelect('a')
                ->where($qb_audio->expr()->eq('e.album', ':album'))
                ->setParameter('album', $rem_album)
                ->getQuery()
                ->getArrayResult();
            $count = count($audioalbum);

            $album_duration = 0;

            foreach ($audioalbum as $audioalbum) {
                $album_duration += $audioalbum['audio']['duration'];
            }

            if ($count > 0) {
                $rem_album->setTrackNumber($count);
            } else
                $rem_album->setTrackNumber(null);

            if ($album_duration && trim($album_duration) != '')
                $rem_album->setDuration($album_duration);
            else
                $rem_album->setDuration(null);

            $em->persist($rem_album);
        }

        $em->flush();
    }

    public function updateVideoArtistInfoByVideoObject(Controller $controller, Video $video)
    {
        $em = $controller->getDoctrine()->getManager();

        $qb = $em->getRepository('musicCmsBundle:ArtistVideo')->createQueryBuilder('e');

        $videoartists = $qb
            ->leftJoin('e.artist', 'artist')
            ->addSelect('artist')
            ->where($qb->expr()->eq('e.video', ':video'))
            ->setParameter('video', $video)
            ->getQuery()
            ->getArrayResult();

        $video_names = array();
        $video_imgs = array();
        $video_html = array();
        foreach ($videoartists as $videoartist) {
            $video_names[] = $videoartist['artist']['name'];
            $video_imgs = $videoartist['artist']['img'];
            $path = $controller->generateUrl('web_artist_detail', array('id' => $videoartist['artist']['id']));
            $video_html [] = '<a class="pjx-url" href="' . $path . '">' . $videoartist['artist']['name'].'</a>';
        }


        if (count($video_names) > 0)
            $video->setArtistName(implode(', ', $video_names));
        else
            $video->setArtistName(null);
        if ($video_imgs && trim($video_imgs) != '')
            $video->setArtistImg($video_imgs);
        else
            $video->setArtistImg(null);
        if (count($video_html) > 0)
            $video->setArtistNameHtml(implode(', ', $video_html));
        else
            $video->setArtistNameHtml(null);

        $em->persist($video);
        $em->flush();
    }

} 