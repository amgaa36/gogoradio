<?php
/**
 * Created by PhpStorm.
 * User: tsetsee
 */

namespace music\CmsBundle\util;

use music\CmsBundle\Entity\Video;
use Symfony\Component\DependencyInjection\Container;

class VideoUpdateUtil
{

    public function updateVideoArtist(Container $container,Video &$video)
    {
        $em = $container->get('doctrine')->getManager();

        $qb_tmp = $em->getRepository('musicCmsBundle:AudioVideo')->createQueryBuilder('e');

        $audvid = $qb_tmp
            ->leftJoin('e.audio', 'audio')
            ->addSelect('audio')
            ->where($qb_tmp->expr()->eq('e.video', ':vid'))
            ->setParameter('vid', $video)
            ->getQuery()
            ->getArrayResult();

        $video->setArtist(implode(', ', array_map(function ($n) {
            return $n['audio']['artist_name'];
        }, $audvid)));

        //throw \Exception($video->getArtist());

        $em->persist($video);
        $em->flush();

    }

} 