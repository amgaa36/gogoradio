<?php
/**
 * Created by PhpStorm.
 * User: ankhaa
 * Date: 3/13/15
 * Time: 11:15 AM
 */

namespace music\CmsBundle\util;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class CheckUtil
{
    public function audioCheck(ContainerInterface $container, EntityManager $em)
    {

        $queryBuilder = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('a');

        $countQueryBuilder = clone $queryBuilder;
        $count = $countQueryBuilder->select('count(a.id)')->getQuery()->getSingleScalarResult();
        $pagesize = 500;
        $pagecount = intval($count / $pagesize);
        if ($count % $pagesize > 0) {
            $pagecount++;
        }

        $fs = new Filesystem();
        $noimgcount = 0;
        $nohlscount = 0;
        $nomp3count = 0;
        for ($page = 1; $page <= $pagecount; $page++) {
            $audios = $queryBuilder
                ->setFirstResult(($page - 1) * $pagesize)
                ->setMaxResults($pagesize)
                ->getQuery()
                ->getArrayResult();
            //echo 'page-'.$pagecount.'->'.$page.' uldsen='.($count-$page*$pagesize).'<br>'.PHP_EOL;
            foreach ($audios as $audio) {
                $hasimg = true;
                $hasimger = false;
                if ($audio['img']) {
                    try {
                        if (!$fs->exists($container->getParameter('imgstatfolder') . $audio['img']))
                            $hasimg = false;
                    } catch (IOException $e) {
                        $hasimger = true;
                    }
                }

                $hashls = true;
                $hashlser = false;
                if ($audio['file_url'] && trim($audio['file_url']) != '') {
                    try {
                        if (!$fs->exists($container->getParameter('statfolder') . $audio['file_url']))
                            $hashls = false;
                    } catch (IOException $e) {
                        $hashlser = true;
                    }
                }

                $hasmp3 = true;
                $hasmp3er = false;
                $mp3path = false;
                if ($audio['file_url'] && trim($audio['file_url']) != '') {
                    if (strpos($audio['file_url'], 'old_audio_hls')) {
                        $mp3path = str_replace('/old_audio_hls', '/old_audio_dwn', $audio['file_url']);
                        $mp3path = str_replace('.m3u8', '.mp3', $mp3path);
                    } else if (strpos($audio['file_url'], 'new_audio_hls')) {
                        $mp3path = str_replace('/new_audio_hls', '/new_audio_dwn', $audio['file_url']);
                        $mp3path = str_replace('.m3u8', '.mp3', $mp3path);
                    }
                    if ($mp3path) {
                        try {
                            if (!$fs->exists($container->getParameter('statfolder') . $mp3path))
                                $hasmp3 = false;
                        } catch (IOException $e) {
                            $hasmp3er = true;
                        }
                    }
                }

                if (!$hasimg || $hasimger || !$hashls || $hashlser || !$hasmp3 || $hasmp3er) {
                    if (!$hasimg || $hasimger) {
                        $noimgcount++;
                    }
                    echo '<span>' . $audio['id'] . ' -> ';
                    if (!$hasimg) {
                        echo 'no img[' . $container->getParameter('statfolder') . $audio['img'] . '] ';
                    }
                    if ($hasimger) {
                        echo 'img err[' . $container->getParameter('statfolder') . $audio['img'] . '] ';
                    }

                    if (!$hashls || $hashlser) {
                        $nohlscount++;
                    }
                    if (!$hashls) {
                        echo 'no hls[' . $container->getParameter('statfolder') . $audio['file_url'] . '] ';
                    }
                    if ($hashlser) {
                        echo 'hls err[' . $container->getParameter('statfolder') . $audio['file_url'] . '] ';
                    }

                    if (!$hasmp3 || $hasmp3er) {
                        $nomp3count++;
                    }
                    if (!$hasmp3) {
                        echo 'no mp3[' . $container->getParameter('statfolder') . $mp3path . '] ';
                    }
                    if ($hasmp3er) {
                        echo 'mp3 err[' . $container->getParameter('statfolder') . $mp3path . '] ';
                    }
                    echo ' ===> ' . $audio['artist_name'] . ' - ' . $audio['name'];
                    echo '</span><br>' . PHP_EOL;
                }
//                if(!$hashls || $hashlser || !$hasmp3 || $hasmp3er){
//                    if(!$hashls || $hashlser){
//                        if(!$hasmp3 || $hasmp3er){
//
//                        }
//                        else{
//                            //mp3 n bga murtluu hls bhgui filuud
//                            $nohlscount++;
//                            echo '<span>';
//                            echo $container->getParameter('statfolder').$mp3path;
//                            echo '</span><br>'.PHP_EOL;
//                        }
//                    }
//                }
            }
        }
        echo '1no img count=' . $noimgcount . ', no hls count=' . $nohlscount . ', no mp3 count=' . $nomp3count . '<br>' . PHP_EOL;
    }

    public function HTMlupdateAudioArtist(Controller $controller)
    {


        $em = $controller->getDoctrine()->getManager();
//        $qb = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('e');
//
//        $audioartists = $qb
//            ->leftJoin('e.audio', 'a')
//            ->where("a.artist_name_html is NULL")
//            ->getQuery()
//            ->getResult();
//
//
//        $logger = $controller->get('logger');
//
//        $batch_size = 20;
//        $batch_index = 1;
//        foreach ($audioartists as $audioartist) {
//
//            if (!$audioartist->artist) continue;
//
//            $qb1 = $em->getRepository('musicCmsBundle:AudioArtist')->createQueryBuilder('e');
//
//            $artists = $qb1
//                ->where($qb->expr()->eq('e.audio', ':audio'))
//                ->setParameter('audio', $audioartist->audio)
//                ->getQuery()
//                ->getResult();
//            $audio_html = array();
//            foreach ($artists as $artist) {
//
//                $path = $controller->generateUrl('web_artist_detail', array('id' => $artist->artist->getId()));
//                $audio_html [] = '<a class="left-pjx-url" href="' . $path . '">' . $artist->artist->name . '</a>';
//            }
//            if (count($audio_html) > 0)
//                $audioartist->audio->setArtistNameHtml(implode(', ', $audio_html));
//            else
//                $audioartist->audio->setArtistNameHtml(null);
//
//
//            $em->persist($audioartist->audio);
//            if ($batch_index % $batch_size === 0)
//                $em->flush();
//            $batch_index = ($batch_index + 1) % $batch_size;
//
//            $logger->addInfo('tsetsee: ', array(
//                'ID: ' . $audioartist->audio->getId(),
//                'NAME: ' . $audioartist->audio->name,
//                'Artist NAME: ' . implode(', ', $audio_html),
//            ));
//
//        }
//        $em->flush();

        $noartistHTMLcount = 0;
        $artistHTMLcount = 0;

        $queryBuilder = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('e');
        $audios = $queryBuilder
            ->getQuery()
            ->getResult();
        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>Artist holbogdoogvi duunuud</b></h2>' . '';
        echo '---------------------------------------------------------' . '<br>';
        foreach ($audios as $audio) {

            if ($audio->artist_name_html == null) {
                $hashtml = false;
                $noartistHTMLcount++;
            } else {
                $hashtml = true;
                $artistHTMLcount++;
            }

            if (!$hashtml) {

                echo '<span>' . $audio->getId() . ' -> ';
                if (!$hashtml) {
                    echo '<a target = "_blank" href = "http://music.gogo.mn:8888/cms/artist/' . $audio->getId() . '/audi/add"> ' . $audio->name . '</a>';
                }

                echo '</span><br>' . PHP_EOL;
            }
        }
        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>html file set hiigdeegvi : ' . $noartistHTMLcount . '</b></h2>' . PHP_EOL;
        echo '<h2><b>html file set hiigdsen : ' . $artistHTMLcount . '</b></h2>' . PHP_EOL;
    }


    public function HTMlupdateAlbumArtist(Controller $controller)
    {

        $em = $controller->getDoctrine()->getManager();
//        $qb = $em->getRepository('musicCmsBundle:AlbumArtist')->createQueryBuilder('e');
//
//        $albumartists = $qb
//            ->leftJoin('e.album', 'a')
//            ->where("a.artist_name_html is NULL")
//            ->getQuery()
//            ->getResult();
//
//
//        $logger = $controller->get('logger');
//
//        $batch_size = 20;
//        $batch_index = 1;
//        foreach ($albumartists as $albumartist) {
//
//            if (!$albumartist->artist) continue;
//
//            $qb1 = $em->getRepository('musicCmsBundle:AlbumArtist')->createQueryBuilder('e');
//
//            $artists = $qb1
//                ->where($qb->expr()->eq('e.album', ':album'))
//                ->setParameter('album', $albumartist->album)
//                ->getQuery()
//                ->getResult();
//            $album_html = array();
//            foreach ($artists as $artist) {
//
//                $path = $controller->generateUrl('web_artist_detail', array('id' => $artist->artist->getId()));
//                $album_html [] = '<a class="left-pjx-url" href="' . $path . '">' . $artist->artist->name . '</a>';
//            }
//            if (count($album_html) > 0)
//                $albumartist->album->setArtistNameHtml(implode(', ', $album_html));
//            else
//                $albumartist->album->setArtistNameHtml(null);
//
//
//            $em->persist($albumartist->album);
//            if ($batch_index % $batch_size === 0)
//                $em->flush();
//            $batch_index = ($batch_index + 1) % $batch_size;
//
//            $logger->addInfo('tsetsee: ', array(
//                'ID: ' . $albumartist->album->getId(),
//                'NAME: ' . $albumartist->album->name,
//                'Artist NAME: ' . implode(', ', $album_html),
//            ));
//
//        }
//        $em->flush();

        $noartistHTMLcount = 0;
        $artistHTMLcount = 0;
        $dar = 1;
        $queryBuilder = $em->getRepository('musicCmsBundle:Album')->createQueryBuilder('e');
        $albums = $queryBuilder
            ->getQuery()
            ->getResult();
        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>Artist holbogdoogvi albumnuud</b></h2>' . '';
        echo '---------------------------------------------------------' . '<br>';
        foreach ($albums as $album) {

            if ($album->artist_name_html == null) {
                $hashtml = false;
                $noartistHTMLcount++;
            } else {
                $hashtml = true;
                $artistHTMLcount++;
            }

            if (!$hashtml) {

                echo '<span>' . $dar++ . '-> ' . $album->getId() . ' -> ';
                if (!$hashtml) {
                    echo '<a target = "_blank" href = "http://music.gogo.mn:8888/cms/album/' . $album->getId() . '/artist/add">' . $album->name . '</a>';
                }

                echo '</span><br>' . PHP_EOL;
            }
        }
        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>html file set hiigdeegvi : ' . $noartistHTMLcount . '</b></h2>' . PHP_EOL;
        echo '<h2><b>html file set hiigdsen : ' . $artistHTMLcount . '</b></h2>' . PHP_EOL;
    }

    public function HTMlupdateAudioAlbum(Controller $controller)
    {


        $em = $controller->getDoctrine()->getManager();
//        $qb = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
//
//        $audioalbums = $qb
//            ->leftJoin('e.audio', 'a')
//            ->where("a.album_name_html is NULL")
//            ->getQuery()
//            ->getResult();
//
//
//        $logger = $controller->get('logger');
//
//        $batch_size = 20;
//        $batch_index = 1;
//        foreach ($audioalbums as $audioalbum) {
//
//            if (!$audioalbum->album) continue;
//
//            $qb1 = $em->getRepository('musicCmsBundle:AlbumAudio')->createQueryBuilder('e');
//
//            $albums = $qb1
//                ->where($qb->expr()->eq('e.audio', ':audio'))
//                ->setParameter('audio', $audioalbum->audio)
//                ->getQuery()
//                ->getResult();
//            $audio_html = array();
//            foreach ($albums as $album) {
//
//                $path = $controller->generateUrl('web_album_detail', array('id' => $album->album->getId()));
//                $audio_html [] = '<a class="left-pjx-url" href="' . $path . '">' . $album->album->name . '</a>';
//            }
//            if (count($audio_html) > 0)
//                $audioalbum->audio->setAlbumNameHtml(implode(', ', $audio_html));
//            else
//                $audioalbum->audio->setAlbumNameHtml(null);
//
//
//            $em->persist($audioalbum->audio);
//            if ($batch_index % $batch_size === 0)
//                $em->flush();
//            $batch_index = ($batch_index + 1) % $batch_size;
//
//            $logger->addInfo('tsetsee: ', array(
//                'ID: ' . $audioalbum->audio->getId(),
//                'NAME: ' . $audioalbum->audio->name,
//                'Artist NAME: ' . implode(', ', $audio_html),
//            ));
//
//        }
//        $em->flush();

        $noartistHTMLcount = 0;
        $artistHTMLcount = 0;
        $des = 1;
        $queryBuilder = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('e');
        $audios = $queryBuilder
            ->getQuery()
            ->getResult();
        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>Album holbogdoogvi duunuud</b></h2>' . '';
        echo '---------------------------------------------------------' . '<br>';
        foreach ($audios as $audio) {

            if ($audio->album_name_html == null) {
                $hashtml = false;
                $noartistHTMLcount++;
            } else {
                $hashtml = true;
                $artistHTMLcount++;
            }

            if (!$hashtml) {

                echo '<span>' . $des++ . '->' . $audio->getId() . ' -> ';
                if (!$hashtml) {
                    echo $audio->name;
                }

                echo '</span><br>' . PHP_EOL;
            }
        }
        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>html file set hiigdeegvi : ' . $noartistHTMLcount . '</b></h2>' . PHP_EOL;
        echo '<h2><b>html file set hiigdsen : ' . $artistHTMLcount . '</b></h2>' . PHP_EOL;
    }

    public function publishArtistNoAudio(Controller $controller)
    {


        $em = $controller->getDoctrine()->getManager();
        $noartistHTMLcount = 0;;
        $queryBuilder = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('e');
//        $artists = $queryBuilder
//            ->select('e')
//            ->where($queryBuilder->expr()->isNotNull('e.published_date'))
//            ->andWhere($queryBuilder->expr()->lte('e.published_date',':date'))
//            ->setParameter('date', new \DateTime('now'))
//            ->getQuery()
//            ->getResult();


        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>Niitlegdsen duugvi  duuchid</b></h2>' . '';
        echo '---------------------------------------------------------' . '<br>';

        //foreach ($artists as $artist) {


        $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('a');

        $audioartists = $qb
            ->where($qb->expr()->isNotNull('a.published_date'))
            ->andWhere($qb->expr()->lte('a.published_date', ':date'))
            ->setParameter('date', new \DateTime('now'))
            ->andWhere($qb->expr()->eq('(SELECT COUNT(aa.audio) FROM musicCmsBundle:AudioArtist aa where aa.artist=a)', '0'))
            ->getQuery()
            ->getArrayResult();

        foreach ($audioartists as $audioartist) {
            echo $audioartist['id'] . ' -> ' . $audioartist['name'] . '<br>';


        }

        // }

        echo '---------------------------------------------------------' . '<br>';
        echo '<h2><b>Niit : ' . count($audioartists) . '</b></h2>' . PHP_EOL;
    }

    public function showgenreAudio(Controller $controller, $page)
    {
        $count = 1;
        $count1 = 0;
        $em = $controller->getDoctrine()->getManager();

        $pagesize = 50;

        $audioids = $em->getRepository('musicCmsBundle:GenreAudio')->createQueryBuilder('e')->select('a.id')->leftJoin('e.audio', 'a')->getQuery()->getScalarResult();

        $qb_audio = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('e');
        $audioalbums = $qb_audio
            ->where($qb_audio->expr()->notIn('e.id', ':ids'))
            ->setParameter('ids', $audioids)
            ->setFirstResult(($page - 1) * $pagesize)
            ->setMaxResults($pagesize)
            ->getQuery()
            ->getResult();

        echo '<h2>' . 'Genre-d oroogvi duunuud' . '</h2>';

        foreach ($audioalbums as $aud) {

            echo $count . '->' . $aud->getId() . '->' . $aud->name . '<br>';
            $count++;
            $count1++;
        }

        echo '<h2>bugd:' . $count1 . '</h2>';
    }


    public function showgenreArtist(Controller $controller, $page)
    {
        $em = $controller->getDoctrine()->getManager();
        $count = 0;

        echo '<h2>' . 'Genre-d oroogvi duuchid' . '</h2>';

        $connection = $em->getConnection();

        $statement = $connection->prepare("select art.id, art.artist_name from msc_audio_artist aa
                    left join msc_artist art on aa.artist_id=art.id
                    where aa.audio_id in
                    (select a.id from msc_audio a left join msc_genre_audio ga on a.id=ga.audio_id
                    where ga.genre_id is null)
                    group by art.id, art.artist_name");

        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $res) {
            echo $res['id'] . '->' . $res['artist_name'] . '<br>';
            $count++;
        }
        echo '<h2>Bugd : ' . $count.'</h2>';

    }


} 