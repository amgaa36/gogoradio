<?php
/**
 * Created by PhpStorm.
 * User: tsetsee
 */

namespace music\CmsBundle\util;

use Symfony\Component\DependencyInjection\Container;

class GenreUpdateUtil
{

    public function updateGenresByAudio(Container $container, &$genres)
    {
        $em = $container->get('doctrine')->getManager();

        foreach ($genres as &$genre) {
            $qb = $em->getRepository('musicCmsBundle:GenreAudio')->createQueryBuilder('e');
            $result = $qb
                ->leftJoin('e.audio', 'a')
                ->select($qb->expr()->count('a.id') . ' AS numbers')
                ->addSelect('SUM(a.duration) AS duration')
                ->where($qb->expr()->eq('e.genre', ':genre'))
                ->setParameter('genre', $genre)
                ->getQuery()
                ->getArrayResult();
            $genre->setDuration($result[0]['duration'] + 0);
            $genre->setTrackNumber($result[0]['numbers']);
            $em->persist($genre);
        }

        $em->flush();

    }

} 