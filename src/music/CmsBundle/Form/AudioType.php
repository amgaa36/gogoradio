<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AudioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder



            ->add('name','text', array(
                'label'=>'Монгол нэр',
                'required'=>true,
            ))

            ->add('name_eng','text', array(
                'label'=>'Гадаад нэр',
                'required'=>false,
            ))

            ->add('imagefile', 'file', array(
                'label' => 'Зураг оруулах',
                'required' => false,
                'attr' => array(
                    'class'=>'btn btn-success fileinput-button',
                )))
            ->add('isImage', 'checkbox', array(
                'label'=>'Зураг устгах эсэх',
                'required'=>false,
            ))

            ->add('audiofile', 'file', array(
                'label' => 'Дуу оруулах',
                'required' => false, 'attr' => array(
                    'class'=>'btn btn-success fileinput-button',
                )))

            ->add('hitone','text', array(
                'label'=>'Hitone эхлэл код',
                'required'=>false,
            ))

            ->add('hitone_dahilt','text', array(
                'label'=>'Hitone дахилт код',
                'required'=>false,
            ))

            ->add('hitone_badag','text', array(
                'label'=>'Hitone бадаг код',
                'required'=>false,
            ))

            ->add('unimusic','text', array(
                'label'=>'Unimusic эхлэл код',
                'required'=>false,
            ))

            ->add('unimusic_dahilt','text', array(
                'label'=>'Unimusic дахилт код',
                'required'=>false,
            ))

            ->add('unimusic_badag','text', array(
                'label'=>'Unimusic бадаг код',
                'required'=>false,
            ))

            ->add('doremi','text', array(
                'label'=>'DoReMi эхлэл код',
                'required'=>false,
            ))

            ->add('doremi_dahilt','text', array(
                'label'=>'DoReMi дахилт код',
                'required'=>false,
            ))

            ->add('doremi_badag','text', array(
                'label'=>'DoReMi бадаг код',
                'required'=>false,
            ))

            ->add('isAudio', 'checkbox', array(
                'label'=>'Дууны файл устгах эсэх',
                'required'=>false,
            ))
            ->add('lyrics','textarea', array(
                'label'=>'Дууны үг',
                'required'=> false
            ))
            ->add('released_year','date',array(
                'label'=>'Бүтээсэн огноо',
                'required' => false,
                'years' => range(Date('Y') - 20, date('Y')+1)
                ))
            ->add('duration', 'integer', array(
                'label' => 'Хугацаа',
                'required' => false,
                'disabled' => true,
            ))
            ->add('listen_count', 'integer', array(
                'label' => 'Сонссон тоо',
                'required' => false,
                'disabled' => true,
            ))
            ->add('like_count', 'integer', array(
                'label' => 'Таалсан тоо',
                'required' => false,
                'disabled' => true,
            ))
            ->add('is_mongolian', 'checkbox', array(
                'label'=>'Монгол эсэх',
                'required'=>false,
            ))
            ->add('publish_date','date',array(
                'label'=>'Нийтлэх огноо',
                'required' => false,
                'years' => range(Date('Y') - 20, date('Y')+1)
                ))
            ->add('studio', 'entity', array(
                'label'=>'Студи',
                'class' => 'musicCmsBundle:Studio',
                'property' => 'name',
                'required' => false,
            ))
            ->add('genres','entity', array(
                'label'=>'Жанр',
                'class' => 'musicCmsBundle:Genre',
                'property' => 'name',
                'required' => false,
                'expanded' => true,
                'multiple'=> true,
            ))
       ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Audio'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_audio';
    }

}
