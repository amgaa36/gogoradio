<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArtistSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text',array('label'=>'Нэр','required'=>false,'attr'=>array('maxlength'=>'255')))
            ->add('img', 'choice',array(
                'choices'   => array('1' => 'Тйим', '2' => 'Үгүй'),
                'label'=>'Зураг шинэчлэсэн эсэх',
                'required'=>false,
                'expanded' => true,))
            ->add('published_date', 'choice',array(
                'choices'   => array('1' => 'Нийтлэгдсэн', '2' => 'Ирээдүйд нийтлэгдэх', '3' =>'Тохиргоо хийгдээгүй'),
                'label'=>'Нийтлэгдсэн эсэх',
                'required'=>false,
                'expanded' => true,))

            ->add('is_mongolian', 'choice',array(
                'choices'   => array('1' => 'Тйим', '2' => 'Үгүй'),
                'label'=>'Монгол эсэх',
                'required'=>false,
                'expanded' => true,))


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Artist'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'musiccms_artist_search';
    }
}
