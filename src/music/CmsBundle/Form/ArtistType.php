<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArtistType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
//            ->add('img')

            ->add('imagefile', 'file', array(
                'label' => 'Зураг',
                'required' => false,
                'attr' => array(
                    'class'=>'btn btn-success fileinput-button',
                )
            ))

            ->add('coverfile', 'file', array(
                'label' => 'Ковер зураг нэмэх',
                'required' => false,
                'attr' => array(
                    'class'=>'btn btn-success fileinput-button',
                )
            ))
            ->add('description')
            ->add('career')
            ->add('web_url','text' ,array(
                'label' => 'Веб хаяг',
                'required' => false,
                ))
            ->add('fb_url','text' ,array(
                'label' => 'Фэйсбүүк хаяг',
                'required' => false,
            ))

            ->add('tw_url','text' ,array(
                'label' => 'Твиттер хаяг',
                'required' => false,
            ))

            ->add('is_band', 'choice', array(
                'choices' => array('1' => 'Тйим', '0' => 'Үгүй'),
                'label' => 'Хамтлаг эсэх',
                'required' => true,
                'expanded' => true,))

            ->add('ispublish', 'choice', array(
                'choices' => array('1' => 'Тийм', '0' => 'Үгүй'),
                'label' => 'Дууг нийтлэх эсэх',
                'required' => true,
                'data' => 0,
                'expanded' => true,
            ))
            ->add('published_date', 'date', array(
                'label' => 'Нийтлэх огноо',
                'required' => false,
                'years' => range(Date('Y') - 20, date('Y') + 1)
            ))
            ->add('birthday')
            ->add('height')
            ->add('nationality')
            ->add('is_mongolian', 'choice', array(
                'choices' => array('1' => 'Тйим', '0' => 'Үгүй'),
                'label' => 'Монгол эсэх',
                'required' => true,
                'expanded' => true,
            ))


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Artist'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_artist';
    }
}
