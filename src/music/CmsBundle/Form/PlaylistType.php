<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlaylistType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Нэр'
            ))
            ->add('imagefile', 'file', array(
                'label' => 'Зураг оруулах',
                'required' => false,
                'attr' => array(
                    'class' => 'btn btn-success fileinput-button',
                )))
            ->add('user_name', 'text', array(
                'label' => 'Хэрэглэгч нэр',
                'required' => false,
                'disabled' => true,
            ))
            ->add('description', 'text', array(
                'label' => 'Тайлбар',
                'required' => false,
            ))
            ->add('duration', 'text', array(
                'label' => 'Хугацаа',
                'required' => false,
                'disabled' => true,
            ))
            ->add('track_number', 'text', array(
                'label' => 'Дууны тоо',
                'required' => false,
                'disabled' => true,
            ))
            ->add('likeCount', 'text', array(
                'label' => 'Like тоо',
                'required' => false,
                'disabled' => true,
            ))
            ->add('created_date', 'date', array(
                'label' => 'Үүсгэсэн огноо',
                 'required' => false,
            ))
            ->add('updated_date', 'date', array(
                'label' => 'Өөрчлөгдсөн огноо',
                'required' => false,
                'disabled' => true,
            ))
            ->add('publish_date', 'date', array(
                'label' => 'Нийтлэх огноо',
                 'required' => false,
            ))
            ->add('is_public', 'checkbox', array(
                'label'=>'Нийтлэх эсэх',

                'required'=>false,
            ));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Playlist'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_playlist';
    }
}
