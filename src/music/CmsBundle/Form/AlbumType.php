<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AlbumType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text', array(
                'label'=> 'Нэр',
                'required'=> true,
            ))
            ->add('imagefile','file', array(
                'label' => 'Зураг оруулах',
                'required' => false,
                'attr' => array(
                    'class'=>'btn btn-success fileinput-button',
                )))
//            ->add('track_number')
//            ->add('duration')
            ->add('released_date','date',array(
                'label'=>'Гаргасан огноо',
                'years' => range(1960,2020),
                'required' => false,
            ))
            ->add('is_mongolian','checkbox',array(
                'label'=>'Монгол эсэх',
                'required' => false,
            ))
//            ->add('like_count')

            ->add('ispublish', 'choice', array(
                'choices' => array('1'=>'Тийм', '0'=> 'Үгүй'),
                'label' => 'Дууг нийтлэх эсэх',
                'required'=>true,
                'data' => 0,
                'expanded'=>true,
            ))
            ->add('publish_date','datetime',array(
                'years' => range(1960,2020),
                'label'=>'Нийтлэх огноо',
                'required' => false,
            ))
            ->add('studio', 'entity', array(
                'label' => 'Студи',
                'class' => 'musicCmsBundle:Studio',
                'property' => 'name',
                'required' => false,
            ))
            ->add('artistids', 'text', array(
                'label' => 'Уран бүтээлчид',
                'required' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Album'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_album';
    }
}

