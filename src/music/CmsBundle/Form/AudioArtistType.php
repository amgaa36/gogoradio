<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AudioArtistType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order')

            ->add('audio', 'entity', array(
                'class' => 'musicCmsBundle:Audio',
                'property' => 'name',
                'required' => false,
            ))


            ->add('artists', 'entity', array(
                'class' => 'musicCmsBundle:Artist',
                'property' => 'name',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
            ))

//            ->add('audio')
//            ->add('artist')
        ;
    }
    
//    /**
//     * @param OptionsResolverInterface $resolver
//     */
//    public function setDefaultOptions(OptionsResolverInterface $resolver)
//    {
//        $resolver->setDefaults(array(
//            'data_class' => 'music\CmsBundle\Entity\AudioArtist'
//        ));
//    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_audioartist';
    }
}
