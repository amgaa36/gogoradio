<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VideoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Нэр:',
                'required' => true,
            ))
            ->add('videoUrl','text', array(
                'label' => 'Видео:',
                'required' => true,
                 ))
            ->add('descr','textarea', array(
                'label' => 'Тайлбар',
                'required' => false,
                 ))
            ->add('publish_date', 'datetime', array(
                'label' => 'Нийтлэх огноо:',
                'required' => false,
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Video'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_video';
    }
}
