<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GenreType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Нэр',
            ))
            ->add('name_en', 'text', array(
                'label'    => 'Нэр (Англи)',
                'required' => false,
            ))
            ->add('description', 'textarea', array(
                'label'    => 'Тайлбар',
                'required' => false,
            ))
            ->add('img_file', 'file', array(
                'label'    => 'Зураг',
                'required' => false,
            ))
            ->add('img', 'text', array(
                'label'    => 'Зураг URL',
                'required' => false,
            ))
            ->add('icon_file', 'file', array(
                'label'    => 'Icon',
                'required' => false,
            ))
            ->add('icon', 'text', array(
                'label'    => 'Icon URL',
                'required' => false,
            ))
            ->add('thumb_file', 'file', array(
                'label'    => 'Mobile thumb зураг',
                'required' => false,
            ))
            ->add('thumb_android', 'text', array(
                'label'    => 'Mobile thumb URL',
                'required' => false,
            ))
            ->add('backgroundImg_file', 'file', array(
                'label'    => 'Background Зураг',
                'required' => false,
            ))
            ->add('backgroundImg', 'text', array(
                'label'    => 'Background Зураг URL',
                'required' => false,
            ))
            ->add('duration', 'integer', array(
                'label'    => 'Үргэлжлэх хугацаа',
                'required' => false,
                'disabled' => true,
            ))
            ->add('track_number', 'integer', array(
                'label'    => 'Дууны тоо',
                'required' => false,
                'disabled' => true,
            ))
            ->add('like_count', 'integer', array(
                'label'    => 'Like-н тоо',
                'disabled' => true,
                'required' => false,
            ))
//            ->add('show', 'checkbox', array(
//                'label'    => 'Радиод харагдах',
//                'required' => false,
//            ))
            ->add('publish_date', 'datetime', array(
                'label'    => 'Нийтлэх огноо',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\Genre'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_genre';
    }
}
