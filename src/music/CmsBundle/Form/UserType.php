<?php

namespace music\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('myroles', 'choice', array(
                'choices' => array('ROLE_ADMIN' => 'ROLE_ADMIN', 'ROLE_ACTOR' => 'ROLE_ACTOR'),
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'label' => 'Permission олгох'
            ))
//            ->add('myroles')
            ->add('socialId')
            ->add('username')
            ->add('loginName', 'text', array(
                'label' => 'Нэвтрэх нэр'
            ))
            ->add('loginType', 'text', array(
                'label' => 'Нэвтрэх төрөл'
            ))
            ->add('lastName', 'text', array(
                'label' => 'Овог',
                'required' => false,
            ))
            ->add('firstName', 'text', array(
                'label' => 'Нэр'
            ))
            ->add('userBirthday', 'date', array(
                'label' => 'Төрсөн огноо'
            ))
            ->add('userEmail', 'text', array(
                'label' => 'Цахим шуудан'
            ))
            ->add('imagefile', 'file', array(
                'label' => 'Зураг',
                'required' => false,
            ))
            ->add('userImageUrl')
            ->add('following')
            ->add('followers')
            ->add('isPremium', 'checkbox', array(
                'label' => 'Premium эсэх',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'music\CmsBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'music_cmsbundle_user';
    }
}
