<?php

namespace music\MobileBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use music\CmsBundle\Entity\Audio;

class AudioController extends Controller
{
    public function newAction(Request $request){

        $lastDateStr = $request->get('date');
        $size = $request->get('pageSize');

        if($lastDateStr == null){
            $lastDate = new \DateTime('now');
        }else{
            $lastDate = new \DateTime($lastDateStr);
        }

        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $artists = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('a')
            ->select("a.id, a.artist_name, a.name,COALESCE(a.img, a.album_img, a.artist_img) as img, a.file_url, a.publish_date")
            ->where('a.publish_date < :pdate')
            ->setParameter(':pdate', $lastDate)
            ->orderBy('a.publish_date','desc')
            ->setMaxResults($size)
            ->getQuery()
            ->getScalarResult();

        $arr['audios'] = $artists;
        return new JsonResponse($arr);
    }

    public function chartAction(){
        $arr = array();

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $currentTable = 'msc_chart_'.date("Y");
        $string = 'select startDate from '.$currentTable.' order by id desc limit 1';
        $statement = $connection->prepare($string);
        $statement->execute();
        $result = $statement->fetch();
        if(!$result){
            $currentTable = 'msc_chart_'.(date('Y')-1);
            $string = 'select startDate from '.$currentTable.' order by id desc limit 1';
            $statement = $connection->prepare($string);
            $statement->execute();
            $result = $statement->fetch();
        }
        $currentDate =  new \DateTime($result['startDate']);

        $queryString = "select hit.sortId as rank,(hit.sortId-hit.prevWeekPos) as up, au.id as id, au.audio_name as name, au.hitone as hitone, COALESCE (au.audio_img,au.album_img,au.artist_img) as img, au.file_url as file_url, au.is_lyrics, au.publish_date, au.artist_name as artist_name
                    from ".$currentTable." hit, msc_audio au
                     where au.publish_date <= :now and hit.audioId = au.id and hit.startDate = :startDate order by rank asc";
        $statement = $connection->prepare($queryString);
        $statement->execute(array(':now' => date_format(new \DateTime(), 'Y-m-d H:i:s'), ':startDate'=>date_format($currentDate,'Y-m-d')));
        $arr['audios'] = $statement->fetchAll();

        return new JsonResponse($arr);
    }

    function translateAction(Request $request){
        $min = $request->get('min');
        $max = $request->get('max');
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $treshDate = new \DateTime("2015-04-06 14:34:07");
        $query = $em->createQuery('SELECT au from musicCmsBundle:Audio au where au.is_mongolian = 1 order by au.created_date DESC')
            ->setMaxResults($max-$min)
            ->setFirstResult($min);
        $audios = $query->getResult();
       for($i=0; $i < count($audios); $i++){
           $audio = $audios[$i];
           $fin = '';
           if($this->isRussian($audio->getName())){
               $results = array();
               preg_match_all('/./u', $audio->getName(), $results);
               foreach ($results[0] as $res) {
                   $fin .= $this->character($res);
               }
           }
           $audio->setNameEng($fin);
           $em->persist($audio);
       }
        $em->flush();
//        $arr['count'] = $audios;

//        $arr = $this->getUser();
        return new JsonResponse(array('count'=>count($audios),'min'=>$min,'max'=>$max));
    }

    function isRussian($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }

    private function character($datas)
    {
        $result = '';

        if      ($datas == 'А') { $result = 'A'; } else if ($datas == 'а') { $result = 'a';}
        else if ($datas == 'Б') { $result = 'B'; } else if ($datas == 'б') { $result = 'b';}
        else if ($datas == 'В') { $result = 'V'; } else if ($datas == 'в') { $result = 'v';}
        else if ($datas == 'Г') { $result = 'G'; } else if ($datas == 'г') { $result = 'g';}
        else if ($datas == 'Д') { $result = 'D'; } else if ($datas == 'д') { $result = 'd';}

        else if ($datas == 'Е') { $result = 'E'; } else if ($datas == 'е') { $result = 'e';}
        else if ($datas == 'Ё') { $result = 'E'; } else if ($datas == 'ё') { $result = 'e';}
        else if ($datas == 'Ж') { $result = 'J'; } else if ($datas == 'ж') { $result = 'j';}
        else if ($datas == 'З') { $result = 'Z'; } else if ($datas == 'з') { $result = 'z';}
        else if ($datas == 'И') { $result = 'I'; } else if ($datas == 'и') { $result = 'i';}

        else if ($datas == 'й') { $result = 'i';}
        else if ($datas == 'К') { $result = 'K'; } else if ($datas == 'к') { $result = 'k';}
        else if ($datas == 'Л') { $result = 'L'; } else if ($datas == 'л') { $result = 'l';}
        else if ($datas == 'М') { $result = 'M'; } else if ($datas == 'м') { $result = 'm';}
        else if ($datas == 'Н') { $result = 'N'; } else if ($datas == 'н') { $result = 'n';}

        else if ($datas == 'О') { $result = 'O'; } else if ($datas == 'о') { $result = 'o';}
        else if ($datas == 'Ө') { $result = 'O'; } else if ($datas == 'ө') { $result = 'o';}
        else if ($datas == 'П') { $result = 'P'; } else if ($datas == 'п') { $result = 'p';}
        else if ($datas == 'Р') { $result = 'R'; } else if ($datas == 'р') { $result = 'r';}
        else if ($datas == 'С') { $result = 'S'; } else if ($datas == 'с') { $result = 's';}

        else if ($datas == 'Т') { $result = 'T'; } else if ($datas == 'т') { $result = 't';}
        else if ($datas == 'У') { $result = 'U'; } else if ($datas == 'у') { $result = 'u';}
        else if ($datas == 'Ү') { $result = 'V'; } else if ($datas == 'ү') { $result = 'v';}
        else if ($datas == 'Ф') { $result = 'P'; } else if ($datas == 'ф') { $result = 'p';}
        else if ($datas == 'Х') { $result = 'H'; } else if ($datas == 'х') { $result = 'h';}

        else if ($datas == 'Ц') { $result = 'Ts'; } else if ($datas == 'ц') { $result = 'ts';}
        else if ($datas == 'Ч') { $result = 'Ch'; } else if ($datas == 'ч') { $result = 'ch';}
        else if ($datas == 'Ш') { $result = 'Sh'; } else if ($datas == 'ш') { $result = 'sh';}
        else if ($datas == 'Щ') { $result = 'Sh'; } else if ($datas == 'щ') { $result = 'sh';}
        else if ($datas == 'ь') { $result = 'i';}

        else if ($datas == 'ы') { $result = 'i';}
        else if ($datas == 'ъ') { $result = 'i';}
        else if ($datas == 'Э') { $result = 'E'; } else if ($datas == 'э') { $result = 'e';}
        else if ($datas == 'Ю') { $result = 'Yu'; } else if ($datas == 'ю') { $result = 'yu';}
        else if ($datas == 'Я') { $result = 'Ya';}   else if ($datas == 'я') { $result = 'ya';}

        else if ($datas == ' ') { $result = ' '; }
        else if ($datas == '.') { $result = '.'; }
        else if ($datas == ',') { $result = ','; }
        else if ($datas == '(') { $result = '('; }
        else if ($datas == ')') { $result = ')'; }
        else if ($datas == '1') { $result = '1'; }
        else if ($datas == '2') { $result = '2'; }
        else if ($datas == '3') { $result = '3'; }
        else if ($datas == '4') { $result = '4'; }
        else if ($datas == '5') { $result = '5'; }
        else if ($datas == '6') { $result = '6'; }
        else if ($datas == '7') { $result = '7'; }
        else if ($datas == '8') { $result = '8'; }
        else if ($datas == '9') { $result = '9'; }
        else if ($datas == '0') { $result = '0'; }
        else if ($datas == '-') { $result = '-'; }
        else if ($datas == '=') { $result = '='; }
        else if ($datas == ';') { $result = ';'; }
        else if ($datas == '}') { $result = '}'; }
        else if ($datas == '{') { $result = '{'; }
        else if ($datas == '/') { $result = '/'; }
        else if ($datas == '~') { $result = '~'; }
        else if ($datas == '>') { $result = '>'; }
        else if ($datas == '<') { $result = '<'; }
        else if ($datas == '!') { $result = '!'; }
        else if ($datas == '*') { $result = '*'; }
        else if ($datas == '%') { $result = '%'; }
        else if ($datas == '$') { $result = '$'; }
        else if ($datas == '₮') { $result = '₮'; }
        else if ($datas == '#') { $result = '#'; }
        else if ($datas == '@') { $result = '@'; }

        else {
            $result = '0';
        }

        return $result;
    }

}
