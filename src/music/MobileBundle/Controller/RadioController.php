<?php
/**
 * Created by PhpStorm.
 * User: amgaa
 * Date: 8/4/15
 * Time: 9:50 AM
 */


namespace music\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class RadioController extends Controller
{
    public function indexAction($name)
    {
        return new JsonResponse(array('you'=>$name));
    }

    public function radiosAction(){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('musicCmsBundle:Genre')->createQueryBuilder('e');
        $genres = $qb->select('e.name, e.img, e.id, e.icon, e.backgroundImg, e.thumb_android')
            ->addSelect('rand() as hidden rand')
            ->andWhere($qb->expr()->isNotNull('e.publish_date'))
            ->andWhere($qb->expr()->lte('e.publish_date', ':now'))
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('rand')
            ->getQuery()
            ->getArrayResult()
        ;
        $arr['radio_list'] = $genres;

        return new JsonResponse($arr);
    }

    public function audiosByGenreAction(Request $request){
        $genreId = $request->get("genreId");
        $em = $this->getDoctrine()->getManager();
        $genre = $this->getDoctrine()->getRepository('musicCmsBundle:Genre')->findOneById($genreId);
        $qb = $em->getRepository('musicCmsBundle:GenreAudio')->createQueryBuilder('e');
        $audios = $qb
            ->leftJoin('e.audio', 'a')
            ->addSelect('a')
            ->addSelect('RAND() as HIDDEN rand')
            ->where($qb->expr()->eq('e.genre', ':genre'))
            ->setParameter('genre', $genre)
            ->andWhere('a.is_mongolian=1')
            ->andWhere($qb->expr()->isNotNull('a.publish_date'))
            ->andWhere($qb->expr()->lte('a.publish_date', ':now'))
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('rand')
            ->setMaxResults(50)
            ->getQuery()
            ->getArrayResult();
        $arr['audios'] = $audios;
        return new JsonResponse($arr);

    }

}
