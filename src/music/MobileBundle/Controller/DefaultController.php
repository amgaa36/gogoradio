<?php

namespace music\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return new JsonResponse(array('you'=>$name));
    }




    public function searchAudioAction(Request $request){

        $squery = $request->get('query');
        $isr = $this->isRussian($squery);
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $artists = $em->getRepository('musicCmsBundle:Audio')->createQueryBuilder('a')
            ->select("a.id, a.artist_name, a.name, a.img as img, a.file_url")
            ->where('a.publish_date < :pdate')
            ->andWhere('a.name like :query')
            ->setParameter(':pdate', new \DateTime('now'))
            ->setParameter(':query','%'.$squery.'%')
            ->getQuery()
            ->getArrayResult();
        $arr['audios'] = $artists;
        return new JsonResponse($arr);
    }



    public function searchPlaylistAction(Request $request){

        $squery = $request->get('query');
        $isr = $this->isRussian($squery);
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $playlists = $em->getRepository('musicCmsBundle:Playlist')->createQueryBuilder('p')
            ->select("p.id, p.name, p.img,p.user_name,p.duration,p.track_number,p.publish_date")
            ->where('p.publish_date < :pdate')
            ->andWhere('p.track_number > 5')
            ->andWhere('p.name like :query')
            ->orderBy('p.publish_date','desc')
            ->setParameter(':pdate',new \DateTime('now'))
            ->setParameter(':query','%'.$squery.'%')
            ->getQuery()
            ->getScalarResult();

        $arr['playlists'] = $playlists;
        return new JsonResponse($arr);
    }

    function isRussian($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }

}
