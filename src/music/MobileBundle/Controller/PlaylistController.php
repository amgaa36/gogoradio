<?php
/**
 * Created by PhpStorm.
 * User: Amgaa
 * Date: 7/9/15
 * Time: 10:23 AM
 */

namespace music\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class PlaylistController extends Controller
{
    public function homePlaylistAction(){
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $playlists = $em->getRepository('musicCmsBundle:Playlist')->createQueryBuilder('p')
            ->select("p.id, p.name, p.img,p.user_name,p.duration,p.track_number,p.publish_date")
            ->where('p.publish_date < :pdate')
            ->andWhere('p.track_number > 5')
            ->orderBy('p.publish_date','desc')
            ->setParameter(':pdate',new \DateTime('now'))
            ->setMaxResults(20)
            ->getQuery()
            ->getScalarResult();

        $arr['playlists'] = $playlists;
        return new JsonResponse($arr);
    }

    public function playlistAudiosAction(Request $request){
        $id = $request->get('playlistId');
        $em = $this->getDoctrine()->getManager();
        $audioQB = $em->getRepository('musicCmsBundle:PlaylistAudio')->createQueryBuilder('pa')
            ->addSelect("aud.id as id, aud.name, aud.artist_name as artist_name, COALESCE(aud.img, aud.album_img, aud.artist_img) as img, aud.file_url as file_url")
            ->leftJoin('pa.audio','aud')
            ->leftJoin('pa.playlist','pl')
            ->where('pl.id=:pid')
            ->setParameter('pid',$id)
            ->getQuery()
            ->getScalarResult();
        return new JsonResponse(array('audios'=>$audioQB));
    }



    public function createPlaylistAction(Request $request){


        $userid = $request->get('userid');
        $em = $this->getDoctrine()->getManager();
        $me = $em->getReference('musicCmsBundle:User', $userid);
        if(!$me) return new JsonResponse(array('success'=>0,'desc'=>'user not found'));

        $name = $request->get ( "name" );
        $isPublic = $request->get( "isPublic" );
        $description = $request->get ( "description" );

        if ($name == null || strlen ( trim($name)) == 0)
            throw $this->createNotFoundException("name is null or length is zero");

        $entities = $em->getRepository('musicCmsBundle:Playlist')->findBy(array('name'=>$name));
        if(count($entities) > 0){
            return new JsonResponse(array('success'=>0,'desc'=>'Өмнө нь үүсгэсэн байна.'));
        }

        $playlist = new Playlist();
        $playlist->user = $me;
        $playlist->name = $name;
        $playlist->user_name = $me->getLoginName();
        if ($name != null && strlen ( trim($name)) > 0)
            $playlist->description = $description;
        $playlist->created_date = new \DateTime();
        $playlist->publish_date = new \DateTime();
        if($isPublic)
            $playlist->is_public = true;
        else
            $playlist->is_public = false;
        $playlist->type = 1;
        $playlist->duration = 0;
        $playlist->track_number = 0;
        $playlist->likeCount = 0;

        $playlist->createDate();

        $em->persist($playlist);
        $em->flush();


        return new JsonResponse(array("pid"=>$playlist->getId(), "success"=>1));
    }

}
