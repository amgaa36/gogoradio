<?php
/**
 * Created by PhpStorm.
 * User: amgaa
 * Date: 8/6/15
 * Time: 9:26 AM
 */


namespace music\MobileBundle\Controller;

use Assetic\Filter\JSMinFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use music\CmsBundle\Entity\User;
use music\WebBundle\Box\MySongBox;
use music\CmsBundle\Entity\UserAudio;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


Class UserController extends Controller{

    public function loginAction(Request $request){

        $social_id = $request->get('socialId');

        $qb = $this->getDoctrine()->getRepository('musicCmsBundle:User')->createQueryBuilder('u');
        $qb->select('u')
            ->where('u.socialId = :gid')
            ->setParameter('gid', $social_id)
            ->setMaxResults(1);
        $result = $qb->getQuery() ->getResult();




        if (count($result) === 0) {

            $


            $user = new User();
            $user->setSocialId($social_id);

            if ($birthday && $birthday != 'null'){
                $user->setUserBirthday(new \DateTime($birthday));
            }
            $user->setUserEmail($email);
            $user->setFirstName($firstName);
            $user->setLastName($lastName);
            $user->setUserImageUrl($imageUrl);
            $user->setLoginName($loginName);
            $user->setDescription($description);
            $user->setLoginType($loginType);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $qb->select('u')
                ->where('u.socialId = :gid')
                ->setParameter('gid', $social_id)
                ->setMaxResults(1);
            $result = $qb->getQuery()->getResult();
            $loggedUser = $result[0];
            $token = new UsernamePasswordToken($user, null, "secured_area", $loggedUser->getRoles());
            $this->get("security.context")->setToken($token);

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        }else {
            $loggedUser = $result[0];

            $token = new UsernamePasswordToken($loggedUser, null, "secured_area", $loggedUser->getRoles());
            $this->get("security.context")->setToken($token);

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        }


        $request->getSession()->set('gogouser', serialize($this->getUser()));

        $userDic['success'] = 1;
        $userDic['user'] = $this->getUser();
        return new JsonResponse($userDic);


    }


    public function logoutAction(){
        $this->get("security.context")->setToken(null);
        $this->get('request')->getSession()->invalidate();
        return new JsonResponse(array('status'=>true));
    }


    public function likeAction(Request $request){
        $userid = $request->get('userid');
        $contentType = $request->get('contentType');
        $contentId = $request->get('contentId');


        $em = $this->getDoctrine()->getManager();

        if($contentType == 1) {
            $content = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $contentId );
        }
        else if($contentType == 2) {
            $content = $em->getRepository ( 'musicCmsBundle:Album' )->find ( $contentId );
        }
        else if($contentType == 3) {
            $content = $em->getRepository ( 'musicCmsBundle:Playlist' )->find ( $contentId );
        }
        else if($contentType == 4) {
            $content = $em->getRepository ( 'musicCmsBundle:Artist' )->find ( $contentId );
        }

        if(!$content) {
            throw $this->createNotFoundException("not found content id=$contentId");
        }

        if($contentType == 1) {
            $qb = $em->getRepository('musicCmsBundle:UserAudio')->createQueryBuilder('ua');
            $ua = $qb->select('ua.id')
                ->where('ua.user_id = :uid')
                ->setParameter('uid', $userid)
                ->andWhere('ua.audio_id = :cid')
                ->setParameter('cid', $contentId)
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();
        }
        else if($contentType == 2) {
            $qb = $em->getRepository('musicCmsBundle:UserAlbum')->createQueryBuilder('ua');
            $ua = $qb->select('ua.id')
                ->where('ua.user_id = :uid')
                ->setParameter('uid', $userid)
                ->andWhere('ua.album_id = :cid')
                ->setParameter('cid', $contentId)
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();
        }
        else if($contentType == 3) {
            $qb = $em->getRepository('musicCmsBundle:UserPlaylist')->createQueryBuilder('ua');
            $ua = $qb->select('ua.id')
                ->where('ua.user_id = :uid')
                ->setParameter('uid', $userid)
                ->andWhere('ua.playlist_id = :cid')
                ->setParameter('cid', $contentId)
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();
        }
        else if($contentType == 4) {
            $qb = $em->getRepository('musicCmsBundle:UserArtist')->createQueryBuilder('ua');
            $ua = $qb->select('ua.id')
                ->where('ua.user_id = :uid')
                ->setParameter('uid', $userid)
                ->andWhere('ua.artist_id = :cid')
                ->setParameter('cid', $contentId)
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();
        }

        if($ua && count($ua) > 0) {
            return new JsonResponse(array('success'=>'1'));
        }


        if($contentType == 1) {
            $userContent = new UserAudio();
            $userContent->audio = $content;
        }
        else if($contentType == 2) {
            $userContent = new UserAlbum();
            $userContent->album = $content;
        }
        else if($contentType == 3) {
            $userContent = new UserPlaylist();
            $userContent->playlist = $content;
        }
        else if($contentType == 4) {
            $userContent = new UserArtist();
            $userContent->artist = $content;
        }



        if($userContent) {
            $userContent->user = $em->getReference('musicCmsBundle:User', $userid);
            $em->persist($userContent);
            $em->flush();
        }

        $content->setLikeCount($content->getLikeCount()+1);
        $em->persist($content);
        $em->flush();

        return new JsonResponse(array('success'=>'1','like_count'=>$content->getLikeCount()));
    }

    public function unlikeAction(Request $request)
    {

        $userid = $request->get('userid');
        $contentType = $request->get('contentType');
        $contentId = $request->get('contentId');

        $em = $this->getDoctrine()->getManager();

        if($contentType == 1) {
            $content = $em->getRepository ( 'musicCmsBundle:Audio' )->find ( $contentId );
        }
        else if($contentType == 2) {
            $content = $em->getRepository ( 'musicCmsBundle:Album' )->find ( $contentId );
        }
        else if($contentType == 3) {
            $content = $em->getRepository ( 'musicCmsBundle:Playlist' )->find ( $contentId );
        }
        else if($contentType == 4) {
            $content = $em->getRepository ( 'musicCmsBundle:Artist' )->find ( $contentId );
        }

        if(!$content) {
            return new JsonResponse(array('success'=>'1', 'desc'=>'oldsongui'));
        }

        if($contentType == 1) {
            $entities = $em->getRepository('musicCmsBundle:UserAudio')->findBy(array(
                'user_id' => $userid, 'audio_id' => $contentId));
        }
        else if($contentType == 2) {
            $entities = $em->getRepository('musicCmsBundle:UserAlbum')->findBy(array(
                'user_id' => $userid, 'album_id' => $contentId));
        }
        else if($contentType == 3) {
            $entities = $em->getRepository('musicCmsBundle:UserPlaylist')->findBy(array(
                'user_id' => $userid, 'playlist_id' => $contentId));
        }
        else if($contentType == 4) {
            $entities = $em->getRepository('musicCmsBundle:UserArtist')->findBy(array(
                'user_id' => $userid, 'artist_id' => $contentId));
        }

        if($entities && count($entities)>0) {
            foreach($entities as $entity) {
                $content->setLikeCount($content->getLikeCount()-1);
                $em->remove($entity);
                $em->flush();
            }


        }
        $em->persist($content);
        $em->flush();

        return new JsonResponse(array('success'=>'1', 'like_count'=>$content->getLikeCount()));

    }


    public function likedSongsAction(Request $request){

        $systemuser = $this->getUser();
        if(!$systemuser){
            return new JsonResponse(array("status"=>false,'desc'=>'not logged'));
        }
        $userid = $systemuser->getId();

        $mysongBox = new MySongBox();
        return new JsonResponse($mysongBox->getMyFavoriteSongs($this,$userid));

    }



}