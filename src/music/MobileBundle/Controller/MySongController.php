<?php
/**
 * Created by PhpStorm.
 * User: amgaa
 * Date: 8/6/15
 * Time: 9:30 AM
 */

namespace music\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

Class MySongController extends Controller{

    public function myPlaylistsAction(Request $request){

        $userid = $request->get('userid');
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $me = $em->getReference('musicCmsBundle:User', $userid);
        if(!$me) return new JsonResponse(array('success'=>0,'desc'=>'user not found'));

        $playlists = $em->getRepository('musicCmsBundle:Playlist')->createQueryBuilder('p')
            ->select("p.id, p.name, p.img,p.user_name,p.duration,p.track_number,p.publish_date")
            ->where('p.user = :u')
            ->orderBy('p.publish_date','desc')
            ->setParameter(':u',$me)
            ->getQuery()
            ->getScalarResult();

        $arr['playlists'] = $playlists;
        return new JsonResponse($arr);

    }

    public function addToPlaylistAction(Request $request){

    }

    public function myHateSongsAction(){
        $systemuser = $this->getUser();
        if(!$systemuser){
            return new JsonResponse(array("status"=>false,'desc'=>'not logged'));
        }
        $userid = $systemuser->getId();
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $me = $em->getReference('musicCmsBundle:User', $userid);
        if(!$me) return new JsonResponse(array('success'=>0,'desc'=>'user not found'));

        $audios = $em->getRepository('musicCmsBundle:UserDislike')->createQueryBuilder('ua')
            ->leftJoin('ua.audio','a')
            ->select("a.id, a.artist_name, a.name,COALESCE(a.img, a.album_img, a.artist_img) as img")
            ->where('ua.user = :u')
            ->setParameter(':u', $me)
            ->orderBy('a.publish_date','desc')
            ->getQuery()
            ->getScalarResult();

        $arr['audios'] = $audios;
        $arr['userid'] = $userid;
        return new JsonResponse($arr);
    }

    public function myFavSongsAction(Request $request){
        $systemuser = $this->getUser();
        if(!$systemuser){
            return new JsonResponse(array("status"=>false,'desc'=>'not logged'));
        }
        $userid = $systemuser->getId();
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $me = $em->getReference('musicCmsBundle:User', $userid);
        if(!$me) return new JsonResponse(array('success'=>0,'desc'=>'user not found'));

        $audios = $em->getRepository('musicCmsBundle:UserAudio')->createQueryBuilder('ua')
            ->leftJoin('ua.audio','a')
            ->select("a.id, a.artist_name, a.name,COALESCE(a.img, a.album_img, a.artist_img) as img")
            ->where('ua.user = :u')
            ->setParameter(':u', $me)
            ->orderBy('a.publish_date','desc')
            ->getQuery()
            ->getScalarResult();

        $arr['audios'] = $audios;
        $arr['userid'] = $userid;
        return new JsonResponse($arr);
    }

    public function myFavPlaylistsAction(Request $request){
        $userid = $request->get('userid');
        $arr = array();
        $em = $this->getDoctrine()->getManager();
        $me = $em->getReference('musicCmsBundle:User', $userid);
        if(!$me) return new JsonResponse(array('success'=>0,'desc'=>'user not found'));

        $playlists = $em->getRepository('musicCmsBundle:UserPlaylist')->createQueryBuilder('up')
            ->leftJoin('up.playlist','p')
            ->select("p.id, p.name, p.img,p.user_name,p.duration,p.track_number,p.publish_date")
            ->where('up.user = :u')
            ->orderBy('p.publish_date','desc')
            ->setParameter(':u',$me)
            ->getQuery()
            ->getScalarResult();

        $arr['playlists'] = $playlists;
        return new JsonResponse($arr);
    }

}