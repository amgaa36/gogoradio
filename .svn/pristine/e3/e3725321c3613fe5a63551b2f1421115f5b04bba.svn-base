<?php

namespace music\CmsBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use music\CmsBundle\Entity\AudioArtist;
use music\CmsBundle\util\AlbumUpdateUtil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use music\CmsBundle\Entity\AlbumArtist;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * AlbumArtist controller.
 *
 * @Route("/albumartist")
 */
class AlbumArtistController extends Controller
{

    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:Album')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Album not found');
        }

        return $this->render('musicCmsBundle:AlbumArtist:index.html.twig', array(
            'entity' => $entity,
        ));

    }


    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('musicCmsBundle:AlbumArtist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AlbumArtist entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    public function addArtistAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('musicCmsBundle:Album')->find($id);
        if (!$album) {
            throw $this->createNotFoundException('Error');
        }
        $album_name = $album->name;
        $albumarts = $album->getArtistalbums();

        $oldartists = new ArrayCollection();
        foreach ($albumarts as $albumart) {
            $oldartists->add($albumart);
        }
        $form = $this->createArtistForm();

        if (strtolower($request->getMethod()) === 'post') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $newartistids = array_map(function ($n) {
                    return intval($n);
                }, explode(',', $form->get('artistids')->getData()));
                //throw $this->createNotFoundException(var_dump($newartistids));
                $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('e');

                $newartists = $qb
                    ->where($qb->expr()->in('e.id', ':ids'))
                    ->setParameter('ids', $newartistids)
                    ->getQuery()
                    ->getResult();

                foreach ($oldartists as $oldartist) {
                    $found = false;
                    foreach ($newartists as $newartist) {
                        if ($newartist->getId() == $oldartist->artist->getId()) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $em->remove($oldartist);
                    }
                }

                foreach ($newartists as $newartist) {
                    $found = false;
                    foreach ($oldartists as $oldartist) {
                        if ($oldartist->artist->getId() == $newartist->getId()) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $dundiin = new AlbumArtist();
                        $dundiin->artist = $newartist;
                        $dundiin->album = $album;
                        $dundiin->order = count($newartists) + 1;
                        $em->persist($dundiin);
                    }
                }

                $em->flush();

                $albumupdateutil = new AlbumUpdateUtil();
                $albumupdateutil->updateAlbumArtistInfoByAlbumObject($this, $album);

                return $this->redirect($this->generateUrl('album', array(
                    'id' => $id,
                )));
            }
        }

        return $this->render('musicCmsBundle:AlbumArtist:show.html.twig', array(
            'audioid' => $id,
            'form' => $form->createView(),
            'albumarts' => $albumarts,
            'albumname' =>$album_name,

        ));

    }

    private function createArtistForm()
    {
        return $this->createFormBuilder()
            ->setMethod("POST")
            ->add('artistids', 'text', array(
                'label' => 'Уран бүтээлчид',
                'required' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Хадгалах',
            ))
            ->getForm();
    }


    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('musicCmsBundle:AlbumArtist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Album not found');
        }

        $album = $entity->album;
        $em->remove($entity);
        $em->flush();

        $albumupdateutil = new AlbumUpdateUtil();

        $albumupdateutil->updateAlbumArtistInfoByAlbumObject($this, $album);

        return $this->redirect($this->generateUrl('album_artist', array(
            'id' => $album->getId(),
        )));
    }

    public function getSelectedArtistAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $request->query->get('q');

        $qb = $em->getRepository('musicCmsBundle:Artist')->createQueryBuilder('e');
        $entities = $qb
            ->select('e.id as id, e.name as name')
            ->where($qb->expr()->like('e.name', ':query'))
            ->setParameter('query', '%' . $q . '%')
            ->getQuery()
            ->getArrayResult();

        return new JsonResponse($entities);
    }
}
